<?php

$conteudo = '[
   {
      "id":"578",
      "valor":"4.00",
      "CLIENTE":{
         "id":"492",
         "nome":"MARIA",
         "sobrenome":"Machado",
         "endereco":"Avenida das Am\u00e9ricas",
         "latitude":null,
         "longitude":null
      },
      "dataCompra":"DATA_AQUI",
      "PRODUTOS":[
         {
            "id":"14135",
            "codigoDeBarras":"7896015516260",
            "nome":"SONRIDOR",
            "detalhes":"500mg cx 60 comp",
            "categoria":"medicamento",
            "quantidade":"2",
            "precoUnitario":".10"
         }
      ],
      "FRANQUIA":{
         "id":"818",
         "nomeFantasia":null,
         "razaoSocial":null,
         "rede":{
            "id":"32",
            "nome":"Sapataria João"
         },
         "endereco":"Rua Acre",
         "latitude":"-22.899079",
         "longitude":"-43.181612"
      }
   }


]';



//a opção true vai forçar o retorno da função como array associativo.
// para json externo: $conteudo = json_decode(file_get_contents('json.json'), true);

foreach($conteudo as $chave => $elementos){
        $cliente = isset($elementos['CLIENTE']) ? $elementos['CLIENTE'] : [];
        $produtos = isset($elementos['PRODUTOS']) ? $elementos['PRODUTOS'] : [];
        $franquia = isset($elementos['FRANQUIA']) ? $elementos['FRANQUIA'] : [];
        $rede = isset($elementos['FRANQUIA']['rede']) ? $elementos['FRANQUIA']['rede'] : [];

        var_dump($cliente);
        var_dump($produtos);
        var_dump($franquia);
        var_dump($rede);

        /**
        As propriedades de $cliente, $produtos, $franquia, etc, podem ser acessadas da seguinte
        maneira:
        $cliente['id'];
        Aqui você pode manipular os dados da forma que desejar (mandar persistir no banco, etc)
        */
        echo $cliente['id'];

        /*******************Exibição generica*******************************/
        exibir('CLIENTE', $cliente);
        exibir('PRODUTOS', $produtos);
        exibir('franquia', $franquia);
        exibir('rede', $rede);
}

function exibir($titulo, $elementos){
    echo '<br><br>' . $titulo;
    foreach($elementos as $chave => $elemento){
        if(is_array($elemento)){
            foreach($elemento as $chave => $valor){
                echo '<br>' . $chave . ' : ' . $valor;
            }
        }else{
            echo '<br>' . $chave . ' : ' . $elemento;
        }
    }
}

?>
