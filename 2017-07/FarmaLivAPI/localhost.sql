-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 13-Jul-2017 às 21:36
-- Versão do servidor: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `estoque`
--
CREATE DATABASE IF NOT EXISTS `estoque` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `estoque`;

-- --------------------------------------------------------

--
-- Estrutura da tabela `blacklist`
--

CREATE TABLE `blacklist` (
  `id` int(11) NOT NULL,
  `varchar1` varchar(255) NOT NULL,
  `varchar2` varchar(255) NOT NULL,
  `varchar3` varchar(255) NOT NULL,
  `varchar4` varchar(255) NOT NULL,
  `varchar5` varchar(255) NOT NULL,
  `num1` int(11) NOT NULL,
  `num2` int(11) NOT NULL,
  `num3` int(11) NOT NULL,
  `num4` int(11) NOT NULL,
  `num5` int(11) NOT NULL,
  `data1` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `data2` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `data3` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `data4` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `data5` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `bit1` int(1) NOT NULL,
  `bit2` int(1) NOT NULL,
  `bit3` int(1) NOT NULL,
  `bit4` int(1) NOT NULL,
  `bit5` int(1) NOT NULL,
  `texto1` longtext NOT NULL,
  `texto2` longtext NOT NULL,
  `texto3` longtext NOT NULL,
  `texto4` longtext NOT NULL,
  `texto5` longtext NOT NULL,
  `alias` varchar(255) NOT NULL,
  `tabela` varchar(255) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `comment_post`
--

CREATE TABLE `comment_post` (
  `id` int(11) NOT NULL,
  `varchar1` varchar(255) NOT NULL,
  `varchar2` varchar(255) NOT NULL,
  `varchar3` varchar(255) NOT NULL,
  `varchar4` varchar(255) NOT NULL,
  `varchar5` varchar(255) NOT NULL,
  `num1` int(11) NOT NULL,
  `num2` int(11) NOT NULL,
  `num3` int(11) NOT NULL,
  `num4` int(11) NOT NULL,
  `num5` int(11) NOT NULL,
  `data1` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `data2` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `data3` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `data4` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `data5` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `bit1` int(1) NOT NULL,
  `bit2` int(1) NOT NULL,
  `bit3` int(1) NOT NULL,
  `bit4` int(1) NOT NULL,
  `bit5` int(1) NOT NULL,
  `texto1` longtext NOT NULL,
  `texto2` longtext NOT NULL,
  `texto3` longtext NOT NULL,
  `texto4` longtext NOT NULL,
  `texto5` longtext NOT NULL,
  `alias` varchar(255) NOT NULL,
  `tabela` varchar(255) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `comment_post`
--

INSERT INTO `comment_post` (`id`, `varchar1`, `varchar2`, `varchar3`, `varchar4`, `varchar5`, `num1`, `num2`, `num3`, `num4`, `num5`, `data1`, `data2`, `data3`, `data4`, `data5`, `bit1`, `bit2`, `bit3`, `bit4`, `bit5`, `texto1`, `texto2`, `texto3`, `texto4`, `texto5`, `alias`, `tabela`) VALUES
(1, 'https', 'Paulo Sérgio Duff', 'Primeiro comentário', 'multitonPDO', '*****', 0, 0, 0, 0, 0, '2017-06-12 19:25:56', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'menssagem'),
(2, 'https', 'Paulo Sérgio Duff', 'Segundo comentário', 'multitonPDO', '*****', 0, 0, 0, 0, 0, '2017-06-12 19:26:06', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'menssagem'),
(3, 'https', 'Paulo Sérgio Duff', 'Segundo comentário', '02', '*****', 0, 0, 0, 0, 0, '2017-06-12 19:31:15', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'menssagem'),
(4, 'multitonPDO', 'Paulo Sérgio Duff', 'Segundo comentário', '02', 'feed', 0, 0, 0, 0, 0, '2017-06-12 19:36:23', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'menssagem'),
(5, 'https', 'Paulo Sérgio Duff', 'Quinto comentário', 'multitonPDO', 'feed', 0, 0, 0, 0, 0, '2017-06-12 19:41:36', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'menssagem'),
(6, '02', 'Paulo Sérgio Duff', 'Sexto comentário', 'multitonPDO', 'feed', 0, 0, 0, 0, 0, '2017-06-12 19:44:48', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'menssagem'),
(7, 'https', 'Paulo Sérgio Duff', 'Sétimo comentário', 'multitonPDO', 'feed', 0, 0, 0, 0, 0, '2017-06-12 20:00:46', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'comment'),
(8, 'https', 'Paulo Sérgio Duff', 'Oitavo comentário', 'multitonPDO', 'feed', 0, 0, 0, 0, 0, '2017-06-12 20:26:07', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', '02'),
(9, 'https', 'Paulo Sérgio Duff', 'Novo comentário', '02', 'feed', 0, 0, 0, 0, 0, '2017-06-12 20:27:55', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'comment'),
(10, 'https', 'Paulo Sérgio Duff', 'Segundo post da minha fanpage', '02', 'GALERÃO', 0, 0, 0, 0, 0, '2017-06-12 20:57:50', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'comment'),
(11, 'https', 'multitonPDO', 'NOVO DADO 2', '02', '*****', 0, 0, 0, 0, 0, '2017-06-13 17:01:44', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'menssagem'),
(12, 'https', 'multitonPDO', 'NOVO DADO 2', '02', '*****', 0, 0, 0, 0, 0, '2017-06-13 19:25:43', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'menssagem'),
(13, 'https', 'Paulo Sérgio Duff', 'Segundo post da minha fanpage', '02', 'NOVO DADO 2', 0, 0, 0, 0, 0, '2017-06-13 21:31:41', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'comment'),
(14, 'https', 'Paulo Sérgio Duff', 'Segundo post da minha fanpage', '02', 'NOVO DADO 2', 0, 0, 0, 0, 0, '2017-06-13 21:40:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'comment'),
(15, 'https', 'Paulo Sérgio Duff', 'Segundo post da minha fanpage 33', '02', 'NOVO DADO 3', 0, 0, 0, 0, 0, '2017-06-14 16:08:17', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'feed'),
(16, 'https', 'Paulo Sérgio Duff', 'Segundo post da minha fanpage 33', '02', 'NOVO DADO 3', 0, 0, 0, 0, 0, '2017-06-14 21:49:16', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'feed'),
(17, 'https', 'Paulo Sérgio Duff', 'Segundo post da minha fanpage 33', '02', 'NOVO DADO 3', 0, 0, 0, 0, 0, '2017-06-14 21:49:34', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'feed'),
(18, 'https', 'Paulo Sérgio Duff', 'Segundo post da minha fanpage 33', '02', 'NOVO DADO 3', 0, 0, 0, 0, 0, '2017-06-14 21:49:47', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'feed'),
(19, 'https', 'Paulo Sérgio Duff', 'Segundo post da minha fanpage 33', '02', 'NOVO DADO 3', 0, 0, 0, 0, 0, '2017-06-14 21:57:59', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'feed'),
(20, 'https', 'root', 'Segundo post da minha fanpage 33', '02', 'NOVO DADO 3', 0, 0, 0, 0, 0, '2017-06-14 21:58:10', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'feed'),
(21, 'https', 'root', 'Segundo post da minha fanpage 33', '02', 'NOVO DADO 3', 0, 0, 0, 0, 0, '2017-06-14 21:59:07', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'feed'),
(22, 'https', 'root', 'vcxzvzxcvxv', '02', 'NOVO DADO 3', 0, 0, 0, 0, 0, '2017-06-14 22:28:08', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'feed'),
(23, 'https', 'root', 'novo teste', '02', 'NOVO DADO 3', 0, 0, 0, 0, 0, '2017-06-14 22:37:32', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'feed'),
(24, 'https', 'root', 'de novo', '02', 'NOVO DADO 3', 0, 0, 0, 0, 0, '2017-06-14 22:38:10', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'feed'),
(25, 'https', 'cobaia3', 'teste', '02', 'NOVO DADO 3', 0, 0, 0, 0, 0, '2017-06-15 16:40:36', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'feed'),
(26, 'https', 'cobaia3', 'Outra mensagem', '02', 'NOVO DADO 3', 0, 0, 0, 0, 0, '2017-06-15 16:45:02', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'feed'),
(27, 'https', 'cobaia3', 'terceira mensagem', '02', 'NOVO DADO 3', 0, 0, 0, 0, 0, '2017-06-15 16:54:40', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'feed'),
(28, 'https', 'cobaia3', 'Quarta mensagem', '02', 'NOVO DADO 3', 0, 0, 0, 0, 0, '2017-06-15 16:55:56', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'feed'),
(29, 'https', 'paulosergioduff', 'Primeira mensagem', '02', 'NOVO DADO 3', 0, 0, 0, 0, 0, '2017-06-15 16:58:08', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'feed');

-- --------------------------------------------------------

--
-- Estrutura da tabela `estoque`
--

CREATE TABLE `estoque` (
  `id` int(11) NOT NULL,
  `varchar1` varchar(255) NOT NULL,
  `varchar2` varchar(255) NOT NULL,
  `varchar3` varchar(255) NOT NULL,
  `varchar4` varchar(255) NOT NULL,
  `varchar5` varchar(255) NOT NULL,
  `num1` int(11) NOT NULL,
  `num2` int(11) NOT NULL,
  `num3` int(11) NOT NULL,
  `num4` int(11) NOT NULL,
  `num5` int(11) NOT NULL,
  `data1` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `data2` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `data3` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `data4` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `data5` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `bit1` int(1) NOT NULL,
  `bit2` int(1) NOT NULL,
  `bit3` int(1) NOT NULL,
  `bit4` int(1) NOT NULL,
  `bit5` int(1) NOT NULL,
  `texto1` longtext NOT NULL,
  `texto2` longtext NOT NULL,
  `texto3` longtext NOT NULL,
  `texto4` longtext NOT NULL,
  `texto5` longtext NOT NULL,
  `alias` varchar(255) NOT NULL,
  `tabela` varchar(255) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `estoque`
--

INSERT INTO `estoque` (`id`, `varchar1`, `varchar2`, `varchar3`, `varchar4`, `varchar5`, `num1`, `num2`, `num3`, `num4`, `num5`, `data1`, `data2`, `data3`, `data4`, `data5`, `bit1`, `bit2`, `bit3`, `bit4`, `bit5`, `texto1`, `texto2`, `texto3`, `texto4`, `texto5`, `alias`, `tabela`) VALUES
(1, '3.55', '32', '7891317129941', '', '', 0, 0, 0, 0, 0, '2017-07-13 14:35:38', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', ''),
(2, '3.68', '32', '7896112119524', '', '', 0, 0, 0, 0, 0, '2017-07-13 14:35:54', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', ''),
(3, '90.14', '18', '7896112113744', '', '', 0, 0, 0, 0, 0, '2017-07-13 18:22:25', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `feed`
--

CREATE TABLE `feed` (
  `id` int(11) NOT NULL,
  `varchar1` varchar(255) NOT NULL,
  `varchar2` varchar(255) NOT NULL,
  `varchar3` varchar(250) NOT NULL,
  `varchar4` varchar(255) NOT NULL,
  `varchar5` varchar(255) NOT NULL,
  `num1` int(11) NOT NULL,
  `num2` int(11) NOT NULL,
  `num3` int(11) NOT NULL,
  `num4` int(11) NOT NULL,
  `num5` int(11) NOT NULL,
  `data1` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `data2` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `data3` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `data4` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `data5` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `bit1` int(1) NOT NULL,
  `bit2` int(1) NOT NULL,
  `bit3` int(1) NOT NULL,
  `bit4` int(1) NOT NULL,
  `bit5` int(1) NOT NULL,
  `texto1` longtext NOT NULL,
  `texto2` longtext NOT NULL,
  `texto3` longtext NOT NULL,
  `texto4` longtext NOT NULL,
  `texto5` longtext NOT NULL,
  `alias` varchar(255) NOT NULL,
  `tabela` varchar(255) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `feed`
--

INSERT INTO `feed` (`id`, `varchar1`, `varchar2`, `varchar3`, `varchar4`, `varchar5`, `num1`, `num2`, `num3`, `num4`, `num5`, `data1`, `data2`, `data3`, `data4`, `data5`, `bit1`, `bit2`, `bit3`, `bit4`, `bit5`, `texto1`, `texto2`, `texto3`, `texto4`, `texto5`, `alias`, `tabela`) VALUES
(187, 'https', 'paulosergioduff', 'tudo limpo', '02', 'NOVO DADO 3', 0, 0, 0, 0, 0, '2017-06-18 16:37:05', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'feed'),
(188, 'https', 'paulosergioduff', 'que interessante', '02', 'NOVO DADO 3', 0, 0, 0, 0, 0, '2017-06-18 16:37:10', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'feed'),
(189, 'https', 'paulosergioduff', 'isso aqui vai ferver porque vou colocar muito texto para testar', '02', 'NOVO DADO 3', 0, 0, 0, 0, 0, '2017-06-18 16:37:25', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'feed'),
(190, 'https', 'billgates', 'vocÊ acha mesmo? Está delirando HAHA', '02', 'NOVO DADO 3', 0, 0, 0, 0, 0, '2017-06-18 16:37:51', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'feed'),
(191, 'https', 'cobaia3', 'me interesso por esta conversa', '02', 'NOVO DADO 3', 0, 0, 0, 0, 0, '2017-06-18 16:57:50', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'feed'),
(192, 'https', 'billgates', 'testando banco', '02', 'NOVO DADO 3', 0, 0, 0, 0, 0, '2017-06-19 01:12:37', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'feed'),
(193, 'https', 'paulosergioduff', 'mais uma noite com mensagens', '02', 'NOVO DADO 3', 0, 0, 0, 0, 0, '2017-06-20 01:21:03', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'feed'),
(197, 'https', 'paulosergioduff', 'demanda', '02', 'NOVO DADO 3', 0, 0, 0, 0, 0, '2017-06-21 14:50:34', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'feed'),
(198, 'https', 'paulosergioduff', 'teste de julho', '02', 'NOVO DADO 3', 0, 0, 0, 0, 0, '2017-07-03 17:31:13', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'feed'),
(199, 'https', 'billgates', 'teste de julho depoi', '02', 'NOVO DADO 3', 0, 0, 0, 0, 0, '2017-07-03 17:34:39', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'feed'),
(200, 'https', 'cobaia3', 'minha vez', '02', 'NOVO DADO 3', 0, 0, 0, 0, 0, '2017-07-03 17:34:57', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'feed');

-- --------------------------------------------------------

--
-- Estrutura da tabela `membros`
--

CREATE TABLE `membros` (
  `id` int(11) NOT NULL,
  `administrador_usuario` varchar(56) NOT NULL,
  `administrador_senha` varchar(255) NOT NULL,
  `permissao` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `membros`
--

INSERT INTO `membros` (`id`, `administrador_usuario`, `administrador_senha`, `permissao`) VALUES
(13, 'cobaia3', 'c0a6f879fed234818b8f0b036f0be43e00c550012f40a4b49bb69ad11d53b54493adcfb01d407b920495f23a0a53e4a5dd47591a9346b8667f179d0f916e4747', ''),
(14, 'paulosergioduff', '01997002f67ed23d216707be635ac2b7ec303bc612afab93dfaeda712c477b95c5056efd88162f3e8ec25811f12ea9c6aad770f3ef18cf9ca02c5cb02c6c68b0', 'admin'),
(27, 'billgates', 'c41a5185e3e50d6e7502e5b22845ae150811de31445147500a673642eeab653af6abd68c5d0fc2b383ae02cd0aae02651fcfbf51c6603fbc46a1d936e59c010f', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `menssage`
--

CREATE TABLE `menssage` (
  `id` int(11) NOT NULL,
  `varchar1` varchar(255) NOT NULL,
  `varchar2` varchar(255) NOT NULL,
  `varchar3` varchar(255) NOT NULL,
  `varchar4` varchar(255) NOT NULL,
  `varchar5` varchar(255) NOT NULL,
  `num1` int(11) NOT NULL,
  `num2` int(11) NOT NULL,
  `num3` int(11) NOT NULL,
  `num4` int(11) NOT NULL,
  `num5` int(11) NOT NULL,
  `data1` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `data2` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `data3` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `data4` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `data5` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `bit1` int(1) NOT NULL,
  `bit2` int(1) NOT NULL,
  `bit3` int(1) NOT NULL,
  `bit4` int(1) NOT NULL,
  `bit5` int(1) NOT NULL,
  `texto1` longtext NOT NULL,
  `texto2` longtext NOT NULL,
  `texto3` longtext NOT NULL,
  `texto4` longtext NOT NULL,
  `texto5` longtext NOT NULL,
  `alias` varchar(255) NOT NULL,
  `tabela` varchar(255) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `menssage`
--

INSERT INTO `menssage` (`id`, `varchar1`, `varchar2`, `varchar3`, `varchar4`, `varchar5`, `num1`, `num2`, `num3`, `num4`, `num5`, `data1`, `data2`, `data3`, `data4`, `data5`, `bit1`, `bit2`, `bit3`, `bit4`, `bit5`, `texto1`, `texto2`, `texto3`, `texto4`, `texto5`, `alias`, `tabela`) VALUES
(93, 'https', 'billgates', 'Mensagem para paulosergioduff', 'paulosergioduff', 'billgatespaulosergioduff', 0, 0, 0, 0, 0, '2017-06-19 00:02:58', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'feed'),
(94, 'https', 'billgates', 'Vamos ver se ele consegue ler', 'paulosergioduff', 'billgatespaulosergioduff', 0, 0, 0, 0, 0, '2017-06-19 00:03:10', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'feed'),
(95, 'https', 'paulosergioduff', 'Estou conseguindo ler. Você consegue ler a minha?', 'billgates', 'paulosergioduffbillgates', 0, 0, 0, 0, 0, '2017-06-19 00:11:04', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'feed'),
(96, 'https', 'paulosergioduff', 'Vamos continuar tentando', 'billgates', 'paulosergioduffbillgates', 0, 0, 0, 0, 0, '2017-06-19 00:11:17', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'feed'),
(97, 'https', 'paulosergioduff', 'Vamos ver você agora', 'billgates', 'paulosergioduffbillgates', 0, 0, 0, 0, 0, '2017-06-19 00:12:33', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'feed'),
(98, 'https', 'billgates', 'Vamos ver você agora', 'billgates', 'billgatesbillgates', 0, 0, 0, 0, 0, '2017-06-19 00:13:09', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'feed'),
(99, 'https', 'billgates', 'ainda consigo ler', 'paulosergioduff', 'billgatespaulosergioduff', 0, 0, 0, 0, 0, '2017-06-19 00:13:22', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'feed'),
(100, 'https', 'billgates', 'acho que tudo deu certo', 'paulosergioduff', 'billgatespaulosergioduff', 0, 0, 0, 0, 0, '2017-06-19 00:13:31', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'feed'),
(106, 'https', 'paulosergioduff', 'minha vez', 'billgates', 'paulosergioduffbillgates', 0, 0, 0, 0, 0, '2017-06-19 01:17:54', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'feed'),
(107, 'https', 'paulosergioduff', 'Estreando recuso de chat privado ', 'cobaia3', 'paulosergioduffcobaia3', 0, 0, 0, 0, 0, '2017-06-19 01:21:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'feed'),
(108, 'https', 'cobaia3', 'hum...', 'paulosergioduff', 'cobaia3paulosergioduff', 0, 0, 0, 0, 0, '2017-06-19 01:21:49', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'feed'),
(109, 'https', 'cobaia3', 'testando aqui também', 'billgates', 'cobaia3billgates', 0, 0, 0, 0, 0, '2017-06-19 01:22:09', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'feed'),
(110, 'https', 'cobaia3', 'Novidade na aparência', 'paulosergioduff', 'cobaia3paulosergioduff', 0, 0, 0, 0, 0, '2017-06-19 01:23:58', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'feed'),
(111, 'https', 'cobaia3', 'teste', 'paulosergioduff', 'cobaia3paulosergioduff', 0, 0, 0, 0, 0, '2017-06-19 01:40:23', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'feed'),
(112, 'https', 'cobaia3', 'Novidade na aparência', 'paulosergioduff', 'cobaia3paulosergioduff', 0, 0, 0, 0, 0, '2017-06-19 01:40:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'feed'),
(113, 'https', 'paulosergioduff', 'minha vez de testar', 'cobaia3', 'paulosergioduffcobaia3', 0, 0, 0, 0, 0, '2017-06-19 01:55:04', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'feed'),
(114, 'https', 'paulosergioduff', 'Outra conversa privada', 'billgates', 'paulosergioduffbillgates', 0, 0, 0, 0, 0, '2017-06-19 01:55:46', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'feed'),
(115, 'https', 'paulosergioduff', 'acho que agora as mensagens estão chegando', 'cobaia3', 'paulosergioduffcobaia3', 0, 0, 0, 0, 0, '2017-06-20 00:58:37', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'feed'),
(116, 'https', 'paulosergioduff', 'recebe minha mensagem', 'billgates', 'paulosergioduffbillgates', 0, 0, 0, 0, 0, '2017-06-20 01:33:19', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'feed'),
(120, 'https', 'paulosergioduff', 'de novo', 'billgates', 'paulosergioduffbillgates', 0, 0, 0, 0, 0, '2017-06-20 09:06:56', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'feed'),
(121, 'https', 'paulosergioduff', 'acho que está tudo funcionando', 'cobaia3', 'paulosergioduffcobaia3', 0, 0, 0, 0, 0, '2017-06-20 09:11:13', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'feed'),
(122, 'https', 'paulosergioduff', 'teste', 'cobaia3', 'paulosergioduffcobaia3', 0, 0, 0, 0, 0, '2017-06-21 14:47:54', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'feed');

-- --------------------------------------------------------

--
-- Estrutura da tabela `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `nome` varchar(16) NOT NULL,
  `link` varchar(255) NOT NULL,
  `hover` varchar(11) NOT NULL,
  `class` varchar(16) NOT NULL,
  `tagInicio` varchar(32) NOT NULL,
  `tagFim` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `menu`
--

INSERT INTO `menu` (`id`, `nome`, `link`, `hover`, `class`, `tagInicio`, `tagFim`) VALUES
(1, 'Menu1', '', '', '', '<ul>', ''),
(2, 'opcao1', '', '', '', '<li>', '</li>'),
(3, 'opcao2', '', '', '', '<li>', '</li>'),
(4, 'opcao3', '', '', '', '<li>', '</li></ul>'),
(5, 'menu2', '', '', '', '<ul>', ''),
(6, 'opcao1', '', '', '', '<li>', '</li>'),
(7, 'opcao2', '', '', '', '<li>', '</li>'),
(8, 'opcao3', '', '', '', '<li>', '</li>');

-- --------------------------------------------------------

--
-- Estrutura da tabela `modelo`
--

CREATE TABLE `modelo` (
  `id` int(11) NOT NULL,
  `varchar1` varchar(255) NOT NULL,
  `varchar2` varchar(255) NOT NULL,
  `varchar3` varchar(255) NOT NULL,
  `varchar4` varchar(255) NOT NULL,
  `varchar5` varchar(255) NOT NULL,
  `num1` int(11) NOT NULL,
  `num2` int(11) NOT NULL,
  `num3` int(11) NOT NULL,
  `num4` int(11) NOT NULL,
  `num5` int(11) NOT NULL,
  `data1` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `data2` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `data3` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `data4` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `data5` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `bit1` int(1) NOT NULL,
  `bit2` int(1) NOT NULL,
  `bit3` int(1) NOT NULL,
  `bit4` int(1) NOT NULL,
  `bit5` int(1) NOT NULL,
  `texto1` longtext NOT NULL,
  `texto2` longtext NOT NULL,
  `texto3` longtext NOT NULL,
  `texto4` longtext NOT NULL,
  `texto5` longtext NOT NULL,
  `alias` varchar(255) NOT NULL,
  `tabela` varchar(255) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `page`
--

CREATE TABLE `page` (
  `id` int(11) NOT NULL,
  `varchar1` varchar(255) NOT NULL,
  `varchar2` varchar(255) NOT NULL,
  `varchar3` varchar(255) NOT NULL,
  `varchar4` varchar(255) NOT NULL,
  `varchar5` varchar(255) NOT NULL,
  `num1` int(11) NOT NULL,
  `num2` int(11) NOT NULL,
  `num3` int(11) NOT NULL,
  `num4` int(11) NOT NULL,
  `num5` int(11) NOT NULL,
  `data1` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `data2` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `data3` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `data4` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `data5` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `bit1` int(1) NOT NULL,
  `bit2` int(1) NOT NULL,
  `bit3` int(1) NOT NULL,
  `bit4` int(1) NOT NULL,
  `bit5` int(1) NOT NULL,
  `texto1` longtext NOT NULL,
  `texto2` longtext NOT NULL,
  `texto3` longtext NOT NULL,
  `texto4` longtext NOT NULL,
  `texto5` longtext NOT NULL,
  `alias` varchar(255) NOT NULL,
  `tabela` varchar(255) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `page`
--

INSERT INTO `page` (`id`, `varchar1`, `varchar2`, `varchar3`, `varchar4`, `varchar5`, `num1`, `num2`, `num3`, `num4`, `num5`, `data1`, `data2`, `data3`, `data4`, `data5`, `bit1`, `bit2`, `bit3`, `bit4`, `bit5`, `texto1`, `texto2`, `texto3`, `texto4`, `texto5`, `alias`, `tabela`) VALUES
(1, 'https', 'Paulo Sérgio Duff', 'Post da minha fanpage', '02', 'postagem da minha página', 0, 0, 0, 0, 0, '2017-06-12 20:47:07', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'page'),
(2, 'https', 'Paulo Sérgio Duff', 'Segundo post da minha fanpage', '02', 'postagem da minha página', 0, 0, 0, 0, 0, '2017-06-12 20:47:31', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'page'),
(3, 'https', 'Paulo Sérgio Duff', 'Segundo post da minha fanpage', '02', 'GALERÃO', 0, 0, 0, 0, 0, '2017-06-12 20:57:50', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'page'),
(4, 'https', 'Paulo Sérgio Duff', 'Segundo post da minha fanpage', '02', 'NOVO DADO 2', 0, 0, 0, 0, 0, '2017-06-13 21:21:27', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'page'),
(5, 'https', 'Paulo Sérgio Duff', 'Segundo post da minha fanpage', '02', 'NOVO DADO 2', 0, 0, 0, 0, 0, '2017-06-13 21:40:53', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'page');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tabela`
--

CREATE TABLE `tabela` (
  `id` int(11) NOT NULL,
  `varchar1` varchar(255) CHARACTER SET utf8 NOT NULL,
  `varchar2` varchar(255) CHARACTER SET utf8 NOT NULL,
  `varchar3` varchar(255) CHARACTER SET utf8 NOT NULL,
  `varchar4` varchar(255) CHARACTER SET utf8 NOT NULL,
  `varchar5` varchar(255) CHARACTER SET utf8 NOT NULL,
  `num1` int(11) NOT NULL,
  `num2` int(11) NOT NULL,
  `num3` int(11) NOT NULL,
  `num4` int(11) NOT NULL,
  `num5` int(11) NOT NULL,
  `data1` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `data2` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `data3` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `data4` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `data5` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `bit1` int(1) NOT NULL,
  `bit2` int(1) NOT NULL,
  `bit3` int(1) NOT NULL,
  `bit4` int(1) NOT NULL,
  `bit5` int(1) NOT NULL,
  `texto1` longtext CHARACTER SET utf8 NOT NULL,
  `texto2` longtext CHARACTER SET utf8 NOT NULL,
  `texto3` longtext CHARACTER SET utf8 NOT NULL,
  `texto4` longtext CHARACTER SET utf8 NOT NULL,
  `texto5` longtext CHARACTER SET utf8 NOT NULL,
  `alias` varchar(255) CHARACTER SET utf8 NOT NULL,
  `tabela` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tabela`
--

INSERT INTO `tabela` (`id`, `varchar1`, `varchar2`, `varchar3`, `varchar4`, `varchar5`, `num1`, `num2`, `num3`, `num4`, `num5`, `data1`, `data2`, `data3`, `data4`, `data5`, `bit1`, `bit2`, `bit3`, `bit4`, `bit5`, `texto1`, `texto2`, `texto3`, `texto4`, `texto5`, `alias`, `tabela`) VALUES
(5, 'Pedro Henrique', 'maria@email.com', 'novasenha', '', '', 0, 0, 0, 0, 0, '2017-03-25 02:19:19', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', ''),
(7, 'Carlos Manuel', 'carlos@hotmail.com', 'abc1234', '', '', 0, 0, 0, 0, 0, '2017-03-25 02:26:26', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', ''),
(8, 'Carlos Manuel2', 'carlos@hotmail.com', 'abc1234', '', '', 0, 0, 0, 0, 0, '2017-03-25 02:27:22', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', ''),
(9, 'Novo Manuel', 'carlos@hotmail.com', 'abc123', '', '', 0, 0, 0, 0, 0, '2017-03-25 03:02:46', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', ''),
(10, 'hackeado', 'carlos@hotmail.com', 'abc123', '', '', 0, 0, 0, 0, 0, '2017-03-25 03:08:11', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', 'login', 'cadastrados'),
(11, 'kkk6789', 'filtro5', 'kkkil@gmailc.om', 'kkkSmais1@mail.com', 'kkkkkalor', 0, 0, 0, 0, 0, '2017-06-12 15:48:51', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'filtro'),
(12, 'kkk6789', 'filtro5', 'kkkil@gmailc.om', 'kkkSmais1@mail.com', 'kkkkkalor', 0, 0, 0, 0, 0, '2017-06-12 15:49:50', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'filtro'),
(13, 'kkk6789', 'filtro5', 'kkkil@gmailc.om', 'kkkSmais1@mail.com', 'kkkkkalor', 0, 0, 0, 0, 0, '2017-06-12 15:49:55', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'filtro');

-- --------------------------------------------------------

--
-- Estrutura da tabela `whitelist`
--

CREATE TABLE `whitelist` (
  `id` int(11) NOT NULL,
  `varchar1` varchar(255) NOT NULL,
  `varchar2` varchar(255) NOT NULL,
  `varchar3` varchar(255) NOT NULL,
  `varchar4` varchar(255) NOT NULL,
  `varchar5` varchar(255) NOT NULL,
  `num1` int(11) NOT NULL,
  `num2` int(11) NOT NULL,
  `num3` int(11) NOT NULL,
  `num4` int(11) NOT NULL,
  `num5` int(11) NOT NULL,
  `data1` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `data2` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `data3` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `data4` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `data5` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `bit1` int(1) NOT NULL,
  `bit2` int(1) NOT NULL,
  `bit3` int(1) NOT NULL,
  `bit4` int(1) NOT NULL,
  `bit5` int(1) NOT NULL,
  `texto1` longtext NOT NULL,
  `texto2` longtext NOT NULL,
  `texto3` longtext NOT NULL,
  `texto4` longtext NOT NULL,
  `texto5` longtext NOT NULL,
  `alias` varchar(255) NOT NULL,
  `tabela` varchar(255) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `whitelist`
--

INSERT INTO `whitelist` (`id`, `varchar1`, `varchar2`, `varchar3`, `varchar4`, `varchar5`, `num1`, `num2`, `num3`, `num4`, `num5`, `data1`, `data2`, `data3`, `data4`, `data5`, `bit1`, `bit2`, `bit3`, `bit4`, `bit5`, `texto1`, `texto2`, `texto3`, `texto4`, `texto5`, `alias`, `tabela`) VALUES
(58, 'https', 'cobaia3', 'billgates', '####', 'NOVO DADO 3', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'whitelist'),
(59, 'https', 'cobaia3', 'paulosergioduff', '####', 'NOVO DADO 3', 0, 0, 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, '', '', '', '', '', '', 'whitelist');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blacklist`
--
ALTER TABLE `blacklist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comment_post`
--
ALTER TABLE `comment_post`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `estoque`
--
ALTER TABLE `estoque`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feed`
--
ALTER TABLE `feed`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `membros`
--
ALTER TABLE `membros`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menssage`
--
ALTER TABLE `menssage`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modelo`
--
ALTER TABLE `modelo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tabela`
--
ALTER TABLE `tabela`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `whitelist`
--
ALTER TABLE `whitelist`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blacklist`
--
ALTER TABLE `blacklist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `comment_post`
--
ALTER TABLE `comment_post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `estoque`
--
ALTER TABLE `estoque`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `feed`
--
ALTER TABLE `feed`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=201;
--
-- AUTO_INCREMENT for table `membros`
--
ALTER TABLE `membros`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `menssage`
--
ALTER TABLE `menssage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `modelo`
--
ALTER TABLE `modelo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `page`
--
ALTER TABLE `page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tabela`
--
ALTER TABLE `tabela`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `whitelist`
--
ALTER TABLE `whitelist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
