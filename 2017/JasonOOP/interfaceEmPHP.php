<?php
// Fonte http://phpbrasil.com/artigo/-4-bAMnsogUW/entendendo-o-conceito-de-interfaces-no-php
interface interfaceCriada
{
                public function gravar($parametro);
}
// Sucesso
class minhaClasse1 implements interfaceCriada
{
                public function gravar($parametro)
                {
                                echo 'gravou';
                }
                public $dado;
}
// Sucesso
class minhaClasse2 implements interfaceCriada
{
                public function gravar($parametro)
                {
                                echo 'gravou';
                }
                public function retornar()
                {
                                echo 'retornar';
                }
}
// Erro (Se remover o comentário) , pois classe não contem método gravar()
/*
class minhaClasse3 implements interfaceCriada{
public function retornar(){
echo 'retornar';
}
}*/
$objeto    = new minhaClasse1;
$parametro = "teste Recebido"; // Apenas para cumprir a interface
$objeto->gravar($parametro);
?>