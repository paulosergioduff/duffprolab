var markersData = [{
            lat: -23.0197484,
            lng: -44.5337303,
            nome: "City Farma",
            morada1: "AV. DR. FRANCISCO MAGALH??ES DE CASTRO 57, Rio de Janeiro, Brazil",
            morada2: "1",
            population: 500 ,codPostal: "3362-522924"
        }, {
            lat: -22.8467643,
            lng: -43.286098,
            nome: "City Farma",
            morada1: "AV. GET?LIO VARGAS, 21 LOJA E S/LOJA, Rio de Janeiro, Brazil",
            morada2: "2",
            population: 500 ,codPostal: "2665-3562 / 2665-234322"
        }, {
            lat: -22.9648859,
            lng: -43.1737403,
            nome: "City Farma",
            morada1: "RUA PRINCESA ISABEL, 21 LOJA 02, Rio de Janeiro, Brazil",
            morada2: "3",
            population: 500 ,codPostal: "2666-413422"
        }, {
            lat: -22.739406,
            lng: -43.3399468,
            nome: "City Farma",
            morada1: "ESTRADA ANNIBAL DA MOTTA, LOTE 1 QUADRA 38 LOJA 1, Rio de Janeiro, Brazil",
            morada2: "4",
            population: 500 ,codPostal: "3771-5235 / 3771-540921"
        }, {
            lat: -22.7446089,
            lng: -43.3292333,
            nome: "City Farma",
            morada1: "ESTRADA ANNIBAL DA MOTTA, S/N LOTE 02 QUADRA H, Rio de Janeiro, Brazil",
            morada2: "5",
            population: 500 ,codPostal: "2785-6823 / 3771-300321"
        }, {
            lat: -22.7510687,
            lng: -43.4036824,
            nome: "City Farma",
            morada1: "ESTRADA RETIRO DA IMPRENSA, 839, Rio de Janeiro, Brazil",
            morada2: "6",
            population: 500 ,codPostal: "2761-021821"
        }, {
            lat: -22.7161433,
            lng: -43.3292545,
            nome: "City Farma",
            morada1: "AV. JOAQUIM DA COSTA LIMA, 14.206, Rio de Janeiro, Brazil",
            morada2: "7",
            population: 500 ,codPostal: "3135-2829 / 3656-490221"
        }, {
            lat: -22.8473633,
            lng: -43.2854539,
            nome: "City Farma",
            morada1: "RUA GETULIO VARGAS, 118, Rio de Janeiro, Brazil",
            morada2: "8",
            population: 500 ,codPostal: "2566-216322"
        }, {
            lat: -21.1444123,
            lng: -41.6842044,
            nome: "City Farma",
            morada1: "RUA TENENTE JOSE TEIXEIRA, 66, Rio de Janeiro, Brazil",
            morada2: "9",
            population: 500 ,codPostal: "3833-925022"
        }, {
            lat: -22.8767203,
            lng: -42.9968002,
            nome: "City Farma",
            morada1: "RODOV. AMARAL PEIXOTO KM 137, Rio de Janeiro, Brazil",
            morada2: "10",
            population: 500 ,codPostal: "2630-457522"
        }, {
            lat: -21.4897598,
            lng: -41.0660009,
            nome: "City Farma",
            morada1: "AVENIDA PRINCIPAL, 17 B, Rio de Janeiro, Brazil",
            morada2: "11",
            population: 500 ,codPostal: "2646-001622"
        }, {
            lat: -22.7276923,
            lng: -42.0342475,
            nome: "City Farma",
            morada1: "RODOV. AMARAL PEIXOTO KM 133 - 125177, Rio de Janeiro, Brazil",
            morada2: "12",
            population: 500 ,codPostal: "2630-4473 / 2630-447522"
        }, {
            lat: -22.514885,
            lng: -41.9271698,
            nome: "City Farma",
            morada1: "AV. GOVERNADOR ROBERTO SILVEIRA, 284, Rio de Janeiro, Brazil",
            morada2: "13",
            population: 500 ,codPostal: "2649-274622"
        }, {
            lat: -22.5590842,
            lng: -42.6891437,
            nome: "City Farma",
            morada1: "RUA FLORIANO PEIXOTO, 161, Rio de Janeiro, Brazil",
            morada2: "14",
            population: 500 ,codPostal: "2649-226122"
        }, {
            lat: -22.8475013,
            lng: -43.2852989,
            nome: "City Farma",
            morada1: "RUA GETULIO VARGAS, 137, Rio de Janeiro, Brazil",
            morada2: "15",
            population: 500 ,codPostal: "2555-448022"
        }, {
            lat: -23.0223031,
            lng: -43.4517088,
            nome: "City Farma",
            morada1: "AV. RAUL VEIGA, 110, Rio de Janeiro, Brazil",
            morada2: "16",
            population: 500 ,codPostal: "2551-115822"
        }, {
            lat: -22.0281289,
            lng: -42.3628445,
            nome: "City Farma",
            morada1: "AV. RAUL VEIGA, 48, Rio de Janeiro, Brazil",
            morada2: "17",
            population: 500 ,codPostal: "2551-015222"
        }, {
            lat: -22.9063843,
            lng: -43.1776274,
            nome: "City Farma",
            morada1: "Avenida Nilo Pe?anha,432, Rio de Janeiro, Brazil",
            morada2: "18",
            population: 500 ,codPostal: "3848-1278 / 3848-146221"
        }, {
            lat: -22.8636484,
            lng: -43.3977894,
            nome: "City Farma",
            morada1: "AV. DUQUE DE CAXIAS, Rio de Janeiro, Brazil",
            morada2: "19",
            population: 500 ,codPostal: "2653-288221"
        }, {
            lat: -22.6802019,
            lng: -43.2914401,
            nome: "City Farma",
            morada1: "ESTRADA VELHA DO PILAR 657, Rio de Janeiro, Brazil",
            morada2: "21",
            population: 500 ,codPostal: "2676-211721"
        }, {
            lat: -22.6466243,
            lng: -43.3108726,
            nome: "City Farma",
            morada1: "ESTRADA VELHA DO PILAR, 182, Rio de Janeiro, Brazil",
            morada2: "22",
            population: 500 ,codPostal: "2773-869921"
        }, {
            lat: -22.7527008,
            lng: -43.3051277,
            nome: "City Farma",
            morada1: "AV. DARCI VARGAS, 1.121, Rio de Janeiro, Brazil",
            morada2: "23",
            population: 500 ,codPostal: "2652-9000 / 2652-938521"
        }, {
            lat: -22.7947789,
            lng: -43.2961586,
            nome: "City Farma",
            morada1: "AV.BRIGADEIRO LIMA E SILVA, 856 LOJAS A-B, Rio de Janeiro, Brazil",
            morada2: "24",
            population: 500 ,codPostal: "2782-731921"
        }, {
            lat: -22.6934835,
            lng: -43.2560867,
            nome: "City Farma",
            morada1: "RUA VICENTE CELESTINO, 704 LOJA A, Rio de Janeiro, Brazil",
            morada2: "26",
            population: 500 ,codPostal: "2776-200021"
        }, {
            lat: -22.6365412,
            lng: -43.2514665,
            nome: "City Farma",
            morada1: "AV. B S/N LOTE 3 QUADRA 57 LOJA 01, Rio de Janeiro, Brazil",
            morada2: "27",
            population: 500 ,codPostal: "3652-492721"
        }, {
            lat: -22.7590593,
            lng: -43.3702531,
            nome: "City Farma",
            morada1: "AV.GENERAL CARLOS M.DE MEDEIROS, 206 QD 18 LT 11 L, Rio de Janeiro, Brazil",
            morada2: "28",
            population: 500 ,codPostal: "3135-9000 / 3135-647121"
        }, {
            lat: -22.9143352,
            lng: -43.1947606,
            nome: "City Farma",
            morada1: "AV. 31 DE MAR?O, 140 - LOJA C, Rio de Janeiro, Brazil",
            morada2: "29",
            population: 500 ,codPostal: "3666-076021"
        }, {
            lat: -22.6776418,
            lng: -43.2477276,
            nome: "City Farma",
            morada1: "PRACA VIEIRA NETO, 135, Rio de Janeiro, Brazil",
            morada2: "30",
            population: 500 ,codPostal: "2678-581721"
        }, {
            lat: -22.7787557,
            lng: -43.3031698,
            nome: "City Farma",
            morada1: "AV. PRESIDENTE TANCREDO NEVES 391, Rio de Janeiro, Brazil",
            morada2: "31",
            population: 500 ,codPostal: "2775-627321"
        }, {
            lat: -22.750436,
            lng: -43.32469,
            nome: "City Farma",
            morada1: "RUA LAURO SODR?? 33, Rio de Janeiro, Brazil",
            morada2: "32",
            population: 500 ,codPostal: "3135-8777 / 2759-580421"
        }, {
            lat: -22.8203325,
            lng: -43.3482839,
            nome: "City Farma",
            morada1: "ESTRADA RIO DOURO,69, Rio de Janeiro, Brazil",
            morada2: "33",
            population: 500 ,codPostal: "2679-6437 / 2679-1328 / 3658-478121"
        }, {
            lat: -22.5412745,
            lng: -42.786398,
            nome: "City Farma",
            morada1: "ESTRADA RIO FRIBURGO, 209 LOJA, Rio de Janeiro, Brazil",
            morada2: "34",
            population: 500 ,codPostal: "3633-2000"
        }, {
            lat: -22.747373,
            lng: -42.858317,
            nome: "City Farma",
            morada1: "AVENIDA 22 DE MAIO, 5.600, Rio de Janeiro, Brazil",
            morada2: "35",
            population: 500 ,codPostal: "2635-1857"
        }, {
            lat: -22.7721589,
            lng: -42.922276,
            nome: "City Farma",
            morada1: "RUA ARTHUR SOUTO, 198 - LOJA 03/04, Rio de Janeiro, Brazil",
            morada2: "36",
            population: 500 ,codPostal: "3638-8000"
        }, {
            lat: -22.7373126,
            lng: -42.8378203,
            nome: "City Farma",
            morada1: "AV. 22 DE MAIO, 7923 - LOJA 02, Rio de Janeiro, Brazil",
            morada2: "37",
            population: 500 ,codPostal: "2635-7552"
        }, {
            lat: -22.8702336,
            lng: -43.7802867,
            nome: "City Farma",
            morada1: "RUA DR. CURVELO CAVALCANTE, 580 LOJA, Rio de Janeiro, Brazil",
            morada2: "38",
            population: 500 ,codPostal: "2688-3159"
        }, {
            lat: -22.8694149,
            lng: -43.7886673,
            nome: "City Farma",
            morada1: "RUA ALZIRA FEITAL, 4 LOTE 1 QUADRA 43, Rio de Janeiro, Brazil",
            morada2: "39",
            population: 500 ,codPostal: "2688-3992"
        }, {
            lat: -21.4202477,
            lng: -41.6917157,
            nome: "City Farma",
            morada1: "AV. JOS? LUIS MARINHO, 77 LOJA A, Rio de Janeiro, Brazil",
            morada2: "40",
            population: 500 ,codPostal: "2783-209322"
        }, {
            lat: -21.2031209,
            lng: -41.8924685,
            nome: "City Farma",
            morada1: "AV. CARDOSO MOREIRA,929, Rio de Janeiro, Brazil",
            morada2: "41",
            population: 500 ,codPostal: "3822-237922"
        }, {
            lat: -22.8730013,
            lng: -43.2671715,
            nome: "City Farma",
            morada1: "AV.BRASIL, 260 - LOJA 1, Rio de Janeiro, Brazil",
            morada2: "42",
            population: 500 ,codPostal: "3351-166624"
        }, {
            lat: -22.1520096,
            lng: -42.4198113,
            nome: "City Farma",
            morada1: "PRA?A CORONEL MONNERAT, 8 - LOJA C, Rio de Janeiro, Brazil",
            morada2: "43",
            population: 500 ,codPostal: "2773-3263 / 2773-418322"
        }, {
            lat: -22.8192453,
            lng: -43.6223389,
            nome: "City Farma",
            morada1: "RODOVIA BR 465 - KM 42 QD B, LTS 1, 2, 3 E 4 LJ C, Rio de Janeiro, Brazil",
            morada2: "44",
            population: 500 ,codPostal: "2682-880021"
        }, {
            lat: -22.7341577,
            lng: -42.71969,
            nome: "City Farma",
            morada1: "RUA MANOEL JOAO GONCALVES, 93, Rio de Janeiro, Brazil",
            morada2: "45",
            population: 500 ,codPostal: "2747-1314 / 2747-142721"
        }, {
            lat: -22.4618596,
            lng: -43.4824046,
            nome: "City Farma",
            morada1: "RUA CORONEL LUIZ PEREIRA DOS SANTOS, 8, Rio de Janeiro, Brazil",
            morada2: "46",
            population: 500 ,codPostal: "2747-4054 / 2747-380721"
        }, {
            lat: -22.9861837,
            lng: -43.2158023,
            nome: "City Farma",
            morada1: "AV. DELFIM MOREIRA, 58, Rio de Janeiro, Brazil",
            morada2: "47",
            population: 500 ,codPostal: "2643-3613"
        }, {
            lat: -22.1154954,
            lng: -43.2088111,
            nome: "City Farma",
            morada1: "RUA PREFEITO WALTER FRANCKLIN,80, Rio de Janeiro, Brazil",
            morada2: "48",
            population: 500 ,codPostal: "2252-2006 / 2255-146824"
        }, {
            lat: -21.9849195,
            lng: -42.2523822,
            nome: "City Farma",
            morada1: "RUA CHAFI ZARIFE, 29 - LOJA, Rio de Janeiro, Brazil",
            morada2: "49",
            population: 500 ,codPostal: "2554-122222"
        }, {
            lat: -22.8203994,
            lng: -43.4074481,
            nome: "City Farma",
            morada1: "AV. ROBERTO DA SILVEIRA, 48 - LOJA 05 E 11, Rio de Janeiro, Brazil",
            morada2: "50",
            population: 500 ,codPostal: "2739-2936 | 2650-916421"
        }, {
            lat: -21.7605955,
            lng: -41.3387533,
            nome: "City Farma",
            morada1: "RUA DR. SIQUEIRA, 448 E 450, Rio de Janeiro, Brazil",
            morada2: "51",
            population: 500 ,codPostal: "2633-130021"
        }, {
            lat: -22.6622688,
            lng: -43.1282683,
            nome: "City Farma",
            morada1: "RUA CAPITAO JOSE DE PAULA 322, Rio de Janeiro, Brazil",
            morada2: "52",
            population: 500 ,codPostal: "2647-0774 / 2647-063821"
        }, {
            lat: -22.6577902,
            lng: -43.0373215,
            nome: "City Farma",
            morada1: "AV SIM?O DA MOTTA, 555, Rio de Janeiro, Brazil",
            morada2: "53",
            population: 500 ,codPostal: "2633-363621"
        }, {
            lat: -22.6124447,
            lng: -43.1789285,
            nome: "City Farma",
            morada1: "AV. SANTOS DUMONT, 413, Rio de Janeiro, Brazil",
            morada2: "54",
            population: 500 ,codPostal: "2659-053321"
        }, {
            lat: -22.8877037,
            lng: -43.3183154,
            nome: "City Farma",
            morada1: "RUA GUARANI, 461 LOJA, Rio de Janeiro, Brazil",
            morada2: "55",
            population: 500 ,codPostal: "2659-900021"
        }, {
            lat: -22.8092126,
            lng: -43.4177861,
            nome: "City Farma",
            morada1: "AV. MIRANDELA, 241-245, Rio de Janeiro, Brazil",
            morada2: "57",
            population: 500 ,codPostal: "2691-9093 / 3760-119521"
        }, {
            lat: -22.8066737,
            lng: -43.4179394,
            nome: "City Farma",
            morada1: "RUA PROFESSOR ALFREDO GONCALVES FIGUEIRA, 170, Rio de Janeiro, Brazil",
            morada2: "58",
            population: 500 ,codPostal: "3761-5505 / 3762-320221"
        }, {
            lat: -22.8707442,
            lng: -43.0963701,
            nome: "City Farma",
            morada1: "AV.CORONEL GUIMAR?ES, 449 LOJA 1, Rio de Janeiro, Brazil",
            morada2: "60",
            population: 500 ,codPostal: "2720-382421"
        }, {
            lat: -22.9039547,
            lng: -43.1166408,
            nome: "City Farma",
            morada1: "AVENIDA JORNALISTA ALBERTO TORRES,115, Rio de Janeiro, Brazil",
            morada2: "61",
            population: 500 ,codPostal: "2622-676721"
        }, {
            lat: -22.3385938,
            lng: -42.5122284,
            nome: "City Farma",
            morada1: "AV. MANOEL CARNEIRO DE MENEZES, 3930, Rio de Janeiro, Brazil",
            morada2: "62",
            population: 500 ,codPostal: "2519-510522"
        }, {
            lat: -22.7259428,
            lng: -44.1390482,
            nome: "City Farma",
            morada1: "RUA PRESIDENTE VARGAS, 08, Rio de Janeiro, Brazil",
            morada2: "63",
            population: 500 ,codPostal: "2522-310822"
        }, {
            lat: -22.2377253,
            lng: -42.5196087,
            nome: "City Farma",
            morada1: "RUA HERDY, 11, Rio de Janeiro, Brazil",
            morada2: "64",
            population: 500 ,codPostal: "2527-157722"
        }, {
            lat: -22.7587798,
            lng: -43.4341301,
            nome: "City Farma",
            morada1: "RUA OSCAR SOARES, 784, Rio de Janeiro, Brazil",
            morada2: "65",
            population: 500 ,codPostal: "3773-898121"
        }, {
            lat: -22.7567271,
            lng: -43.4501544,
            nome: "City Farma",
            morada1: "RUA DOUTOR BARROS JUNIOR,272, Rio de Janeiro, Brazil",
            morada2: "66",
            population: 500 ,codPostal: "3540-1854 / 3540-205821"
        }, {
            lat: -22.8282437,
            lng: -43.5985765,
            nome: "City Farma",
            morada1: "ESTRADA DE MADUREIRA, 20, Rio de Janeiro, Brazil",
            morada2: "67",
            population: 500 ,codPostal: "2686-1012 / 2799-508421"
        }, {
            lat: -22.8539048,
            lng: -43.6017398,
            nome: "City Farma",
            morada1: "ESTRADA RIO S?O PAULO, 4002 - Km32 LOJA A, Rio de Janeiro, Brazil",
            morada2: "69",
            population: 500 ,codPostal: "3759-108521"
        }, {
            lat: -22.8557264,
            lng: -43.6022535,
            nome: "City Farma",
            morada1: "ALAMEDA S?O BERNARDO, 20, Rio de Janeiro, Brazil",
            morada2: "70",
            population: 500 ,codPostal: "2764-429721"
        }, {
            lat: -22.7488577,
            lng: -43.439811,
            nome: "City Farma",
            morada1: "RUA LUIZ SOBRAL, 1.170, Rio de Janeiro, Brazil",
            morada2: "71",
            population: 500 ,codPostal: "2667-423021"
        }, {
            lat: -22.6814899,
            lng: -43.4458705,
            nome: "City Farma",
            morada1: "RUA HELENA, 429, Rio de Janeiro, Brazil",
            morada2: "72",
            population: 500 ,codPostal: "3764-1000 / 2767-261021"
        }, {
            lat: -23.2204787,
            lng: -44.7190241,
            nome: "City Farma",
            morada1: "RUA MANOEL FERREIRA DOS SANTOS PADUA, S/N LOJAS 1, Rio de Janeiro, Brazil",
            morada2: "74",
            population: 500 ,codPostal: "3371-102224"
        }, {
            lat: -22.4195721,
            lng: -43.1767884,
            nome: "City Farma",
            morada1: "RUA BERNARDO COUTINHO, 1.861, Rio de Janeiro, Brazil",
            morada2: "75",
            population: 500 ,codPostal: "2225-193324"
        }, {
            lat: -22.4156275,
            lng: -43.1392607,
            nome: "City Farma",
            morada1: "ESTRADA UNI?O E INDUSTRIA, 8.428, Rio de Janeiro, Brazil",
            morada2: "76",
            population: 500 ,codPostal: "2221-126624"
        }, {
            lat: -22.5112551,
            lng: -43.1783582,
            nome: "City Farma",
            morada1: "RUA DO IMPERADOR, 958, Rio de Janeiro, Brazil",
            morada2: "77",
            population: 500 ,codPostal: "2242-3690 / 2231-478024"
        }, {
            lat: -22.8616009,
            lng: -43.0897094,
            nome: "City Farma",
            morada1: "RUA DR. PORCIUNCOLA, 82, Rio de Janeiro, Brazil",
            morada2: "78",
            population: 500 ,codPostal: "2231-362024"
        }, {
            lat: -22.2826206,
            lng: -43.0914261,
            nome: "City Farma",
            morada1: "ESTRADA UNI??O E INDUSTRIA, 4490, Rio de Janeiro, Brazil",
            morada2: "79",
            population: 500 ,codPostal: "2221-383124"
        }, {
            lat: -22.3852459,
            lng: -43.1327758,
            nome: "City Farma",
            morada1: "ESTRADA DAS ARCAS,68, Rio de Janeiro, Brazil",
            morada2: "80",
            population: 500 ,codPostal: "2222-506524"
        }, {
            lat: -22.4890309,
            lng: -43.1531562,
            nome: "City Farma",
            morada1: "RUA QUISSAMA, 2.143, Rio de Janeiro, Brazil",
            morada2: "81",
            population: 500 ,codPostal: "2246-146124"
        }, {
            lat: -22.8960034,
            lng: -43.1229041,
            nome: "City Farma",
            morada1: "RUA 15 DE NOVEMBRO, 49 LOJA 1, Rio de Janeiro, Brazil",
            morada2: "82",
            population: 500 ,codPostal: "2734-025424"
        }, {
            lat: -22.5271387,
            lng: -41.9447064,
            nome: "City Farma",
            morada1: "RODOVIA AMARAL PEIXOTO, 4.532, Rio de Janeiro, Brazil",
            morada2: "83",
            population: 500 ,codPostal: "2764-592322"
        }, {
            lat: -22.5221507,
            lng: -41.9355499,
            nome: "City Farma",
            morada1: "ALAMEDA CASIMIRO DE ABREU, 314, Rio de Janeiro, Brazil",
            morada2: "85",
            population: 500 ,codPostal: "2764-136822"
        }, {
            lat: -23.003205,
            lng: -43.3420805,
            nome: "City Farma",
            morada1: "AV. GEST?O SENGES, 185 - LOJA 113, Rio de Janeiro, Brazil",
            morada2: "87",
            population: 500 ,codPostal: "3328-500021"
        }, {
            lat: -23.0005334,
            lng: -43.3582418,
            nome: "City Farma",
            morada1: "AV. DAS AMERICAS, 7607 - LOJAS: 103/104, Rio de Janeiro, Brazil",
            morada2: "88",
            population: 500 ,codPostal: "2438-6666 / 2438-900421"
        }, {
            lat: -22.9994143,
            lng: -43.3438598,
            nome: "City Farma",
            morada1: "AV. DAS AM??RICAS,3.120, Rio de Janeiro, Brazil",
            morada2: "89",
            population: 500 ,codPostal: "3215-8690 / 3215-867621"
        }, {
            lat: -22.9876154,
            lng: -43.3088907,
            nome: "City Farma",
            morada1: "ESTRADA DA BARRA DA TIJUCA, 3817, Rio de Janeiro, Brazil",
            morada2: "90",
            population: 500 ,codPostal: "2486-307521"
        }, {
            lat: -22.7970943,
            lng: -43.383916,
            nome: "City Farma",
            morada1: "RUA DR. POTY DE MEDEIROS, 60 - LOJA 104, Rio de Janeiro, Brazil",
            morada2: "91",
            population: 500 ,codPostal: "2438-6074 / 3150-400421"
        }, {
            lat: -23.0038849,
            lng: -43.4252298,
            nome: "City Farma",
            morada1: "AVENIDA DAS AMERICAS,11391, Rio de Janeiro, Brazil",
            morada2: "92",
            population: 500 ,codPostal: "2499-500021"
        }, {
            lat: -22.8668012,
            lng: -43.2719747,
            nome: "City Farma",
            morada1: "AV.NOVA BRASILIA, 10 - LOJA B, Rio de Janeiro, Brazil",
            morada2: "93",
            population: 500 ,codPostal: "2573-5069 / 3866-211521"
        }, {
            lat: -22.8555413,
            lng: -43.2430384,
            nome: "City Farma",
            morada1: "RUA TEIXEIRA RIBEIRO, 689, Rio de Janeiro, Brazil",
            morada2: "94",
            population: 500 ,codPostal: "2573-2000 / 3868-031121"
        }, {
            lat: -22.9526404,
            lng: -43.1872678,
            nome: "City Farma",
            morada1: "Rua Voluntarios da Patria, 173, Rio de Janeiro, Brazil",
            morada2: "95",
            population: 500 ,codPostal: "2527-7303 / 2539-393921"
        }, {
            lat: -22.9535445,
            lng: -43.1899063,
            nome: "City Farma",
            morada1: "RUA VOLUNTARIOS DA PATRIA, 241, Rio de Janeiro, Brazil",
            morada2: "96",
            population: 500 ,codPostal: "2527-100021"
        }, {
            lat: -22.8845786,
            lng: -43.5549755,
            nome: "City Farma",
            morada1: "ESTRADA DA POSSE 3700, LOJA A, Rio de Janeiro, Brazil",
            morada2: "97",
            population: 500 ,codPostal: "3592-4777 - 3592-476721"
        }, {
            lat: -22.8883375,
            lng: -43.5608107,
            nome: "City Farma",
            morada1: "ESTRADA GUANDU DO SAPE,20, Rio de Janeiro, Brazil",
            morada2: "98",
            population: 500 ,codPostal: "3627-2730 / 3627-273121"
        }, {
            lat: -22.9140105,
            lng: -43.5632604,
            nome: "City Farma",
            morada1: "RUA AUGUSTO DE VASCONCELOS, 1290, Rio de Janeiro, Brazil",
            morada2: "99",
            population: 500 ,codPostal: "2490-800021"
        }, {
            lat: -22.9139686,
            lng: -43.5486074,
            nome: "City Farma",
            morada1: "RUA OLINDA ELLIS, 1755 /LOJA J, Rio de Janeiro, Brazil",
            morada2: "100",
            population: 500 ,codPostal: "3384-171521"
        }, {
            lat: -22.8936063,
            lng: -43.5732441,
            nome: "City Farma",
            morada1: "RUA ARICURI, 1570, Rio de Janeiro, Brazil",
            morada2: "101",
            population: 500 ,codPostal: "2413-929221"
        }, {
            lat: -22.9029017,
            lng: -43.5761768,
            nome: "City Farma",
            morada1: "RUA ANFRISIO FIALHO , 20 LOJA A, Rio de Janeiro, Brazil",
            morada2: "102",
            population: 500 ,codPostal: "3547-0001 / 2415-671121"
        }, {
            lat: -22.9139388,
            lng: -43.6021481,
            nome: "City Farma",
            morada1: "AVENIDA CES?RIO DE MELO , 7518 - LOJA B, Rio de Janeiro, Brazil",
            morada2: "103",
            population: 500 ,codPostal: "2409-878721"
        }, {
            lat: -22.9304701,
            lng: -43.1790978,
            nome: "City Farma",
            morada1: "LARGO DO MACHADO, 20 LOJA B, Rio de Janeiro, Brazil",
            morada2: "104",
            population: 500 ,codPostal: "2557-3252 / 3826-177121"
        }, {
            lat: -22.9005767,
            lng: -43.1818484,
            nome: "City Farma",
            morada1: "AV. MARECHAL FLORIANO, 22, Rio de Janeiro, Brazil",
            morada2: "106",
            population: 500 ,codPostal: "2233-846421"
        }, {
            lat: -22.9673904,
            lng: -43.1860817,
            nome: "City Farma",
            morada1: "RUA SIQUEIRA CAMPOS, 115 - A, Rio de Janeiro, Brazil",
            morada2: "107",
            population: 500 ,codPostal: "21"
        }, {
            lat: -22.8790539,
            lng: -43.2674484,
            nome: "City Farma",
            morada1: "AV DOM HELDER CAMARA, 3352, Rio de Janeiro, Brazil",
            morada2: "108",
            population: 500 ,codPostal: "2501-676721"
        }, {
            lat: -22.8822897,
            lng: -43.2761358,
            nome: "City Farma",
            morada1: "AV.DOM HELDER CAMARA, 4370 LOJA A E B, Rio de Janeiro, Brazil",
            morada2: "109",
            population: 500 ,codPostal: "2228-713421"
        }, {
            lat: -22.9889137,
            lng: -43.5526484,
            nome: "City Farma",
            morada1: "ESTRADA DA ILHA,2100, Rio de Janeiro, Brazil",
            morada2: "110",
            population: 500 ,codPostal: "2394-200021"
        }, {
            lat: -22.8798004,
            lng: -43.2592946,
            nome: "City Farma",
            morada1: "RUA TENENTE ABEL CUNHA, 145, Rio de Janeiro, Brazil",
            morada2: "111",
            population: 500 ,codPostal: "2260-527321"
        }, {
            lat: -22.8769384,
            lng: -43.2570178,
            nome: "City Farma",
            morada1: "RUA TENENTE ABEL CUNHA, 14, Rio de Janeiro, Brazil",
            morada2: "112",
            population: 500 ,codPostal: "3977-716721"
        }, {
            lat: -22.8852278,
            lng: -43.6192711,
            nome: "City Farma",
            morada1: "ESTRADA DO CAMPINHO, 5990 LOJA B, Rio de Janeiro, Brazil",
            morada2: "113",
            population: 500 ,codPostal: "3403-100021"
        }, {
            lat: -22.9899922,
            lng: -43.32099,
            nome: "City Farma",
            morada1: "ESTRADA DO ITANHANGA 2.781, Rio de Janeiro, Brazil",
            morada2: "114",
            population: 500 ,codPostal: "2484-4000 / 3349-557721"
        }, {
            lat: -22.9331609,
            lng: -43.3633633,
            nome: "City Farma",
            morada1: "RUA PROF. HENRIQUE COSTA, 730, Rio de Janeiro, Brazil",
            morada2: "115",
            population: 500 ,codPostal: "2424-7000 / 2424-725421"
        }, {
            lat: -22.9679012,
            lng: -43.3919438,
            nome: "City Farma",
            morada1: "AVENIDA JAIME POGGI,80, Rio de Janeiro, Brazil",
            morada2: "116",
            population: 500 ,codPostal: "2288-1000 / 2288-5555 / 2288-888821"
        }, {
            lat: -22.8696039,
            lng: -43.4121657,
            nome: "City Farma",
            morada1: "ESTRADA GENERAL CANROBERT DA COSTA, 1558, Rio de Janeiro, Brazil",
            morada2: "117",
            population: 500 ,codPostal: "2301-1675 / 2301-166821"
        }, {
            lat: -22.8956419,
            lng: -43.2744931,
            nome: "City Farma",
            morada1: "RUA ARISTIDE CAIRE, 281 B, Rio de Janeiro, Brazil",
            morada2: "118",
            population: 500 ,codPostal: "2501-9495 / 2501-3131 / 2501-115521"
        }, {
            lat: -22.8690738,
            lng: -43.3496445,
            nome: "City Farma",
            morada1: "RUA CAROLINA MACHADO, 1.012, Rio de Janeiro, Brazil",
            morada2: "119",
            population: 500 ,codPostal: "3390-542321"
        }, {
            lat: -22.8293519,
            lng: -43.2772875,
            nome: "City Farma",
            morada1: "AV. LOBO J?NIOR, 960 A, Rio de Janeiro, Brazil",
            morada2: "120",
            population: 500 ,codPostal: "2260-6878 / 2573-693721"
        }, {
            lat: -22.8866241,
            lng: -43.3070821,
            nome: "City Farma",
            morada1: "AV. DOM HELDER C?MARA, 8.255, Rio de Janeiro, Brazil",
            morada2: "121",
            population: 500 ,codPostal: "2594-6076 / 2595-3330 / 2593-957821"
        }, {
            lat: -23.011086,
            lng: -43.4610886,
            nome: "City Farma",
            morada1: "Av. das Am?ricas,15.500, Rio de Janeiro, Brazil",
            morada2: "122",
            population: 500 ,codPostal: "7803-533121"
        }, {
            lat: -23.0207934,
            lng: -43.4878482,
            nome: "City Farma",
            morada1: "AV. DAS AM?RICAS, 19.019 - LOJA 203 A/B, Rio de Janeiro, Brazil",
            morada2: "123",
            population: 500 ,codPostal: "2025-233321"
        }, {
            lat: -23.0236056,
            lng: -43.478602,
            nome: "City Farma",
            morada1: "RUA FREDERICO QUARTAROLLI, 107 LOJA A, Rio de Janeiro, Brazil",
            morada2: "124",
            population: 500 ,codPostal: "3418-060621"
        }, {
            lat: -23.0303266,
            lng: -43.4731697,
            nome: "City Farma",
            morada1: "ESTRADA DO PONTAL,7535, Rio de Janeiro, Brazil",
            morada2: "125",
            population: 500 ,codPostal: "3579-422221"
        }, {
            lat: -22.8451225,
            lng: -43.3488988,
            nome: "City Farma",
            morada1: "AV. DOS ITALIANOS, 794, Rio de Janeiro, Brazil",
            morada2: "126",
            population: 500 ,codPostal: "2471-268521"
        }, {
            lat: -22.9166043,
            lng: -43.2222183,
            nome: "City Farma",
            morada1: "RUA MARIZ DE BARROS, 899, Rio de Janeiro, Brazil",
            morada2: "128",
            population: 500 ,codPostal: "2254-0411/ 2284-6627 /2254-168721"
        }, {
            lat: -22.5235051,
            lng: -43.1830614,
            nome: "City Farma",
            morada1: "RUA PROFESSOR CARDOSO FONTES 54, Rio de Janeiro, Brazil",
            morada2: "129",
            population: 500 ,codPostal: "2453-900021"
        }, {
            lat: -22.7879866,
            lng: -43.311764,
            nome: "A Nossa Drogaria",
            morada1: "R. Jos? de Alvarenga, 378, Rio de Janeiro, Brazil",
            morada2: "130",
            population: 500 ,codPostal: "21"
        }, {
            lat: -22.7973069,
            lng: -43.313771,
            nome: "A Nossa Drogaria",
            morada1: "R. Joaquim Lopes de Macedo, 15 a 19, Rio de Janeiro, Brazil",
            morada2: "131",
            population: 500 ,codPostal: "21"
        }, {
            lat: -22.7879518,
            lng: -43.3107786,
            nome: "A Nossa Drogaria",
            morada1: "Av. Presidente Kennedy, 1.613, Rio de Janeiro, Brazil",
            morada2: "133",
            population: 500 ,codPostal: "21"
        }, {
            lat: -22.7867911,
            lng: -43.3123849,
            nome: "A Nossa Drogaria",
            morada1: "Av. Nilo Pe?anha, 138, Rio de Janeiro, Brazil",
            morada2: "134",
            population: 500 ,codPostal: "21"
        }, {
            lat: -22.790004,
            lng: -43.3073259,
            nome: "A Nossa Drogaria",
            morada1: "Pra?a Roberto Silveira, 54, Rio de Janeiro, Brazil",
            morada2: "135",
            population: 500 ,codPostal: "21"
        }, {
            lat: -22.9053344,
            lng: -43.5597454,
            nome: "A Nossa Drogaria",
            morada1: "R. Coronel Agostinho, 129, Rio de Janeiro, Brazil",
            morada2: "136",
            population: 500 ,codPostal: "21"
        }, {
            lat: -22.9180307,
            lng: -43.5628993,
            nome: "A Nossa Drogaria",
            morada1: "Estr. do Monteiro, 1.200, Lj 102 B/C, Rio de Janeiro, Brazil",
            morada2: "138",
            population: 500 ,codPostal: "21"
        }, {
            lat: -22.8790862,
            lng: -43.4646032,
            nome: "A Nossa Drogaria",
            morada1: "Av. C?nego Vasconcelos, 423 G, Rio de Janeiro, Brazil",
            morada2: "139",
            population: 500 ,codPostal: "21"
        }, {
            lat: -22.8085072,
            lng: -43.4164038,
            nome: "A Nossa Drogaria",
            morada1: "Estr. Mirandela, 74, Rio de Janeiro, Brazil",
            morada2: "140",
            population: 500 ,codPostal: "21"
        }, {
            lat: -22.8090384,
            lng: -43.4179027,
            nome: "A Nossa Drogaria",
            morada1: "Estr. Mirandela, 248, Rio de Janeiro, Brazil",
            morada2: "141",
            population: 500 ,codPostal: "21"
        }, {
            lat: -22.8667189,
            lng: -43.2548573,
            nome: "A Nossa Drogaria",
            morada1: "Pra?a das Na??es, 172 A, Rio de Janeiro, Brazil",
            morada2: "142",
            population: 500 ,codPostal: "21"
        }, {
            lat: -22.8653595,
            lng: -43.2549519,
            nome: "A Nossa Drogaria",
            morada1: "Rua Cardoso de Morais, 45, Rio de Janeiro, Brazil",
            morada2: "143",
            population: 500 ,codPostal: "21"
        }, {
            lat: -22.7535758,
            lng: -43.4544457,
            nome: "A Nossa Drogaria",
            morada1: "Av. Gov. Roberto da Silveira, 540, Ljs 101 a 104, Rio de Janeiro, Brazil",
            morada2: "144",
            population: 500 ,codPostal: "21"
        }, {
            lat: -22.7603657,
            lng: -43.4502399,
            nome: "A Nossa Drogaria",
            morada1: "Av. Marechal Floriano Peixoto, 1.996, Rio de Janeiro, Brazil",
            morada2: "145",
            population: 500 ,codPostal: "21"
        }, {
            lat: -22.9063716,
            lng: -43.1763846,
            nome: "A Nossa Drogaria",
            morada1: "Av. Nilo Pe?anha, 209, Rio de Janeiro, Brazil",
            morada2: "146",
            population: 500 ,codPostal: "21"
        }, {
            lat: -22.9166296,
            lng: -43.6834058,
            nome: "A Nossa Drogaria",
            morada1: "R. Felipe Cardoso, 105, Rio de Janeiro, Brazil",
            morada2: "147",
            population: 500 ,codPostal: "21"
        }, {
            lat: -22.9166512,
            lng: -43.6835888,
            nome: "A Nossa Drogaria",
            morada1: "R. Felipe Cardoso, 89, Rio de Janeiro, Brazil",
            morada2: "148",
            population: 500 ,codPostal: "21"
        }, {
            lat: -22.7755705,
            lng: -43.3576604,
            nome: "A Nossa Drogaria",
            morada1: "Av. Comendador Teles, 2.391, Rio de Janeiro, Brazil",
            morada2: "149",
            population: 500 ,codPostal: "21"
        }, {
            lat: -22.7755592,
            lng: -43.3592199,
            nome: "A Nossa Drogaria",
            morada1: "Av. Comendador Teles, 590/ Loja 102, Rio de Janeiro, Brazil",
            morada2: "150",
            population: 500 ,codPostal: "21"
        }, {
            lat: -22.8410511,
            lng: -43.278351,
            nome: "A Nossa Drogaria",
            morada1: "R. dos Romeiros, 111 a 119, Rio de Janeiro, Brazil",
            morada2: "151",
            population: 500 ,codPostal: "21"
        }, {
            lat: -22.8402193,
            lng: -43.2774193,
            nome: "A Nossa Drogaria",
            morada1: "Rua Nicar?gua, 186 Loja a/b, Rio de Janeiro, Brazil",
            morada2: "152",
            population: 500 ,codPostal: "21"
        }, {
            lat: -22.872182,
            lng: -43.3362853,
            nome: "A Nossa Drogaria",
            morada1: "Av. Ministro Edgard Romero, 193, Rio de Janeiro, Brazil",
            morada2: "153",
            population: 500 ,codPostal: "21"
        }, {
            lat: -22.8746332,
            lng: -43.3373876,
            nome: "A Nossa Drogaria",
            morada1: "Av. Ministro Edgard Romero, 71, Rio de Janeiro, Brazil",
            morada2: "154",
            population: 500 ,codPostal: "21"
        }, {
            lat: -22.7634894,
            lng: -43.4011865,
            nome: "Drogaria Atual",
            morada1: "Rua Jo?o Fernandes Neto, 1.376, Rio de Janeiro, Brazil",
            morada2: "155",
            population: 500 ,codPostal: "2759-9000 / 2761-433321"
        }, {
            lat: -22.9725556,
            lng: -43.3680419,
            nome: "Drogaria Atual",
            morada1: "Av. Boulevard, 163 Loja, Rio de Janeiro, Brazil",
            morada2: "156",
            population: 500 ,codPostal: "3775-500021"
        }, {
            lat: -22.8705864,
            lng: -43.7781628,
            nome: "Drogaria Atual",
            morada1: "Rua Dr. Curvelo Cavalvanti, 358, Rio de Janeiro, Brazil",
            morada2: "157",
            population: 500 ,codPostal: "3781-1000 / 3781-111121"
        }, {
            lat: -22.8698109,
            lng: -43.7777927,
            nome: "Drogaria Atual",
            morada1: "Rua General Bocai?va, 7, Rio de Janeiro, Brazil",
            morada2: "158",
            population: 500 ,codPostal: "3781-1000 / 3781-111121"
        }, {
            lat: -22.8705877,
            lng: -43.7781492,
            nome: "Drogaria Atual",
            morada1: "Rua Dr. Curvelo Cavalvanti, 320, Rio de Janeiro, Brazil",
            morada2: "159",
            population: 500 ,codPostal: "3781-1000 / 3781-111121"
        }, {
            lat: -22.8706232,
            lng: -43.7790764,
            nome: "Drogaria Atual",
            morada1: "Rua Dr. Curvelo Cavalvanti, 449, Rio de Janeiro, Brazil",
            morada2: "160",
            population: 500 ,codPostal: "3781-1000 / 3781-111121"
        }, {
            lat: -22.8703156,
            lng: -43.7787277,
            nome: "Drogaria Atual",
            morada1: "Rua Dr. Curvelo Cavalvanti, 414 CEP 23815-292, Rio de Janeiro, Brazil",
            morada2: "161",
            population: 500 ,codPostal: "3781-1000 / 3781-111121"
        }, {
            lat: -22.8798407,
            lng: -43.5390317,
            nome: "Drogaria Atual",
            morada1: "Rua Dr. Monteiro de Azevedo, 34 Loja 10, Rio de Janeiro, Brazil",
            morada2: "162",
            population: 500 ,codPostal: "3781-1000 / 3781-111121"
        }, {
            lat: -22.8705903,
            lng: -43.7781236,
            nome: "Drogaria Atual",
            morada1: "Rua Dr. Curvelo Cavalvanti, 248, Rio de Janeiro, Brazil",
            morada2: "163",
            population: 500 ,codPostal: "3781-1000 / 3781-111121"
        }, {
            lat: -22.8705781,
            lng: -43.7797444,
            nome: "Drogaria Atual",
            morada1: "R. Dr. Curvelo Cavalcante, 523A, Rio de Janeiro, Brazil",
            morada2: "164",
            population: 500 ,codPostal: "3781-1000 / 3781-111121"
        }, {
            lat: -22.9499254,
            lng: -43.1801288,
            nome: "Drogaria Atual",
            morada1: "Av. Santos Moreira, 40 - Loja 02, Rio de Janeiro, Brazil",
            morada2: "165",
            population: 500 ,codPostal: "2762-9514 / 2762-961722"
        }, {
            lat: -22.7833225,
            lng: -43.4303075,
            nome: "Drogaria Atual",
            morada1: "Rua Mister Watkins, 50, Rio de Janeiro, Brazil",
            morada2: "166",
            population: 500 ,codPostal: "2796-151521"
        }, {
            lat: -22.8114788,
            lng: -43.4229938,
            nome: "Drogaria Atual",
            morada1: "Av. Mirandela, 829, Rio de Janeiro, Brazil",
            morada2: "168",
            population: 500 ,codPostal: "3760-6000 / 3760-804421"
        }, {
            lat: -22.718659,
            lng: -43.5251189,
            nome: "Drogaria Atual",
            morada1: "Rua Coronel Monteiro de Barros, 274, Rio de Janeiro, Brazil",
            morada2: "169",
            population: 500 ,codPostal: "2763-2301 / 2763-800021"
        }, {
            lat: -22.7584486,
            lng: -43.4505667,
            nome: "Drogaria Atual",
            morada1: "Rua Otavio Tarquino, 173, Rio de Janeiro, Brazil",
            morada2: "170",
            population: 500 ,codPostal: "3763-900021"
        }, {
            lat: -22.7610366,
            lng: -43.4544803,
            nome: "Drogaria Atual",
            morada1: "Rua Ivan Vigne, 181, Rio de Janeiro, Brazil",
            morada2: "171",
            population: 500 ,codPostal: "2667-6000 / 2667-494921"
        }, {
            lat: -22.6923429,
            lng: -43.2677149,
            nome: "Drogaria Atual",
            morada1: "Rua Thomaz Fonseca, 49, Rio de Janeiro, Brazil",
            morada2: "172",
            population: 500 ,codPostal: "2767-202521"
        }, {
            lat: -22.7427451,
            lng: -43.4859755,
            nome: "Drogaria Atual",
            morada1: "Rua Tom?s Fonseca, 226, Rio de Janeiro, Brazil",
            morada2: "173",
            population: 500 ,codPostal: "2668-171621"
        }, {
            lat: -22.8531899,
            lng: -43.6017167,
            nome: "Drogaria Atual",
            morada1: "Estrada Rio - S?o Paulo , 3596, Rio de Janeiro, Brazil",
            morada2: "174",
            population: 500 ,codPostal: "2686-177221"
        }, {
            lat: -22.706915,
            lng: -43.430229,
            nome: "Drogaria Atual",
            morada1: "Rua Cameron, 31 Loja, Rio de Janeiro, Brazil",
            morada2: "175",
            population: 500 ,codPostal: "2882-007721"
        }, {
            lat: -22.6876512,
            lng: -43.4603553,
            nome: "Drogaria Atual",
            morada1: "Rua Pintassilgo, 327, Rio de Janeiro, Brazil",
            morada2: "176",
            population: 500 ,codPostal: "3767-040121"
        }, {
            lat: -22.6102365,
            lng: -43.7096978,
            nome: "Drogaria Atual",
            morada1: "Rua Dominique Level, 94, Rio de Janeiro, Brazil",
            morada2: "177",
            population: 500 ,codPostal: "2683-211421"
        }, {
            lat: -22.7161495,
            lng: -43.5565506,
            nome: "Drogaria Atual",
            morada1: "Av. Irm?os Guinle, 1.093, Rio de Janeiro, Brazil",
            morada2: "178",
            population: 500 ,codPostal: "2665-100121"
        }, {
            lat: -22.9545427,
            lng: -43.6891022,
            nome: "Drogaria Atual",
            morada1: "Rua 14, 339, Rio de Janeiro, Brazil",
            morada2: "179",
            population: 500 ,codPostal: "2230-406821"
        }, {
            lat: -22.8864973,
            lng: -43.5583984,
            nome: "Drogaria Atual",
            morada1: "Estrada do Mendanha, 294 Loja A, Rio de Janeiro, Brazil",
            morada2: "180",
            population: 500 ,codPostal: "2415-900021"
        }, {
            lat: -22.838793,
            lng: -43.3790554,
            nome: "Drogaria Atual",
            morada1: "Rua Marcos de Macedo, 254, Rio de Janeiro, Brazil",
            morada2: "181",
            population: 500 ,codPostal: "3107-300021"
        }, {
            lat: -22.8401844,
            lng: -43.3780463,
            nome: "Drogaria Atual",
            morada1: "Rua Marcos de Macedo, 464, Rio de Janeiro, Brazil",
            morada2: "182",
            population: 500 ,codPostal: "3106-816621"
        }, {
            lat: -22.872122,
            lng: -43.4318339,
            nome: "Drogaria Atual",
            morada1: "Rua Marechal Modestino, 230 Loja 5E, Rio de Janeiro, Brazil",
            morada2: "183",
            population: 500 ,codPostal: "3335-400021"
        }, {
            lat: -22.8564036,
            lng: -43.3293869,
            nome: "Drogaria Atual",
            morada1: "Av Monsenhor Felix, 11 Loja A, Rio de Janeiro, Brazil",
            morada2: "184",
            population: 500 ,codPostal: "3381-900021"
        }, {
            lat: -22.7985707,
            lng: -43.4188901,
            nome: "Drogaria Atual",
            morada1: "Rua Dr. Rufino Gon?alves, 104 A Loja, Rio de Janeiro, Brazil",
            morada2: "185",
            population: 500 ,codPostal: "2751-5186 / 3755-959521"
        }, {
            lat: -22.7782762,
            lng: -43.3681691,
            nome: "Drogaria Atual",
            morada1: "Av. Presidente Lincoln, 691, Rio de Janeiro, Brazil",
            morada2: "186",
            population: 500 ,codPostal: "2699-122221"
        }, {
            lat: -22.9076324,
            lng: -43.5601684,
            nome: "Drogaria Atual",
            morada1: "Rua Eliz?rio de Souza, 670, Rio de Janeiro, Brazil",
            morada2: "187",
            population: 500 ,codPostal: "2756-245221"
        }, {
            lat: -22.8117538,
            lng: -43.6291876,
            nome: "Drogaria Atual",
            morada1: "Av. Ministro Fernando Costa, 471, Rio de Janeiro, Brazil",
            morada2: "188",
            population: 500 ,codPostal: "2682-1932 / 3787-400021"
        }, {
            lat: -22.7473363,
            lng: -43.6992087,
            nome: "Drogaria Atual",
            morada1: "Av. Ministro Fernando Costa, 483 lj 3,4 e 5, Rio de Janeiro, Brazil",
            morada2: "189",
            population: 500 ,codPostal: "2682-1932 / 3787-400021"
        }, {
            lat: -22.7312025,
            lng: -43.357467,
            nome: "Drogarias Viva Mais",
            morada1: "Est. Do Conde, s/n - Lt.2 Q.5 Lj. 02/03/04, Rio de Janeiro, Brazil",
            morada2: "190",
            population: 500 ,codPostal: "3663-020721"
        }, {
            lat: -22.9196404,
            lng: -43.1762119,
            nome: "Drogarias Viva Mais",
            morada1: "R. L, 496 - Km, 133, Rio de Janeiro, Brazil",
            morada2: "191",
            population: 500 ,codPostal: "2630-510022"
        }, {
            lat: -22.900766,
            lng: -42.4796835,
            nome: "Drogarias Viva Mais",
            morada1: "Rua L - km 133 s/n Qd.14 -Lote 19 lj. 5, Rio de Janeiro, Brazil",
            morada2: "192",
            population: 500 ,codPostal: "2630-966622"
        }, {
            lat: -22.8166496,
            lng: -43.4206864,
            nome: "Drogarias Viva Mais",
            morada1: "Rua Dr. Manoel Reis, 968, Rio de Janeiro, Brazil",
            morada2: "193",
            population: 500 ,codPostal: "2674-225621"
        }, {
            lat: -22.9386625,
            lng: -43.1802582,
            nome: "Drogarias Viva Mais",
            morada1: "Rua Marechal Bento Manoel, 391 LJ. A, Rio de Janeiro, Brazil",
            morada2: "194",
            population: 500 ,codPostal: "2671-196821"
        }, {
            lat: -22.7904299,
            lng: -43.1813016,
            nome: "Drogarias Viva Mais",
            morada1: "Av. Ilha das Enxadas, 679 lj. C, Rio de Janeiro, Brazil",
            morada2: "195",
            population: 500 ,codPostal: "2466-212021"
        }, {
            lat: -22.8852859,
            lng: -43.5035848,
            nome: "Drogarias Viva Mais",
            morada1: "Est. Amaral Peixoto, s/n - Lj.3, Rio de Janeiro, Brazil",
            morada2: "196",
            population: 500 ,codPostal: "2635-169621"
        }, {
            lat: -22.9070997,
            lng: -43.1760954,
            nome: "Drogarias Viva Mais",
            morada1: "P?. Roberto Pereira dos Santos, Rio de Janeiro, Brazil",
            morada2: "198",
            population: 500 ,codPostal: "3639-169421"
        }, {
            lat: -22.7755134,
            lng: -43.2928651,
            nome: "Drogarias Viva Mais",
            morada1: "Rua Silva Jardim, 500, Rio de Janeiro, Brazil",
            morada2: "199",
            population: 500 ,codPostal: "2772-259722"
        }, {
            lat: -22.9069988,
            lng: -42.8228573,
            nome: "Drogarias Viva Mais",
            morada1: "R Raul Alfredo de Andrade, lj. 03. Quadra k - Lt 1, Rio de Janeiro, Brazil",
            morada2: "200",
            population: 500 ,codPostal: "3731-9718"
        }, {
            lat: -22.4069866,
            lng: -43.4242234,
            nome: "Drogarias Viva Mais",
            morada1: "Estrada da Cachoeira, 37 lj. 5 e 6, Rio de Janeiro, Brazil",
            morada2: "201",
            population: 500 ,codPostal: "2634-9545 / 2636-8000"
        }, {
            lat: -22.8069682,
            lng: -43.4159313,
            nome: "Drogarias Viva Mais",
            morada1: "Av. Carmela Dutra, 1825 e 1829, Rio de Janeiro, Brazil",
            morada2: "202",
            population: 500 ,codPostal: "2791-909021"
        }, {
            lat: -22.8693114,
            lng: -43.1159799,
            nome: "Drogarias Viva Mais",
            morada1: "R. Presidente Vargas, 179, Rio de Janeiro, Brazil",
            morada2: "203",
            population: 500 ,codPostal: "2722-0738 / 2722-000321"
        }, {
            lat: -22.8804793,
            lng: -43.0961772,
            nome: "Drogarias Viva Mais",
            morada1: "Alameda S?o Boa Ventura,553 Loja 02, Rio de Janeiro, Brazil",
            morada2: "204",
            population: 500 ,codPostal: "2625-448621"
        }, {
            lat: -22.8069682,
            lng: -43.4159313,
            nome: "Drogarias Viva Mais",
            morada1: "Av. Carmela Dutra, 1825 e 1829, Rio de Janeiro, Brazil",
            morada2: "206",
            population: 500 ,codPostal: "2791-909021"
        }, {
            lat: -22.9076611,
            lng: -43.1043435,
            nome: "Drogarias Viva Mais",
            morada1: "Rua Lemos Cunha, 424 Loja 3, Rio de Janeiro, Brazil",
            morada2: "207",
            population: 500 ,codPostal: "2714-305021"
        }, {
            lat: -22.9383876,
            lng: -43.1751276,
            nome: "Drogarias Viva Mais",
            morada1: "Rua S?o Janu?rio, 130 lj, Rio de Janeiro, Brazil",
            morada2: "208",
            population: 500 ,codPostal: "2625-156221"
        }, {
            lat: -22.8615811,
            lng: -43.0952749,
            nome: "Drogarias Viva Mais",
            morada1: "Rua Dr. March,266 lj A e B, Rio de Janeiro, Brazil",
            morada2: "209",
            population: 500 ,codPostal: "2720-717021"
        }, {
            lat: -22.9052556,
            lng: -43.1775701,
            nome: "Drogarias Viva Mais",
            morada1: "Av. Viscnde Rio Branco, s/n, Rio de Janeiro, Brazil",
            morada2: "210",
            population: 500 ,codPostal: "2613 212021"
        }, {
            lat: -22.8228396,
            lng: -43.0438005,
            nome: "Drogarias Viva Mais",
            morada1: "Rua Doutor Nilo Pe?anha, 76 Lj. 02 e 14, Rio de Janeiro, Brazil",
            morada2: "211",
            population: 500 ,codPostal: "2618 4000 / 2719 484921"
        }, {
            lat: -22.8091256,
            lng: -43.1998624,
            nome: "Drogarias Viva Mais",
            morada1: "Rua S?o Louren?o, 311, Rio de Janeiro, Brazil",
            morada2: "212",
            population: 500 ,codPostal: "2722 244621"
        }, {
            lat: -22.8705929,
            lng: -43.0980548,
            nome: "Drogarias Viva Mais",
            morada1: "Rua Francisco Sardinha, 4, Rio de Janeiro, Brazil",
            morada2: "213",
            population: 500 ,codPostal: "2628-0220 / 2628-854021"
        }, {
            lat: -22.8363262,
            lng: -43.3509661,
            nome: "Drogarias Viva Mais",
            morada1: "R. Imboassu,172, Rio de Janeiro, Brazil",
            morada2: "214",
            population: 500 ,codPostal: "2607-142721"
        }, {
            lat: -22.7494471,
            lng: -43.4403217,
            nome: "Drogarias Viva Mais",
            morada1: "Rua Ubaldino Ribeiro dos Santos,75, Rio de Janeiro, Brazil",
            morada2: "218",
            population: 500 ,codPostal: "2667-0111 / 2667-517121"
        }, {
            lat: -22.8820461,
            lng: -43.4300858,
            nome: "Drogarias Viva Mais",
            morada1: "Rua Dona Helena, 24 qd. 6 lote 162, Rio de Janeiro, Brazil",
            morada2: "219",
            population: 500 ,codPostal: "2658 1000 / 2667 606021"
        }, {
            lat: -22.7542167,
            lng: -43.4545909,
            nome: "Drogarias Viva Mais",
            morada1: "Av. Gov. Roberto Silveira, 470 - Lj. 14, Rio de Janeiro, Brazil",
            morada2: "220",
            population: 500 ,codPostal: "2695-200021"
        }, {
            lat: -22.7449828,
            lng: -43.4949317,
            nome: "Drogarias Viva Mais",
            morada1: "Rua dos Quarteis, 344, Rio de Janeiro, Brazil",
            morada2: "221",
            population: 500 ,codPostal: "3764-400021"
        }, {
            lat: -22.9425568,
            lng: -43.1828901,
            nome: "Drogarias Viva Mais",
            morada1: "Rua Muniz Barreto,53, Rio de Janeiro, Brazil",
            morada2: "222",
            population: 500 ,codPostal: "2658-779421"
        }, {
            lat: -22.8188307,
            lng: -43.4248094,
            nome: "Drogarias Viva Mais",
            morada1: "R. Mario Ferreira do Reis, 259 Casa 1 -Lt.02. Qd.B, Rio de Janeiro, Brazil",
            morada2: "224",
            population: 500 ,codPostal: "3778 9112 / 3764-814821"
        }, {
            lat: -22.9244734,
            lng: -43.1783865,
            nome: "Drogarias Viva Mais",
            morada1: "Rua Pedro Am?rico, 116, Rio de Janeiro, Brazil",
            morada2: "225",
            population: 500 ,codPostal: "2205-574721"
        }, {
            lat: -23.0218603,
            lng: -43.4634357,
            nome: "Drogarias Viva Mais",
            morada1: "R. Eng. Haroldo Cavalcanti, 420 - Lj. 105, Rio de Janeiro, Brazil",
            morada2: "226",
            population: 500 ,codPostal: "2779-200021"
        }, {
            lat: -22.870362,
            lng: -43.3670246,
            nome: "Drogarias Viva Mais",
            morada1: "R. Divis?ria,355 -Lj.B, Rio de Janeiro, Brazil",
            morada2: "227",
            population: 500 ,codPostal: "2451-7080 / 2451-705521"
        }, {
            lat: -22.8479651,
            lng: -43.3063368,
            nome: "Drogarias Viva Mais",
            morada1: "Av. Vicente de Carvalho, 1127 LJ. B, Rio de Janeiro, Brazil",
            morada2: "228",
            population: 500 ,codPostal: "2482-3803 / 3013 217321"
        }, {
            lat: -22.9814323,
            lng: -43.6646283,
            nome: "Drogarias Viva Mais",
            morada1: "Est. do Piai, 1.455 - Lj.B, Rio de Janeiro, Brazil",
            morada2: "229",
            population: 500 ,codPostal: "3427-700021"
        }, {
            lat: -22.8859582,
            lng: -43.4755581,
            nome: "Drogarias Viva Mais",
            morada1: "Rua Rio da Prata,1.342, Rio de Janeiro, Brazil",
            morada2: "230",
            population: 500 ,codPostal: "3291-500021"
        }, {
            lat: -22.8714488,
            lng: -43.4581784,
            nome: "Drogarias Viva Mais",
            morada1: "Rua Figueiredo Camargo, 851, Rio de Janeiro, Brazil",
            morada2: "231",
            population: 500 ,codPostal: "3468-2924 / 3464-577321"
        }, {
            lat: -22.8859582,
            lng: -43.4755581,
            nome: "Drogarias Viva Mais",
            morada1: "Rua Rio da Prata,1.342, Rio de Janeiro, Brazil",
            morada2: "232",
            population: 500 ,codPostal: "3291-3231 / 3291 316921"
        }, {
            lat: -22.8997893,
            lng: -43.559339,
            nome: "Drogarias Viva Mais",
            morada1: "Rua Barcelos Domingos, 89 lj. C, Rio de Janeiro, Brazil",
            morada2: "233",
            population: 500 ,codPostal: "2412-136321"
        }, {
            lat: -22.8984236,
            lng: -43.5661199,
            nome: "Drogarias Viva Mais",
            morada1: "Est. Rio do A, 505 lj. A, Rio de Janeiro, Brazil",
            morada2: "234",
            population: 500 ,codPostal: "2413 8800 / 2394 598921"
        }, {
            lat: -22.8382688,
            lng: -43.3499515,
            nome: "Drogarias Viva Mais",
            morada1: "Av. dos Italianos, 1.112, Rio de Janeiro, Brazil",
            morada2: "235",
            population: 500 ,codPostal: "3855-900021"
        }, {
            lat: -22.9300727,
            lng: -43.2425496,
            nome: "Drogarias Viva Mais",
            morada1: "Rua Uruguai, 339, Lj B, Rio de Janeiro, Brazil",
            morada2: "236",
            population: 500 ,codPostal: "2288-899421"
        }, {
            lat: -22.9046171,
            lng: -43.5983081,
            nome: "Drogarias Viva Mais",
            morada1: "Rua Seabra Filho, 526 Lj.B, Rio de Janeiro, Brazil",
            morada2: "237",
            population: 500 ,codPostal: "3364-400421"
        }, {
            lat: -22.9076345,
            lng: -43.2701209,
            nome: "Drogarias Viva Mais",
            morada1: "Rua Bar?o do Bom Retiro, 484, Rio de Janeiro, Brazil",
            morada2: "238",
            population: 500 ,codPostal: "2201-8383 / 2581-224221"
        }, {
            lat: -22.9105535,
            lng: -43.2757713,
            nome: "Drogarias Viva Mais",
            morada1: "Rua Cabu?u, 144, Rio de Janeiro, Brazil",
            morada2: "239",
            population: 500 ,codPostal: "3173-640021"
        }, {
            lat: -22.8082218,
            lng: -43.3279959,
            nome: "Drogarias Viva Mais",
            morada1: "Rua Prof. Costa Ribeiro, 460 Lj. A, Rio de Janeiro, Brazil",
            morada2: "240",
            population: 500 ,codPostal: "3346 900021"
        }, {
            lat: -22.8055393,
            lng: -43.3644919,
            nome: "Drogarias Viva Mais",
            morada1: "Av. Sargento de Mil?cias, 158, Rio de Janeiro, Brazil",
            morada2: "242",
            population: 500 ,codPostal: "2407-6229/ 3252-608121"
        }, {
            lat: -22.8925088,
            lng: -43.3479523,
            nome: "Drogarias Viva Mais",
            morada1: "Rua Cap. Menezes, 715, Rio de Janeiro, Brazil",
            morada2: "243",
            population: 500 ,codPostal: "3350-832221"
        }, {
            lat: -22.8509576,
            lng: -43.2698857,
            nome: "Drogarias Viva Mais",
            morada1: "Jo?o Rego, 161, Rio de Janeiro, Brazil",
            morada2: "244",
            population: 500 ,codPostal: "2260-284721"
        }, {
            lat: -23.0059335,
            lng: -43.4307065,
            nome: "Drogarias Viva Mais",
            morada1: "Av. das Am?ricas, 11889 Lj. A, Rio de Janeiro, Brazil",
            morada2: "245",
            population: 500 ,codPostal: "2498 700021"
        }, {
            lat: -22.8314416,
            lng: -43.4003208,
            nome: "Drogarias Viva Mais",
            morada1: "R. Campos, 602 LJ, Rio de Janeiro, Brazil",
            morada2: "246",
            population: 500 ,codPostal: "3752 -300021"
        }, {
            lat: -22.7982659,
            lng: -43.41911,
            nome: "Drogarias Viva Mais",
            morada1: "P?. Doutor Rufino Gon?alves, 42, Rio de Janeiro, Brazil",
            morada2: "247",
            population: 500 ,codPostal: "3755-400021"
        }, {
            lat: -22.7755645,
            lng: -43.358658,
            nome: "Drogarias Viva Mais",
            morada1: "Av. Comendador Teles, Lt.10 Q.4, Rio de Janeiro, Brazil",
            morada2: "248",
            population: 500 ,codPostal: "3757-363721"
        }, {
            lat: -22.836197,
            lng: -42.1013435,
            nome: "Drogarias Viva Mais",
            morada1: "Rua Dr. Ant?nio Alves, 174, Rio de Janeiro, Brazil",
            morada2: "249",
            population: 500 ,codPostal: "2621-3262 / 7834553822"
        }, {
            lat: -21.5429328,
            lng: -42.1816803,
            nome: "Drogarias Viva Mais",
            morada1: "Av. Dr. Temistocles de Almeida, 3, Rio de Janeiro, Brazil",
            morada2: "250",
            population: 500 ,codPostal: "3851 0521 / 3853 239822"
        }, {
            lat: -22.8209413,
            lng: -43.0439688,
            nome: "Drogarias Viva Mais",
            morada1: "Av. Dezoito do Forte, 570, Rio de Janeiro, Brazil",
            morada2: "252",
            population: 500 ,codPostal: "3716-100021"
        }, {
            lat: -22.8363262,
            lng: -43.3509661,
            nome: "Drogarias Viva Mais",
            morada1: "R. Imboassu,172, Rio de Janeiro, Brazil",
            morada2: "253",
            population: 500 ,codPostal: "2607-142721"
        }, {
            lat: -22.8225835,
            lng: -42.9987391,
            nome: "Drogarias Viva Mais",
            morada1: "Est. Raul Veiga, 356 Lj. 03, Rio de Janeiro, Brazil",
            morada2: "254",
            population: 500 ,codPostal: "2620 1185 / 2622 606021"
        }, {
            lat: -22.8272829,
            lng: -43.0611664,
            nome: "Drogarias Viva Mais",
            morada1: "R. Doutor Francisco Portela, 2541 - Lt. 9, Rio de Janeiro, Brazil",
            morada2: "255",
            population: 500 ,codPostal: "2607 1555 /2606 077721"
        }, {
            lat: -22.8286721,
            lng: -43.0319868,
            nome: "Drogarias Viva Mais",
            morada1: "R. Guilherme dos Santos Andrade, 255, Rio de Janeiro, Brazil",
            morada2: "257",
            population: 500 ,codPostal: "3705 300021"
        }, {
            lat: -22.8460384,
            lng: -43.053308,
            nome: "Drogarias Viva Mais",
            morada1: "Est. Mentor Couto, 4678 L.1 e 2 - Q.7, Rio de Janeiro, Brazil",
            morada2: "258",
            population: 500 ,codPostal: "3713-500021"
        }, {
            lat: -22.804913,
            lng: -42.966503,
            nome: "Drogarias Viva Mais",
            morada1: "Av. Pres.Roosevelt,326 lj. 1, Rio de Janeiro, Brazil",
            morada2: "259",
            population: 500 ,codPostal: "3712-656121"
        }, {
            lat: -22.8380581,
            lng: -43.0127902,
            nome: "Drogarias Viva Mais",
            morada1: "Av. Jos? Mendon?a de Campos, 1.225, Rio de Janeiro, Brazil",
            morada2: "260",
            population: 500 ,codPostal: "2701-921521"
        }, {
            lat: -22.8216695,
            lng: -43.0006553,
            nome: "Drogarias Viva Mais",
            morada1: "Rua Manoel Jo?o Gon?alves, 198 Lj. 01, Rio de Janeiro, Brazil",
            morada2: "261",
            population: 500 ,codPostal: "3715-206621"
        }, {
            lat: -22.8177425,
            lng: -43.0196732,
            nome: "Drogarias Viva Mais",
            morada1: "Rua Doutor Alfredo Backer,1579 Bl A 4 LJ 3, Rio de Janeiro, Brazil",
            morada2: "262",
            population: 500 ,codPostal: "2614-500021"
        }, {
            lat: -22.8077906,
            lng: -43.0160192,
            nome: "Drogarias Viva Mais",
            morada1: "Av. Dr. Humberto Soeiro de Carvalho, 446 lj. 02, Rio de Janeiro, Brazil",
            morada2: "263",
            population: 500 ,codPostal: "2701-800021"
        }, {
            lat: -22.9239134,
            lng: -43.2371702,
            nome: "Drogarias Viva Mais",
            morada1: "Est. do Coelho,1.226 lt. 6, Rio de Janeiro, Brazil",
            morada2: "265",
            population: 500 ,codPostal: "2702100021"
        }, {
            lat: -22.9016724,
            lng: -43.1765305,
            nome: "Drogarias Viva Mais",
            morada1: "Rua Almirante Buenos Aires, 215 Lj. 1 e 2, Rio de Janeiro, Brazil",
            morada2: "266",
            population: 500 ,codPostal: "3611-8600 / 2616-847621"
        }, {
            lat: -22.7958505,
            lng: -43.0454065,
            nome: "Drogarias Viva Mais",
            morada1: "Av. Porto do Rosa,1266 Lj.1, Rio de Janeiro, Brazil",
            morada2: "267",
            population: 500 ,codPostal: "3614 2837 / 2723 082321"
        }, {
            lat: -22.8790075,
            lng: -43.088987,
            nome: "Drogarias Viva Mais",
            morada1: "Rua Alzira Vargas Peixoto,130 LJ. 04 e 05, Rio de Janeiro, Brazil",
            morada2: "268",
            population: 500 ,codPostal: "3713-722221"
        }, {
            lat: -22.8091091,
            lng: -43.0382417,
            nome: "Drogarias Viva Mais",
            morada1: "Av. Paulo Lemos, 15 Lj. 01, Rio de Janeiro, Brazil",
            morada2: "269",
            population: 500 ,codPostal: "2713 976921"
        }, {
            lat: -22.8548908,
            lng: -43.0816872,
            nome: "Drogarias Viva Mais",
            morada1: "Est. Covanca da Concei??o,882 LJ. 104, Rio de Janeiro, Brazil",
            morada2: "270",
            population: 500 ,codPostal: "3119-223321"
        }, {
            lat: -22.8196163,
            lng: -43.3817429,
            nome: "Drogarias Viva Mais",
            morada1: "Rua Cel. Moreira C?sar, 25 Lj. 1, Rio de Janeiro, Brazil",
            morada2: "272",
            population: 500 ,codPostal: "2606-8000 / 2712-703621"
        }, {
            lat: -22.8410044,
            lng: -43.37113,
            nome: "Drogarias Viva Mais",
            morada1: "R. Francisco Portela, 6, Rio de Janeiro, Brazil",
            morada2: "273",
            population: 500 ,codPostal: "2605-0110 / 2605-795021"
        }, {
            lat: -22.8308382,
            lng: -43.2842582,
            nome: "Drogarias Viva Mais",
            morada1: "Av. Visconde de Santar?m, 35, Rio de Janeiro, Brazil",
            morada2: "274",
            population: 500 ,codPostal: "2601-314821"
        }, {
            lat: -22.9189939,
            lng: -43.2542097,
            nome: "Drogarias Ofert?o",
            morada1: "Rua Teodoro da Silva, 774 Loja A , Rio de Janeiro, Brazil",
            morada2: "275",
            population: 500 ,codPostal: "2577-6000 / 2577-7362"
        }, {
            lat: -22.9240021,
            lng: -43.2367199,
            nome: "Drogarias Ofert?o",
            morada1: "Rua Bar?o de Mesquita, 356 Loja D , Rio de Janeiro, Brazil",
            morada2: "276",
            population: 500 ,codPostal: "2571-7000"
        }, {
            lat: -22.91999,
            lng: -43.234004,
            nome: "Drogarias Ofert?o",
            morada1: "Rua Major ?vila, 455 Loja B e C , Rio de Janeiro, Brazil",
            morada2: "277",
            population: 500 ,codPostal: "2254-3180"
        }, {
            lat: -22.9370929,
            lng: -43.2454088,
            nome: "Drogarias Ofert?o",
            morada1: "Rua Conde de Bonfim, 879 , Rio de Janeiro, Brazil",
            morada2: "278",
            population: 500 ,codPostal: "2238-0123 / 2570-2472"
        }, {
            lat: -22.9230548,
            lng: -43.3778801,
            nome: "Drogarias Ofert?o",
            morada1: "Estr. Rodrigues Caldas, 305 Lj B , Rio de Janeiro, Brazil",
            morada2: "279",
            population: 500 ,codPostal: "2445-5000"
        }, {
            lat: -22.8561149,
            lng: -43.3289756,
            nome: "Drogarias Ofert?o",
            morada1: "Av. Monsenhor F?lix, 28 Loja B , Rio de Janeiro, Brazil",
            morada2: "280",
            population: 500 ,codPostal: "3013-7108"
        }, {
            lat: -22.8999958,
            lng: -43.2261812,
            nome: "Drogarias Ofert?o",
            morada1: "Rua S?o Luiz Gonzaga, 364 , Rio de Janeiro, Brazil",
            morada2: "282",
            population: 500 ,codPostal: "2589-2020"
        }, {
            lat: -22.8755027,
            lng: -43.4871625,
            nome: "Drogarias Ofert?o",
            morada1: "Rua Ubat?, 50 loja , Rio de Janeiro, Brazil",
            morada2: "283",
            population: 500 ,codPostal: "3336-4590"
        }, {
            lat: -22.8838421,
            lng: -43.4064505,
            nome: "Drogarias Ofert?o",
            morada1: "Av. Marechal Fontenele, 3.784 Lj B , Rio de Janeiro, Brazil",
            morada2: "284",
            population: 500 ,codPostal: "3482-8000"
        }, {
            lat: -22.8806898,
            lng: -43.4286339,
            nome: "Drogarias Ofert?o",
            morada1: "Rua Capit?o Teixeira, 15 Loja , Rio de Janeiro, Brazil",
            morada2: "285",
            population: 500 ,codPostal: "3337-2525 / 3337-2416"
        }, {
            lat: -22.8873849,
            lng: -43.4356956,
            nome: "Drogarias Ofert?o",
            morada1: "R. dos Limites, 1250 Loja C , Rio de Janeiro, Brazil",
            morada2: "286",
            population: 500 ,codPostal: "3335-8000 / 2401-2060"
        }, {
            lat: -22.8774647,
            lng: -43.4439557,
            nome: "Drogarias Ofert?o",
            morada1: "R. dos Limites, 1404 A , Rio de Janeiro, Brazil",
            morada2: "287",
            population: 500 ,codPostal: "3482-0010"
        }, {
            lat: -22.9023127,
            lng: -43.2560735,
            nome: "Drogarias Ofert?o",
            morada1: "R. 24 de maio, 441Loja , Rio de Janeiro, Brazil",
            morada2: "288",
            population: 500 ,codPostal: "2501-1101"
        }, {
            lat: -22.8877274,
            lng: -43.2965895,
            nome: "Drogarias Ofert?o",
            morada1: "Rua da Aboli??o, 383 , Rio de Janeiro, Brazil",
            morada2: "289",
            population: 500 ,codPostal: "2229-9000"
        }, {
            lat: -22.9252668,
            lng: -43.2499752,
            nome: "Drogarias Ofert?o",
            morada1: "Rua Bar?o de Mesquita, 758 Loja B , Rio de Janeiro, Brazil",
            morada2: "291",
            population: 500 ,codPostal: "2575-5874"
        }, {
            lat: -22.8755907,
            lng: -43.3140929,
            nome: "Drogarias Ofert?o",
            morada1: "Rua Gra?a Melo, 296 Loja A , Rio de Janeiro, Brazil",
            morada2: "292",
            population: 500 ,codPostal: "3276-5302"
        }, {
            lat: -22.8320327,
            lng: -43.3460023,
            nome: "Drogarias Ofert?o",
            morada1: "Av. dos Italianos, 1420 - Loja A , Rio de Janeiro, Brazil",
            morada2: "293",
            population: 500 ,codPostal: "2473-2650 / 2473-2651"
        }, {
            lat: -22.919842,
            lng: -43.2610054,
            nome: "Drogarias Ofert?o",
            morada1: "Rua Br. do Bom Retiro, 2254 , Rio de Janeiro, Brazil",
            morada2: "294",
            population: 500 ,codPostal: "2576-6000"
        }, {
            lat: -22.9128475,
            lng: -43.2668453,
            nome: "Drogarias Ofert?o",
            morada1: "Rua Br. do Bom Retiro, 1184 Lj B C , Rio de Janeiro, Brazil",
            morada2: "295",
            population: 500 ,codPostal: "2201-1998"
        }, {
            lat: -22.8654214,
            lng: -43.2930471,
            nome: "Drogarias Ofert?o",
            morada1: "Est Adhemar Bebiano, 3987 Bl A Loja C , Rio de Janeiro, Brazil",
            morada2: "296",
            population: 500 ,codPostal: "2229-2555"
        }, {
            lat: -22.9584874,
            lng: -43.3516795,
            nome: "Drogarias Ofert?o",
            morada1: "Av. das Lagoas, 2011 - Jacarepagu? , Rio de Janeiro, Brazil",
            morada2: "297",
            population: 500 ,codPostal: "3342-3434 "
        }, {
            lat: -22.9635238,
            lng: -43.3492974,
            nome: "Drogarias Ofert?o",
            morada1: "Av. Isabel Domingues S/N Lt 24 Qd 3 Loja B , Rio de Janeiro, Brazil",
            morada2: "298",
            population: 500 ,codPostal: "2426-0760"
        }, {
            lat: -22.9219343,
            lng: -43.2645979,
            nome: "Drogarias Ofert?o",
            morada1: "Av. Julio Furtado, 108 A Loja 2 , Rio de Janeiro, Brazil",
            morada2: "299",
            population: 500 ,codPostal: "2576-3000 "
        }, {
            lat: -22.8405363,
            lng: -43.3776043,
            nome: "Drogarias Ofert?o",
            morada1: "Rua Marcos de Macedo, n? 120 Lj C , Rio de Janeiro, Brazil",
            morada2: "300",
            population: 500 ,codPostal: "3106-3939 / 3106-8586"
        }, {
            lat: -22.8365295,
            lng: -43.375004,
            nome: "Drogarias Ofert?o",
            morada1: "Rua Dr. Heliodoro Balbi, 517 , Rio de Janeiro, Brazil",
            morada2: "301",
            population: 500 ,codPostal: "3106-4050"
        }, {
            lat: -22.877849,
            lng: -43.2616235,
            nome: "Drogarias Ofert?o",
            morada1: "Rua F?lix Ferreira, 45 , Rio de Janeiro, Brazil",
            morada2: "303",
            population: 500 ,codPostal: "2564-6000"
        }, {
            lat: -22.8451896,
            lng: -43.3566668,
            nome: "Drogarias Ofert?o",
            morada1: "Rua Urura?, 1246 Lojas A e B , Rio de Janeiro, Brazil",
            morada2: "304",
            population: 500 ,codPostal: "2471-0707"
        }, {
            lat: -22.9174062,
            lng: -43.6823673,
            nome: "Drogarias Ofert?o",
            morada1: "Rua Padre Janu?rio, 43 , Rio de Janeiro, Brazil",
            morada2: "305",
            population: 500 ,codPostal: "3899-1143"
        }, {
            lat: -22.823799,
            lng: -43.3319325,
            nome: "Drogarias Ofert?o",
            morada1: "Av. Brasil, 17.810 Loja B , Rio de Janeiro, Brazil",
            morada2: "306",
            population: 500 ,codPostal: "3371-6102"
        }, {
            lat: -22.8379592,
            lng: -43.3242977,
            nome: "Drogarias Ofert?o",
            morada1: "Av. Monsenhor F?lix, 926 Loja F , Rio de Janeiro, Brazil",
            morada2: "307",
            population: 500 ,codPostal: "2539-4000"
        }, {
            lat: -22.987284,
            lng: -43.3019555,
            nome: "Drogarias Ofert?o",
            morada1: "Est. da Barra da Tijuca, 3.140 Loja C , Rio de Janeiro, Brazil",
            morada2: "309",
            population: 500 ,codPostal: "2495-3638"
        }, {
            lat: -22.8089616,
            lng: -43.3216757,
            nome: "Drogarias Ofert?o",
            morada1: "R. Franz Liszt, 526 Loja , Rio de Janeiro, Brazil",
            morada2: "310",
            population: 500 ,codPostal: "3346-4000"
        }, {
            lat: -22.9630899,
            lng: -43.1708546,
            nome: "Drogarias Ofert?o",
            morada1: "Rua Gustavo Sampaio, 576 Loja B , Rio de Janeiro, Brazil",
            morada2: "311",
            population: 500 ,codPostal: "2275-5000 / 2275-2349"
        }, {
            lat: -22.9154959,
            lng: -43.2316185,
            nome: "Drogarias Ofert?o",
            morada1: "Rua S?o Francisco Xavier, 358 - Loja A , Rio de Janeiro, Brazil",
            morada2: "312",
            population: 500 ,codPostal: "2569-4000 / 2569-3731"
        }, {
            lat: -22.9100175,
            lng: -43.287651,
            nome: "Drogarias Ofert?o",
            morada1: "Rua Pedro de Carvalho, 580 Loja A e B , Rio de Janeiro, Brazil",
            morada2: "314",
            population: 500 ,codPostal: "2594-5758 / 3275-5387"
        }, {
            lat: -22.8986303,
            lng: -43.2775381,
            nome: "Drogarias Ofert?o",
            morada1: "Rua Aristides Caire, n? 102 , Rio de Janeiro, Brazil",
            morada2: "315",
            population: 500 ,codPostal: "2241-2522 / 2241-3748"
        }, {
            lat: -22.9039814,
            lng: -43.2956164,
            nome: "Drogarias Ofert?o",
            morada1: "Rua Dias da Cruz, 768 Loja B , Rio de Janeiro, Brazil",
            morada2: "316",
            population: 500 ,codPostal: "2595-8680 / 2595-4566"
        }, {
            lat: -22.8500998,
            lng: -43.2652288,
            nome: "Drogarias Ofert?o",
            morada1: "R. Uranos, 1298 Lj A , Rio de Janeiro, Brazil",
            morada2: "317",
            population: 500 ,codPostal: "3281-7000 / 2561-3305 "
        }, {
            lat: -22.8132403,
            lng: -43.3742331,
            nome: "Drogarias Ofert?o",
            morada1: "Av. Professor Bernardino, 113 Lj B , Rio de Janeiro, Brazil",
            morada2: "318",
            population: 500 ,codPostal: "3019-8876 / 2455-7105"
        }, {
            lat: -22.8196925,
            lng: -43.3598446,
            nome: "Drogarias Ofert?o",
            morada1: "Est. de Botafogo,1223 Loja A , Rio de Janeiro, Brazil",
            morada2: "319",
            population: 500 ,codPostal: "2474-6899"
        }, {
            lat: -22.8850726,
            lng: -43.3104208,
            nome: "Drogarias Ofert?o",
            morada1: "Av. Dom Helder C?mara, 8644 , Rio de Janeiro, Brazil",
            morada2: "320",
            population: 500 ,codPostal: "3979-9800"
        }, {
            lat: -22.8863862,
            lng: -43.3026559,
            nome: "Drogarias Ofert?o",
            morada1: "Av. Dom Helder C?mara, 7797 loja , Rio de Janeiro, Brazil",
            morada2: "321",
            population: 500 ,codPostal: "3899-8982"
        }, {
            lat: -22.9246858,
            lng: -43.6726877,
            nome: "Drogarias Ofert?o",
            morada1: "Rua N S das Gra?as, 1.299 Loja A , Rio de Janeiro, Brazil",
            morada2: "322",
            population: 500 ,codPostal: "2560-5211 / 2573-9157"
        }, {
            lat: -22.8677819,
            lng: -43.4411967,
            nome: "Drogarias Ofert?o",
            morada1: "Rua Nil?polis, 107 , Rio de Janeiro, Brazil",
            morada2: "323",
            population: 500 ,codPostal: "3463-5000 / 3159-5692"
        }, {
            lat: -22.8687195,
            lng: -43.4395317,
            nome: "Drogarias Ofert?o",
            morada1: "Est. da ?gua Branca, 2430 Loja E , Rio de Janeiro, Brazil",
            morada2: "324",
            population: 500 ,codPostal: "3463-0806 / 3463-0812 "
        }, {
            lat: -22.8382177,
            lng: -43.0647715,
            nome: "Drogarias Ofert?o",
            morada1: "Rua Doutor Getulio Vargas, 2230 , Rio de Janeiro, Brazil",
            morada2: "325",
            population: 500 ,codPostal: "2605-9447/2605-9443"
        }, {
            lat: -22.8438643,
            lng: -43.0738613,
            nome: "Drogarias Ofert?o",
            morada1: "Rua Dr. Pio Borges , 2743 , Rio de Janeiro, Brazil",
            morada2: "326",
            population: 500 ,codPostal: "2720-1290 / 2624-1305"
        }, {
            lat: -22.9036294,
            lng: -43.1906091,
            nome: "Drogarias Ofert?o",
            morada1: "Rua Cristiano Otoni, 40 Lj 3 , Rio de Janeiro, Brazil",
            morada2: "327",
            population: 500 ,codPostal: "2601-4996"
        }, {
            lat: -22.8219554,
            lng: -43.0210092,
            nome: "Drogarias Ofert?o",
            morada1: "Av. Jos? Mendon?a de Campos, 210 , Rio de Janeiro, Brazil",
            morada2: "328",
            population: 500 ,codPostal: "3710-6700"
        }, {
            lat: -22.8241722,
            lng: -43.0693573,
            nome: "Drogarias Ofert?o",
            morada1: "Rua Capit?o Jo?o Manoel, 1824 , Rio de Janeiro, Brazil",
            morada2: "329",
            population: 500 ,codPostal: "2605-5746 / 2724-1767"
        }, {
            lat: -22.114628,
            lng: -43.2057823,
            nome: "Drogarias Ofert?o",
            morada1: "Av. Condessa do Rio Novo, 1419 , Rio de Janeiro, Brazil",
            morada2: "330",
            population: 500 ,codPostal: "2252-093024"
        }, {
            lat: -22.115257,
            lng: -43.2079054,
            nome: "Drogarias Ofert?o",
            morada1: "R. Dr Walmir Pe?anha, 64 Lj C, D , Rio de Janeiro, Brazil",
            morada2: "331",
            population: 500 ,codPostal: "2255-454324"
        }, {
            lat: -22.779988,
            lng: -43.382836,
            nome: "Drogarias Ofert?o",
            morada1: "Av. Ex-Combatentes, 30 , Rio de Janeiro, Brazil",
            morada2: "333",
            population: 500 ,codPostal: "3337- 455124"
        }, {
            lat: -21.2008674,
            lng: -41.8979281,
            nome: "Drogarias Ofert?o",
            morada1: "Av: Presidente Dutra - 543 LJ 1 , Rio de Janeiro, Brazil",
            morada2: "334",
            population: 500 ,codPostal: "3822-200022"
        }, {
            lat: -22.8609003,
            lng: -43.1023093,
            nome: "Drogarias Ofert?o",
            morada1: "Rua General Castrioto, 494 , Rio de Janeiro, Brazil",
            morada2: "335",
            population: 500 ,codPostal: "3620-3636 / 3620-313121"
        }, {
            lat: -22.388161,
            lng: -43.1323499,
            nome: "Drogarias Ofert?o",
            morada1: "Estrada Uni?o e Ind?stria, 11.755 Loja 1 , Rio de Janeiro, Brazil",
            morada2: "338",
            population: 500 ,codPostal: "2222-256624"
        }, {
            lat: -22.4895091,
            lng: -43.1537441,
            nome: "Drogarias Ofert?o",
            morada1: "Rua Quissam?, 2.066 , Rio de Janeiro, Brazil",
            morada2: "339",
            population: 500 ,codPostal: "2249-3844 / 2249-393324"
        }, {
            lat: -22.7719442,
            lng: -43.3645244,
            nome: "Drogarias Ofert?o",
            morada1: "Est. Severino M. de Araujo, 170 - Lj B , Rio de Janeiro, Brazil",
            morada2: "340",
            population: 500 ,codPostal: "2761-302021"
        }, {
            lat: -22.7514031,
            lng: -43.3038865,
            nome: "Drogarias Ofert?o",
            morada1: "Av. Gal Rondon, 08 Lj C , Rio de Janeiro, Brazil",
            morada2: "342",
            population: 500 ,codPostal: "3777-227721"
        }, {
            lat: -22.7982595,
            lng: -43.3051747,
            nome: "Drogarias Ofert?o",
            morada1: "Av Gov Leonel de Moura Brizola, S/N Qd 09 Lt 03 , Rio de Janeiro, Brazil",
            morada2: "343",
            population: 500 ,codPostal: "3134-100021"
        }, {
            lat: -22.8776047,
            lng: -43.4634832,
            nome: "Drogaria Galanti",
            morada1: "Rua Silva Cardoso, n? 229 , Rio de Janeiro, Brazil",
            morada2: "345",
            population: 500 ,codPostal: "2401-8152 / 2401-826621"
        }, {
            lat: -22.7640267,
            lng: -43.4008618,
            nome: "Drogaria Galanti",
            morada1: "Rua Jo?o Fernandes Neto, 1305 - 1307, Rio de Janeiro, Brazil",
            morada2: "346",
            population: 500 ,codPostal: "3771-1482 / 3663-249821"
        }, {
            lat: -22.9056084,
            lng: -43.5595536,
            nome: "Drogaria Galanti",
            morada1: "Rua Coronel Agostinho, n? 153, Rio de Janeiro, Brazil",
            morada2: "347",
            population: 500 ,codPostal: "2415-296821"
        }, {
            lat: -22.9069711,
            lng: -43.1764689,
            nome: "Drogaria Galanti",
            morada1: "Rua da Ajuda, n? 35, Rio de Janeiro, Brazil",
            morada2: "348",
            population: 500 ,codPostal: "2533-2715 / 2533-271721"
        }, {
            lat: -22.970499,
            lng: -43.1864322,
            nome: "Drogaria Galanti",
            morada1: "Avenida Nossa Senhora de Copacabana, n? 656, Rio de Janeiro, Brazil",
            morada2: "349",
            population: 500 ,codPostal: "2255-1770 / 2255-052921"
        }, {
            lat: -22.9647079,
            lng: -43.1794857,
            nome: "Drogaria Galanti",
            morada1: "Rua Barata Ribeiro n? 157, Rio de Janeiro, Brazil",
            morada2: "350",
            population: 500 ,codPostal: "2541-8845 / 2244-018021"
        }, {
            lat: -22.7882319,
            lng: -43.3117299,
            nome: "Drogaria Galanti",
            morada1: "Rua Jos? de Alvarenga, n? 405, Rio de Janeiro, Brazil",
            morada2: "351",
            population: 500 ,codPostal: "2771-0003 / 2771-558621"
        }, {
            lat: -22.9109132,
            lng: -43.2121549,
            nome: "Drogaria Galanti",
            morada1: "Avenida Presidente Vargas, n? 59, Rio de Janeiro, Brazil",
            morada2: "352",
            population: 500 ,codPostal: "2671-248121"
        }, {
            lat: -22.8747143,
            lng: -43.3372605,
            nome: "Drogaria Galanti",
            morada1: "Avenida Ministro Edgar Romero, n? 72, Rio de Janeiro, Brazil",
            morada2: "353",
            population: 500 ,codPostal: "3390-5010 / 3390-039621"
        }, {
            lat: -22.8746332,
            lng: -43.3378817,
            nome: "Drogaria Galanti",
            morada1: "Rua Dagmar da Fonseca, 19, Rio de Janeiro, Brazil",
            morada2: "354",
            population: 500 ,codPostal: "2450-168421"
        }, {
            lat: -22.8088497,
            lng: -43.4176297,
            nome: "Drogaria Galanti",
            morada1: "Avenida Get?lio Vargas, n? 1476, Rio de Janeiro, Brazil",
            morada2: "355",
            population: 500 ,codPostal: "3760-2811 / 3762-358221"
        }, {
            lat: -22.8949745,
            lng: -43.1227934,
            nome: "Drogaria Galanti",
            morada1: "Rua Jos? Clemente n? 42, Rio de Janeiro, Brazil",
            morada2: "356",
            population: 500 ,codPostal: "2620-1200 / 2620-119421"
        }, {
            lat: -22.8084159,
            lng: -43.4159728,
            nome: "Drogaria Galanti",
            morada1: "Avenida Mirandela, n? 35, Rio de Janeiro, Brazil",
            morada2: "357",
            population: 500 ,codPostal: "3760-074521"
        }, {
            lat: -22.7580691,
            lng: -43.4490148,
            nome: "Drogaria Galanti",
            morada1: "Travessa Almerinda Lucas de Azeredo, n? 70, Rio de Janeiro, Brazil",
            morada2: "358",
            population: 500 ,codPostal: "2667-8388 / 2669-267821"
        }, {
            lat: -22.7594822,
            lng: -43.4491181,
            nome: "Drogaria Galanti",
            morada1: "Avenida Governador Amaral Peixoto, n? 142, Rio de Janeiro, Brazil",
            morada2: "359",
            population: 500 ,codPostal: "2668-3073 / 2668-409221"
        }, {
            lat: -22.8471961,
            lng: -43.2856509,
            nome: "Drogaria Galanti",
            morada1: "Rua Get?lio Vargas, n? 90, Rio de Janeiro, Brazil",
            morada2: "360",
            population: 500 ,codPostal: "2768-7599 / 2768-858521"
        }, {
            lat: -22.7588354,
            lng: -43.450544,
            nome: "Drogaria Galanti",
            morada1: "Avenida Governador Amaral Peixoto, n? 324, Rio de Janeiro, Brazil",
            morada2: "361",
            population: 500 ,codPostal: "2667-4810 / 2667-386821"
        }, {
            lat: -22.997726,
            lng: -43.3875618,
            nome: "Drogaria Galanti",
            morada1: "Avenida Doutor Luiz Guimar?es, n? 153, Rio de Janeiro, Brazil",
            morada2: "362",
            population: 500 ,codPostal: "2768-4880 / 2768-437721"
        }, {
            lat: -22.9196268,
            lng: -43.2588234,
            nome: "Drogaria Galanti",
            morada1: "Avenida Doutor Luiz Guimar?es, n? 93, Rio de Janeiro, Brazil",
            morada2: "363",
            population: 500 ,codPostal: "2768-138721"
        }, {
            lat: -22.7596204,
            lng: -43.4494988,
            nome: "Drogaria Galanti",
            morada1: "Rua Coronel Francisco Soares, n? 134, Rio de Janeiro, Brazil",
            morada2: "364",
            population: 500 ,codPostal: "2765-3362 / 2765-342421"
        }, {
            lat: -22.9075989,
            lng: -43.1841961,
            nome: "Drogaria Galanti",
            morada1: "Pra?a Visconde do Rio Branco, n? 16, Rio de Janeiro, Brazil",
            morada2: "365",
            population: 500 ,codPostal: "2237-6009 / 2237-849924"
        }, {
            lat: -22.5105764,
            lng: -43.1780081,
            nome: "Drogaria Galanti",
            morada1: "Rua Dezesseis de Mar?o, n? 175, Rio de Janeiro, Brazil",
            morada2: "366",
            population: 500 ,codPostal: "2242-9362 / 2242-716224"
        }, {
            lat: -22.5103507,
            lng: -43.1763905,
            nome: "Drogaria Galanti",
            morada1: "Rua do Imperador, n? 728, Rio de Janeiro, Brazil",
            morada2: "367",
            population: 500 ,codPostal: "2247-700024"
        }, {
            lat: -22.7164153,
            lng: -43.5561037,
            nome: "Drogaria Galanti",
            morada1: "Avenida Irm?os Guinle, n? 1037, Rio de Janeiro, Brazil",
            morada2: "368",
            population: 500 ,codPostal: "2665-5831 / 2665-584721"
        }, {
            lat: -22.5076001,
            lng: -43.1702847,
            nome: "Drogaria Galanti",
            morada1: "Rua do Imperador, n? 9, Rio de Janeiro, Brazil",
            morada2: "369",
            population: 500 ,codPostal: "2243-7252 / 2245-072124"
        }, {
            lat: -22.8230293,
            lng: -43.0459145,
            nome: "Drogaria Galanti",
            morada1: "Pra?a Luiz Palmier, n? 50, Rio de Janeiro, Brazil",
            morada2: "370",
            population: 500 ,codPostal: "2606-3578 / 2606-315621"
        }, {
            lat: -22.8832271,
            lng: -43.6142454,
            nome: "Drogaria Galanti",
            morada1: "Rua Dezesseis, n? 79, Rio de Janeiro, Brazil",
            morada2: "372",
            population: 500 ,codPostal: "3342-6284 / 3348-121624"
        }, {
            lat: -22.7457594,
            lng: -43.4890526,
            nome: "Drogarias Poupe Mais",
            morada1: "Rua Leonel Gouveia, 324 loja, Rio de Janeiro, Brazil",
            morada2: "374",
            population: 500 ,codPostal: "3102-400021"
        }, {
            lat: -22.0568571,
            lng: -42.5187652,
            nome: "Drogarias Poupe Mais",
            morada1: "R. Dr. Feliciano Sodre, 137, Rio de Janeiro, Brazil",
            morada2: "375",
            population: 500 ,codPostal: "2607-700021"
        }, {
            lat: -22.7143492,
            lng: -42.644432,
            nome: "Drogarias Poupe Mais",
            morada1: "Rua Doutor Luiz Palmer, 60, Rio de Janeiro, Brazil",
            morada2: "376",
            population: 500 ,codPostal: "2604-428621"
        }, {
            lat: -22.8851177,
            lng: -43.1255765,
            nome: "Drogarias Poupe Mais",
            morada1: "Av. Visconde do Rio Branco, s/n - lj. 31/32, Rio de Janeiro, Brazil",
            morada2: "377",
            population: 500 ,codPostal: "2613-212021"
        }, {
            lat: -22.7803975,
            lng: -42.938764,
            nome: "Drogarias Poupe Mais",
            morada1: "Av. Afonso Salles,199 , Rio de Janeiro, Brazil",
            morada2: "378",
            population: 500 ,codPostal: "3638-0606/3638-539421"
        }, {
            lat: -22.8736545,
            lng: -43.0517371,
            nome: "Drogarias Poupe Mais",
            morada1: "Rod. Amaral Peixoto, 3677 Lj B - KM 29, Rio de Janeiro, Brazil",
            morada2: "379",
            population: 500 ,codPostal: "2645-300021"
        }, {
            lat: -22.8461184,
            lng: -43.3032573,
            nome: "Drogarias Poupe Mais",
            morada1: "Av. Vicente de Carvalho, 1213 lj. C, Rio de Janeiro, Brazil",
            morada2: "380",
            population: 500 ,codPostal: "2613-21203351-5787/3013-524721"
        }, {
            lat: -22.9051539,
            lng: -43.5596272,
            nome: "Drogaria Pop",
            morada1: "Rua Coronel Agostinho, 135, Rio de Janeiro, Brazil",
            morada2: "381",
            population: 500 ,codPostal: "2411-147221"
        }, {
            lat: -22.8921721,
            lng: -43.1234961,
            nome: "Drogaria Pop",
            morada1: "Av. Visconde do Rio Branco, 361, Rio de Janeiro, Brazil",
            morada2: "382",
            population: 500 ,codPostal: "2717-377921"
        }, {
            lat: -22.9056154,
            lng: -43.1792899,
            nome: "Drogaria Pop",
            morada1: "Rua Uruguaiana, 19, Rio de Janeiro, Brazil",
            morada2: "383",
            population: 500 ,codPostal: "2252-633221"
        }, {
            lat: -22.9704009,
            lng: -43.1885823,
            nome: "Drogaria Pop",
            morada1: "Rua Santa Clara, 127, Rio de Janeiro, Brazil",
            morada2: "384",
            population: 500 ,codPostal: "2545-200021"
        }, {
            lat: -22.9780647,
            lng: -43.1908387,
            nome: "Drogaria Pop",
            morada1: "Av. Nossa Senhora de Copacabana, 950, Rio de Janeiro, Brazil",
            morada2: "385",
            population: 500 ,codPostal: "2545-200021"
        }, {
            lat: -22.9780167,
            lng: -43.1909122,
            nome: "Drogaria Pop",
            morada1: "Av. Nossa Senhora de Copacabana, 1048, Rio de Janeiro, Brazil",
            morada2: "386",
            population: 500 ,codPostal: "2545-200021"
        }, {
            lat: -22.984276,
            lng: -43.200828,
            nome: "Drogaria Pop",
            morada1: "Rua Visconde de Piraj?, 200, Rio de Janeiro, Brazil",
            morada2: "387",
            population: 500 ,codPostal: "2525-400021"
        }, {
            lat: -22.9142896,
            lng: -43.2191917,
            nome: "Drogaria Pop",
            morada1: "Rua Mariz e Barros, 470-E, Rio de Janeiro, Brazil",
            morada2: "388",
            population: 500 ,codPostal: "2569-272721"
        }, {
            lat: -22.4745558,
            lng: -44.461237,
            nome: "Drogaria Pop",
            morada1: "Rua Feliciano Sodr?, 239, Rio de Janeiro, Brazil",
            morada2: "389",
            population: 500 ,codPostal: "2604-5054 21"
        }, {
            lat: -22.8733768,
            lng: -43.3376229,
            nome: "Drogaria Pop",
            morada1: "Estrada do Portela 29, loja H, Rio de Janeiro, Brazil",
            morada2: "390",
            population: 500 ,codPostal: "3350-646621"
        }, {
            lat: -22.7600983,
            lng: -43.4478481,
            nome: "Drogaria Pop",
            morada1: "Avenida Governador Amaral Peixoto, 6, Rio de Janeiro, Brazil",
            morada2: "391",
            population: 500 ,codPostal: "2668-270421"
        }, {
            lat: -22.7887338,
            lng: -43.3115426,
            nome: "Drogaria Pop",
            morada1: "Rua Jos? de Alvarenga, 288, Rio de Janeiro, Brazil",
            morada2: "392",
            population: 500 ,codPostal: "2671-810121"
        }, {
            lat: -22.8082142,
            lng: -43.416171,
            nome: "Drogaria Pop",
            morada1: "Av. Mirandela, 42, Rio de Janeiro, Brazil",
            morada2: "393",
            population: 500 ,codPostal: "2691-852321"
        }, {
            lat: -22.8034479,
            lng: -43.3702841,
            nome: "Drogaria Pop",
            morada1: "Rua Assia Tanus Bedran, 81, Rio de Janeiro, Brazil",
            morada2: "394",
            population: 500 ,codPostal: "2756-595821"
        }, {
            lat: -22.4703299,
            lng: -43.8257923,
            nome: "Drogarias Pov?o",
            morada1: "Pra?a Nilo Pe?anha, 54, Rio de Janeiro, Brazil",
            morada2: "395",
            population: 500 ,codPostal: "2443-985024"
        }, {
            lat: -22.7640383,
            lng: -43.4359673,
            nome: "Drogarias Pov?o",
            morada1: "Rua Governador Portela, 42, Rio de Janeiro, Brazil",
            morada2: "396",
            population: 500 ,codPostal: "2443-985024"
        }, {
            lat: -22.763957,
            lng: -43.4365178,
            nome: "Drogarias Pov?o",
            morada1: "Rua Governador Portela, 110, Rio de Janeiro, Brazil",
            morada2: "398",
            population: 500 ,codPostal: "2443-985024"
        }, {
            lat: -22.517229,
            lng: -44.106473,
            nome: "Drogarias Pov?o",
            morada1: "Rua General Oswaldo Pinto da Veiga (Rua 14), 26 Lj 214 , Rio de Janeiro, Brazil",
            morada2: "399",
            population: 500 ,codPostal: "3343-502524"
        }, {
            lat: -22.902679,
            lng: -43.281969,
            nome: "Drogarias Elite Farma",
            morada1: "Rua Dias da Cruz, N? 166 , Rio de Janeiro, Brazil",
            morada2: "400",
            population: 500 ,codPostal: "3272-100021"
        }, {
            lat: -22.8874328,
            lng: -43.3488974,
            nome: "Drogarias Elite Farma",
            morada1: "Rua Pinto Teles, N? 441, Rio de Janeiro, Brazil",
            morada2: "401",
            population: 500 ,codPostal: "3390-700021"
        }, {
            lat: -22.9388723,
            lng: -43.3410643,
            nome: "Drogarias Elite Farma",
            morada1: "Estrada dos Tr?s Rios, N? 271 , Rio de Janeiro, Brazil",
            morada2: "402",
            population: 500 ,codPostal: "2436-011121"
        }, {
            lat: -22.9182867,
            lng: -43.3736014,
            nome: "Drogarias Elite Farma",
            morada1: "Rua Bacairis, N? 464 / Lj ????, Rio de Janeiro, Brazil",
            morada2: "403",
            population: 500 ,codPostal: "2423-305121"
        }, {
            lat: -22.9520656,
            lng: -43.3383564,
            nome: "Drogarias Elite Farma",
            morada1: "Estrada de Jacarepagu?, N? 6508 , Rio de Janeiro, Brazil",
            morada2: "404",
            population: 500 ,codPostal: "3415-500021"
        }, {
            lat: -22.989312,
            lng: -43.3223171,
            nome: "Drogarias Elite Farma",
            morada1: "Rua Eng? Souza Filho, N? 190, Rio de Janeiro, Brazil",
            morada2: "405",
            population: 500 ,codPostal: "3116-000421"
        }, {
            lat: -22.8661371,
            lng: -43.2551692,
            nome: "Drogaria Na??es",
            morada1: "Pra?a das Na??es, 244, Rio de Janeiro, Brazil",
            morada2: "406",
            population: 500 ,codPostal: "3976-995021"
        }, {
            lat: -22.8640077,
            lng: -43.2546867,
            nome: "Drogaria Na??es",
            morada1: "Rua Cardoso de Morais, 97, Rio de Janeiro, Brazil",
            morada2: "407",
            population: 500 ,codPostal: "3865-000621"
        }, {
            lat: -22.9049745,
            lng: -43.1797223,
            nome: "Drogaria Na??es",
            morada1: "R. Uruguaiana, 39 Loja A, Rio de Janeiro, Brazil",
            morada2: "408",
            population: 500 ,codPostal: "3861-900321"
        }, {
            lat: -22.9033574,
            lng: -43.1788367,
            nome: "Drogaria Na??es",
            morada1: "R. do Ros?rio, 131, Rio de Janeiro, Brazil",
            morada2: "409",
            population: 500 ,codPostal: "3861-900221"
        }, {
            lat: -22.9217835,
            lng: -43.2586459,
            nome: "Drogaria Wilson",
            morada1: "Rua Bar?o do Retiro, 2593, Rio de Janeiro, Brazil",
            morada2: "410",
            population: 500 ,codPostal: "2577-884521"
        }, {
            lat: -22.901719,
            lng: -43.1812431,
            nome: "Drogaria Wilson",
            morada1: "Av. Pres. Vargas, 542, Rio de Janeiro, Brazil",
            morada2: "411",
            population: 500 ,codPostal: "2233-898921"
        }, {
            lat: -22.8995171,
            lng: -43.1777144,
            nome: "Drogaria Wilson",
            morada1: "Rua Candel?ria, 80, Rio de Janeiro, Brazil",
            morada2: "412",
            population: 500 ,codPostal: "2263-051821"
        }, {
            lat: -22.9029883,
            lng: -43.1773039,
            nome: "Drogaria Wilson",
            morada1: "Rua da Quitanda, 81, Rio de Janeiro, Brazil",
            morada2: "413",
            population: 500 ,codPostal: "2252-508521"
        }, {
            lat: -22.9839965,
            lng: -43.2136936,
            nome: "Drogaria Wilson",
            morada1: "Av. Henrique Dumont, 85, Rio de Janeiro, Brazil",
            morada2: "414",
            population: 500 ,codPostal: "2249-700021"
        }, {
            lat: -22.9689185,
            lng: -43.1857581,
            nome: "Drogaria Wilson",
            morada1: "Rua Barata Ribeiro, 411, Rio de Janeiro, Brazil",
            morada2: "415",
            population: 500 ,codPostal: "2255-400021"
        }, {
            lat: -22.9821688,
            lng: -43.2245662,
            nome: "Drogaria Wilson",
            morada1: "Rua Dias Ferreira, 618 Loja A , Rio de Janeiro, Brazil",
            morada2: "416",
            population: 500 ,codPostal: "2511-700021"
        }, {
            lat: -22.9359466,
            lng: -43.1892821,
            nome: "Drogaria Wilson",
            morada1: "Rua das Laranjeiras, 347, Rio de Janeiro, Brazil",
            morada2: "417",
            population: 500 ,codPostal: "2285-700021"
        }, {
            lat: -22.8280494,
            lng: -43.3456212,
            nome: "Drogarias Boa Sa?de",
            morada1: "Est. Engenheiro Edgard Soutello, 23 , Rio de Janeiro, Brazil",
            morada2: "418",
            population: 500 ,codPostal: "3448-246621"
        }, {
            lat: -22.9247592,
            lng: -43.243196,
            nome: "Drogarias Boa Sa?de",
            morada1: "Rua Bar?o de Mesquita, 605-A , Rio de Janeiro, Brazil",
            morada2: "419",
            population: 500 ,codPostal: "2572-5415 / 2288-676421"
        }, {
            lat: -22.9740114,
            lng: -43.3678359,
            nome: "Drogarias Boa Sa?de",
            morada1: "Av. Embaixador Abelardo Bueno, 199 , Rio de Janeiro, Brazil",
            morada2: "420",
            population: 500 ,codPostal: "2421-558221"
        }, {
            lat: -22.9625028,
            lng: -43.1766495,
            nome: "Drogarias Boa Sa?de",
            morada1: "Rua Barata Ribeiro, 32-Lj B , Rio de Janeiro, Brazil",
            morada2: "421",
            population: 500 ,codPostal: "2275-2343 / 2543-393621"
        }, {
            lat: -22.9641849,
            lng: -43.17502,
            nome: "Drogarias Boa Sa?de",
            morada1: "Av. Nossa Sra. De Copacabana, 98 ? lj A , Rio de Janeiro, Brazil",
            morada2: "422",
            population: 500 ,codPostal: "2541-460021"
        }, {
            lat: -22.9078438,
            lng: -43.190798,
            nome: "Drogarias Boa Sa?de",
            morada1: "Rua Moncorvo Filho, 17 Lj B , Rio de Janeiro, Brazil",
            morada2: "423",
            population: 500 ,codPostal: "3806-1861 / 2292-433921"
        }, {
            lat: -22.9144842,
            lng: -43.1895823,
            nome: "Drogarias Boa Sa?de",
            morada1: "Rua do Riachuelo, 267 ? A , Rio de Janeiro, Brazil",
            morada2: "424",
            population: 500 ,codPostal: "2232-8117 / 2232-055221"
        }, {
            lat: -22.8192427,
            lng: -43.3668484,
            nome: "Drogarias Boa Sa?de",
            morada1: "Rua Virgilio Filho, 192 ? Loja A , Rio de Janeiro, Brazil",
            morada2: "426",
            population: 500 ,codPostal: "2407-707921"
        }, {
            lat: -22.7919112,
            lng: -43.1689137,
            nome: "Drogarias Boa Sa?de",
            morada1: "Rua Comendador Bastos, 172 ? Lj F , Rio de Janeiro, Brazil",
            morada2: "427",
            population: 500 ,codPostal: "3396-2015 / 2466-901421"
        }, {
            lat: -22.8459925,
            lng: -43.3738064,
            nome: "Drogarias Boa Sa?de",
            morada1: "Rua Luiz Coutinho Cavalcante, 268 ? Lj A , Rio de Janeiro, Brazil",
            morada2: "428",
            population: 500 ,codPostal: "2458-519 /2451-379321"
        }, {
            lat: -22.9359722,
            lng: -43.666824,
            nome: "Drogarias Boa Sa?de",
            morada1: "Est. da Pedra, s/n -Qd.40 -Lt.01-Lot.13.768 , Rio de Janeiro, Brazil",
            morada2: "429",
            population: 500 ,codPostal: "3401-800021"
        }, {
            lat: -22.8880857,
            lng: -43.2596422,
            nome: "Drogarias Boa Sa?de",
            morada1: "Rua Darcy Vargas, 73 , Rio de Janeiro, Brazil",
            morada2: "430",
            population: 500 ,codPostal: "2581-842821"
        }, {
            lat: -22.873203,
            lng: -43.2706273,
            nome: "Drogarias Boa Sa?de",
            morada1: "Rua Chapadinha, 15 lj. A , Rio de Janeiro, Brazil",
            morada2: "431",
            population: 500 ,codPostal: "2260-0947 / 3884-621121"
        }, {
            lat: -22.8767048,
            lng: -43.3526316,
            nome: "Drogarias Boa Sa?de",
            morada1: "Rua Henrique Braga, 619 ? Lj A, Rio de Janeiro, Brazil",
            morada2: "432",
            population: 500 ,codPostal: "2464-759521"
        }, {
            lat: -22.8965125,
            lng: -43.364803,
            nome: "Drogarias Boa Sa?de",
            morada1: "Estrada da Ch?cara, 431 , Rio de Janeiro, Brazil",
            morada2: "433",
            population: 500 ,codPostal: "3444-448921"
        }, {
            lat: -22.8836167,
            lng: -43.3423189,
            nome: "Drogarias Boa Sa?de",
            morada1: "Rua C?ndido Ben?cio, s/n ? Rua 1, n 1 ? Lj. J, Rio de Janeiro, Brazil",
            morada2: "434",
            population: 500 ,codPostal: "3382-0233 | 3168-957921"
        }, {
            lat: -22.8505644,
            lng: -43.3262694,
            nome: "Drogarias Boa Sa?de",
            morada1: "Av. Monsenhor F?lix, 306 e 306 A , Rio de Janeiro, Brazil",
            morada2: "435",
            population: 500 ,codPostal: "3013-1420 / 2482-798721"
        }, {
            lat: -22.846845,
            lng: -43.2654195,
            nome: "Drogarias Boa Sa?de",
            morada1: "Rua Ang?lica Mota, 490 A , Rio de Janeiro, Brazil",
            morada2: "436",
            population: 500 ,codPostal: "3495-020022"
        }, {
            lat: -22.9108797,
            lng: -43.2119685,
            nome: "Drogarias Boa Sa?de",
            morada1: "Rua Presidente Vargas N?79, Lj.1 ? Sl j.l , Rio de Janeiro, Brazil",
            morada2: "437",
            population: 500 ,codPostal: "2533-175822"
        }, {
            lat: -22.9146281,
            lng: -43.6533935,
            nome: "Drogarias Boa Sa?de",
            morada1: "Rua Cilon Cunha Brum, 250 , Rio de Janeiro, Brazil",
            morada2: "438",
            population: 500 ,codPostal: "3395-723621"
        }, {
            lat: -22.7947249,
            lng: -43.296011,
            nome: "Drogarias Boa Sa?de",
            morada1: "Rua Evaristo da Veiga, 339 ? Loja A , Rio de Janeiro, Brazil",
            morada2: "439",
            population: 500 ,codPostal: "2673-053121"
        }, {
            lat: -22.9063773,
            lng: -43.1761852,
            nome: "Drogarias Boa Sa?de",
            morada1: "Av. Nilo Pe?anha, 2.492 , Rio de Janeiro, Brazil",
            morada2: "440",
            population: 500 ,codPostal: "2772-213321"
        }, {
            lat: -22.7036446,
            lng: -43.2796402,
            nome: "Drogarias Boa Sa?de",
            morada1: "Av. Actura, s/n? ? Lt. 08 ? Qd. 08 , Rio de Janeiro, Brazil",
            morada2: "441",
            population: 500 ,codPostal: "2776-200621"
        }, {
            lat: -22.8117559,
            lng: -43.6455928,
            nome: "Drogarias Boa Sa?de",
            morada1: "Rua Vicente Celestino, 1.026 ? Lj A , Rio de Janeiro, Brazil",
            morada2: "442",
            population: 500 ,codPostal: "2776-3561 / 2678-579921"
        }, {
            lat: -22.8028233,
            lng: -43.3141312,
            nome: "Drogarias Boa Sa?de",
            morada1: "Rua Vicente Celestino, 1.048 ? Lj A , Rio de Janeiro, Brazil",
            morada2: "443",
            population: 500 ,codPostal: "2678-754421"
        }, {
            lat: -22.979453,
            lng: -43.2936748,
            nome: "Drogarias Boa Sa?de",
            morada1: "Av. B, 480 ? Lj 2 , Rio de Janeiro, Brazil",
            morada2: "444",
            population: 500 ,codPostal: "2675-188621"
        }, {
            lat: -22.9196743,
            lng: -43.1762397,
            nome: "Drogarias Boa Sa?de",
            morada1: "Rua Rio de Janeiro, s/n Loja B , Rio de Janeiro, Brazil",
            morada2: "445",
            population: 500 ,codPostal: "3654-4016 / 3024-205721"
        }, {
            lat: -22.5551472,
            lng: -43.2560349,
            nome: "Drogarias Boa Sa?de",
            morada1: "Rod. Washington Luiz, 16.780 ? Loja H , Rio de Janeiro, Brazil",
            morada2: "446",
            population: 500 ,codPostal: "2776-400021"
        }, {
            lat: -22.9267893,
            lng: -43.2045402,
            nome: "Drogarias Boa Sa?de",
            morada1: "Rua Angustia, s/n? ? Lt 14 ? Qd 06 ? Lj , Rio de Janeiro, Brazil",
            morada2: "447",
            population: 500 ,codPostal: "2776-614921"
        }, {
            lat: -22.6773545,
            lng: -43.2481607,
            nome: "Drogarias Boa Sa?de",
            morada1: "Av. Presidente Roosevelt, s/n?-Qd. 1C-Lt.3 e 5-Lj. 4 , Rio de Janeiro, Brazil",
            morada2: "448",
            population: 500 ,codPostal: "3656-6039 / 2776-137721"
        }, {
            lat: -22.600688,
            lng: -43.2924322,
            nome: "Drogarias Boa Sa?de",
            morada1: "Trav. Nobrega Ribeiro, 17 , Rio de Janeiro, Brazil",
            morada2: "449",
            population: 500 ,codPostal: "3658-886221"
        }, {
            lat: -22.780347,
            lng: -42.938203,
            nome: "Drogarias Boa Sa?de",
            morada1: "Av. Afonso Salles, 155 ? Qd. E ? Apolo II , Rio de Janeiro, Brazil",
            morada2: "450",
            population: 500 ,codPostal: "3638-759221"
        }, {
            lat: -22.7753122,
            lng: -42.9221,
            nome: "Drogarias Boa Sa?de",
            morada1: "Av. Prefeito Milton Rodrigues da Rocha, 439 , Rio de Janeiro, Brazil",
            morada2: "451",
            population: 500 ,codPostal: "3638-336221"
        }, {
            lat: -22.7753122,
            lng: -42.9221,
            nome: "Drogarias Boa Sa?de",
            morada1: "Av. Prefeito Milton Rodrigues da Rocha, 439 , Rio de Janeiro, Brazil",
            morada2: "452",
            population: 500 ,codPostal: "3637-843721"
        }, {
            lat: -21.2033066,
            lng: -41.8916483,
            nome: "Drogarias Boa Sa?de",
            morada1: "Av. Cardoso Moreira, 860 , Rio de Janeiro, Brazil",
            morada2: "453",
            population: 500 ,codPostal: "3822-1007 / 3824-364622"
        }, {
            lat: -22.7117118,
            lng: -42.6373238,
            nome: "Drogarias Boa Sa?de",
            morada1: "Av. Cel. Balbino, 314 , Rio de Janeiro, Brazil",
            morada2: "454",
            population: 500 ,codPostal: "3847-256522"
        }, {
            lat: -21.1956138,
            lng: -41.896143,
            nome: "Drogarias Boa Sa?de",
            morada1: "Rua Ant?nio Francisco Rosa, 62 , Rio de Janeiro, Brazil",
            morada2: "455",
            population: 500 ,codPostal: "3822-299722"
        }, {
            lat: -22.9270277,
            lng: -43.1799911,
            nome: "Drogarias Boa Sa?de",
            morada1: "Rua Alferes Bastos, 18 , Rio de Janeiro, Brazil",
            morada2: "456",
            population: 500 ,codPostal: "3829-205822"
        }, {
            lat: -22.3145816,
            lng: -41.7185583,
            nome: "Drogarias Boa Sa?de",
            morada1: "Av. Doutor S?rgio Vieira de Melo, s/n? Q.30 L.381, Rio de Janeiro, Brazil",
            morada2: "457",
            population: 500 ,codPostal: "2770-507922"
        }, {
            lat: -22.7799234,
            lng: -43.3039969,
            nome: "Drogarias Boa Sa?de",
            morada1: "Av. Pres. Tancredo Neves, 238 , Rio de Janeiro, Brazil",
            morada2: "458",
            population: 500 ,codPostal: "2770-507922"
        }, {
            lat: -22.6092492,
            lng: -43.1784452,
            nome: "Drogarias Boa Sa?de",
            morada1: "Av. Santos Dumont, 177 , Rio de Janeiro, Brazil",
            morada2: "459",
            population: 500 ,codPostal: "2739-1614"
        }, {
            lat: -22.7835335,
            lng: -43.4304285,
            nome: "Drogarias Boa Sa?de",
            morada1: "Rua Em?lio Guadagny, 1.936 ? Lj B, Rio de Janeiro, Brazil",
            morada2: "460",
            population: 500 ,codPostal: "3763-2064 / 3763-254221"
        }, {
            lat: -22.8073397,
            lng: -43.4210713,
            nome: "Drogarias Boa Sa?de",
            morada1: "Rua Jo?o Pessoa, 1.740 ? Lj B , Rio de Janeiro, Brazil",
            morada2: "461",
            population: 500 ,codPostal: "2691-8958 / 2792-224921"
        }, {
            lat: -22.8070868,
            lng: -43.4136752,
            nome: "Drogarias Boa Sa?de",
            morada1: "Estr. General Mena Barreto, 125 loja , Rio de Janeiro, Brazil",
            morada2: "462",
            population: 500 ,codPostal: "2653-5330 / 2792-102521"
        }, {
            lat: -22.953238,
            lng: -43.09343,
            nome: "Drogarias Boa Sa?de",
            morada1: "Av. Almirante Tamandar?, 668 ? Lj 101 , Rio de Janeiro, Brazil",
            morada2: "463",
            population: 500 ,codPostal: "2619-1626 / 2619-9000 / 2619-600621"
        }, {
            lat: -22.7599655,
            lng: -43.4797362,
            nome: "Drogarias Boa Sa?de",
            morada1: "Av. Ministro Lafaiete de Andrade, 1.807 , Rio de Janeiro, Brazil",
            morada2: "464",
            population: 500 ,codPostal: "2669-8388 / 2667-449021"
        }, {
            lat: -22.5263335,
            lng: -43.1716512,
            nome: "Drogarias Boa Sa?de",
            morada1: "Rua Tereza, 1.713 , Rio de Janeiro, Brazil",
            morada2: "465",
            population: 500 ,codPostal: "2243-700024"
        }, {
            lat: -22.5138452,
            lng: -43.2224841,
            nome: "Drogarias Boa Sa?de",
            morada1: "Rua Doutor Paulo Herv?, 1.169 , Rio de Janeiro, Brazil",
            morada2: "466",
            population: 500 ,codPostal: "2235-685624"
        }, {
            lat: -22.513088,
            lng: -43.220612,
            nome: "Drogarias Boa Sa?de",
            morada1: "Rua Dr. Paulo Herv?, 108 , Rio de Janeiro, Brazil",
            morada2: "467",
            population: 500 ,codPostal: "2246-107524"
        }, {
            lat: -22.5116893,
            lng: -43.2020454,
            nome: "Drogarias Boa Sa?de",
            morada1: "Rua Bingen, 1.011 , Rio de Janeiro, Brazil",
            morada2: "468",
            population: 500 ,codPostal: "2247-291224"
        }, {
            lat: -22.8723176,
            lng: -43.3705351,
            nome: "Drogarias Boa Sa?de",
            morada1: "Rua Sta Rita de C?ssia, 400 , Rio de Janeiro, Brazil",
            morada2: "469",
            population: 500 ,codPostal: "2242-711524"
        }, {
            lat: -22.8964311,
            lng: -43.267436,
            nome: "Drogarias Boa Sa?de",
            morada1: "Rua Cristov?o Colombo, 02 , Rio de Janeiro, Brazil",
            morada2: "470",
            population: 500 ,codPostal: "2231-052924"
        }, {
            lat: -22.9096026,
            lng: -43.1765751,
            nome: "Drogarias Boa Sa?de",
            morada1: "Rua Treze de Maio, 282-284-286 , Rio de Janeiro, Brazil",
            morada2: "472",
            population: 500 ,codPostal: "2233-1717 / 2243-511024"
        }, {
            lat: -22.8761517,
            lng: -43.4233854,
            nome: "Drogarias Boa Sa?de",
            morada1: "Rua do Imperador, 75 ? Lj 23 , Rio de Janeiro, Brazil",
            morada2: "473",
            population: 500 ,codPostal: "2231-565624"
        }, {
            lat: -22.509091,
            lng: -43.1722509,
            nome: "Drogarias Boa Sa?de",
            morada1: "Rua Paulo Barbosa, 64 , Rio de Janeiro, Brazil",
            morada2: "474",
            population: 500 ,codPostal: "2231-1778 / 2231-619024"
        }, {
            lat: -22.5104134,
            lng: -43.1756257,
            nome: "Drogarias Boa Sa?de",
            morada1: "Rua do Imperador, 595 / 601 , Rio de Janeiro, Brazil",
            morada2: "475",
            population: 500 ,codPostal: "2237-7373 / 2246-800024"
        }, {
            lat: -22.507222,
            lng: -43.1921569,
            nome: "Drogarias Boa Sa?de",
            morada1: "Rua Paulino Afonso, 410 , Rio de Janeiro, Brazil",
            morada2: "476",
            population: 500 ,codPostal: "2243-7166 / 2231-658024"
        }, {
            lat: -22.5081565,
            lng: -43.1872497,
            nome: "Drogarias Boa Sa?de",
            morada1: "Rua Montecaseiros, 111 , Rio de Janeiro, Brazil",
            morada2: "477",
            population: 500 ,codPostal: "2247-021824"
        }, {
            lat: -22.5256167,
            lng: -43.1928438,
            nome: "Drogarias Boa Sa?de",
            morada1: "Rua Coronel Veiga, loja 03 , Rio de Janeiro, Brazil",
            morada2: "478",
            population: 500 ,codPostal: "2243-902324"
        }, {
            lat: -22.4425312,
            lng: -43.1397883,
            nome: "Drogarias Boa Sa?de",
            morada1: "Pra?a Luiz Furtado da Rosa, 74 , Rio de Janeiro, Brazil",
            morada2: "479",
            population: 500 ,codPostal: "2221-171624"
        }, {
            lat: -22.5467054,
            lng: -43.2070683,
            nome: "Drogarias Boa Sa?de",
            morada1: "Rua Pref. Yedo Fiuza, 1.650 ? Lt 1.656 , Rio de Janeiro, Brazil",
            morada2: "480",
            population: 500 ,codPostal: "2231-120824"
        }, {
            lat: -22.4014627,
            lng: -43.1348062,
            nome: "Drogarias Boa Sa?de",
            morada1: "Estr. Uni?o e Ind?stria, 10.091 , Rio de Janeiro, Brazil",
            morada2: "481",
            population: 500 ,codPostal: "2222-6233 / 2222-023724"
        }, {
            lat: -22.4501899,
            lng: -43.1444537,
            nome: "Drogarias Boa Sa?de",
            morada1: "Estr. Uni?o e Ind?stria, 11.870 ? Lj 1 , Rio de Janeiro, Brazil",
            morada2: "482",
            population: 500 ,codPostal: "2222-995524"
        }, {
            lat: -22.4271713,
            lng: -43.0631071,
            nome: "Drogarias Boa Sa?de",
            morada1: "Est. Philuvio Cerqueira Rodrigues, 2.464 ? Loja 4 , Rio de Janeiro, Brazil",
            morada2: "483",
            population: 500 ,codPostal: "2222-837724"
        }, {
            lat: -22.486838,
            lng: -43.1510186,
            nome: "Drogarias Boa Sa?de",
            morada1: "Rua Bernardo de Proen?a, 158 ? LJ 10 , Rio de Janeiro, Brazil",
            morada2: "484",
            population: 500 ,codPostal: "2221-1776 / 2242-240024"
        }, {
            lat: -22.4978813,
            lng: -43.1996721,
            nome: "Drogarias Boa Sa?de",
            morada1: "Rua Mosela N? 1.229, Loja , Rio de Janeiro, Brazil",
            morada2: "485",
            population: 500 ,codPostal: "2231-053324"
        }, {
            lat: -22.3358695,
            lng: -43.1314667,
            nome: "Drogarias Boa Sa?de",
            morada1: "Estrada Uni?o e Ind?stria, 19.247 , Rio de Janeiro, Brazil",
            morada2: "486",
            population: 500 ,codPostal: "2223-2114 / 2223-186424"
        }, {
            lat: -22.4765849,
            lng: -43.1710908,
            nome: "Drogarias Boa Sa?de",
            morada1: "Rua Dr. Hermog?nio Silva, 113 , Rio de Janeiro, Brazil",
            morada2: "487",
            population: 500 ,codPostal: "2245-691324"
        }, {
            lat: -22.9123151,
            lng: -43.1946854,
            nome: "Drogarias Boa Sa?de",
            morada1: "Rua Dr. Mattos N? 45 loja 02 , Rio de Janeiro, Brazil",
            morada2: "489",
            population: 500 ,codPostal: "2734-5757"
        }, {
            lat: -22.8196163,
            lng: -43.3817429,
            nome: "Drogarias Boa Sa?de",
            morada1: "Rua Coronel Moreira Cesar, 115 ? casa 01 , Rio de Janeiro, Brazil",
            morada2: "490",
            population: 500 ,codPostal: "2722-156621"
        }, {
            lat: -22.8045,
            lng: -43.0255196,
            nome: "Drogarias Boa Sa?de",
            morada1: "Rua Vicente de Lima Cleto, 710 , Rio de Janeiro, Brazil",
            morada2: "491",
            population: 500 ,codPostal: "2725-3209 / 2702-237821"
        }, {
            lat: -22.8037396,
            lng: -42.9967937,
            nome: "Drogarias Boa Sa?de",
            morada1: "Rua Itoror?s, 504 , Rio de Janeiro, Brazil",
            morada2: "492",
            population: 500 ,codPostal: "3606-7757 / 3606-013221"
        }, {
            lat: -22.8245655,
            lng: -42.9702599,
            nome: "Drogarias Boa Sa?de",
            morada1: "Estrada do Pacheco, 472 , Rio de Janeiro, Brazil",
            morada2: "493",
            population: 500 ,codPostal: "2603-982121"
        }, {
            lat: -22.855949,
            lng: -43.1005049,
            nome: "Drogarias Boa Sa?de",
            morada1: "Rua Oliveira Botelho, 349 ? Lj 11 / 13 , Rio de Janeiro, Brazil",
            morada2: "494",
            population: 500 ,codPostal: "2628-513721"
        }, {
            lat: -22.8056077,
            lng: -43.0250103,
            nome: "Drogarias Boa Sa?de",
            morada1: "Rua Vicente de Lima Cleto, 442 , Rio de Janeiro, Brazil",
            morada2: "495",
            population: 500 ,codPostal: "3710-910321"
        }, {
            lat: -22.7841484,
            lng: -43.366107,
            nome: "Drogarias Boa Sa?de",
            morada1: "Rua Coreana s/n? ? L.1 Q.21 , Rio de Janeiro, Brazil",
            morada2: "496",
            population: 500 ,codPostal: "2753-916321"
        }, {
            lat: -22.7430161,
            lng: -43.4720126,
            nome: "Drogarias Boa Sa?de",
            morada1: "Av. D?lio Guaran?, 584 Lj A e B , Rio de Janeiro, Brazil",
            morada2: "497",
            population: 500 ,codPostal: "2755-020021"
        }, {
            lat: -22.7784517,
            lng: -43.36732,
            nome: "Drogarias Boa Sa?de",
            morada1: "Av. Presidente Lincoln, 988 ? Loja C , Rio de Janeiro, Brazil",
            morada2: "498",
            population: 500 ,codPostal: "3757-262921"
        }, {
            lat: -22.7779351,
            lng: -43.3372453,
            nome: "Drogarias Boa Sa?de",
            morada1: "Av. Miguel Couto, 941 , Rio de Janeiro, Brazil",
            morada2: "499",
            population: 500 ,codPostal: "3659-5948 / 2751-644021"
        }, {
            lat: -22.8066831,
            lng: -43.4168489,
            nome: "Drogarias Boa Sa?de",
            morada1: "Av. Get?lio de Moura, 1.709 , Rio de Janeiro, Brazil",
            morada2: "500",
            population: 500 ,codPostal: "2756-0587 / 3753-643321"
        }, {
            lat: -22.7950442,
            lng: -43.3820216,
            nome: "Drogarias Boa Sa?de",
            morada1: "Av. Get?lio de Moura, 1.467 , Rio de Janeiro, Brazil",
            morada2: "501",
            population: 500 ,codPostal: "3754-018521"
        }, {
            lat: -22.7797999,
            lng: -43.3494926,
            nome: "Drogarias Boa Sa?de",
            morada1: "Av. Comendador Teles, 2.200 , Rio de Janeiro, Brazil",
            morada2: "502",
            population: 500 ,codPostal: "3757-590821"
        }, {
            lat: -22.8199464,
            lng: -43.6331171,
            nome: "Drogarias Boa Sa?de",
            morada1: "Rua Manoel Gon?alves S/N?, Rio de Janeiro, Brazil",
            morada2: "503",
            population: 500 ,codPostal: "2747-206721"
        }, {
            lat: -22.7332614,
            lng: -42.719226,
            nome: "Drogarias Boa Sa?de",
            morada1: "Rua Manoel Jo?o Gon?alves, 193 ? loja 2 , Rio de Janeiro, Brazil",
            morada2: "504",
            population: 500 ,codPostal: "2747-1717 / 2747-327821"
        }, {
            lat: -22.4009335,
            lng: -42.9781451,
            nome: "Drogarias Boa Sa?de",
            morada1: "Av. Pres. Roselvelt, 1.301 , Rio de Janeiro, Brazil",
            morada2: "505",
            population: 500 ,codPostal: "2743-167521"
        }, {
            lat: -22.8905988,
            lng: -43.482188,
            nome: "Drogaria Destaque",
            morada1: "Rua Marmiari, 980, Rio de Janeiro, Brazil",
            morada2: "506",
            population: 500 ,codPostal: "2401-066921"
        }, {
            lat: -22.8873495,
            lng: -43.4356809,
            nome: "Drogaria Destaque",
            morada1: "Rua Limites, 1370, Rio de Janeiro, Brazil",
            morada2: "507",
            population: 500 ,codPostal: "3271-667521"
        }, {
            lat: -22.86897,
            lng: -43.4197041,
            nome: "Drogaria Destaque",
            morada1: "Rua Princesa Leopoldina, 08, Rio de Janeiro, Brazil",
            morada2: "508",
            population: 500 ,codPostal: "3291-200021"
        }, {
            lat: -22.8813118,
            lng: -43.4634345,
            nome: "Drogaria Destaque",
            morada1: "Rua Silva Cardoso, 629, Rio de Janeiro, Brazil",
            morada2: "509",
            population: 500 ,codPostal: "2401-171921"
        }, {
            lat: -22.8799518,
            lng: -43.446779,
            nome: "Drogaria Destaque",
            morada1: "Estr. do Realengo, 549, Rio de Janeiro, Brazil",
            morada2: "510",
            population: 500 ,codPostal: "3332-600021"
        }, {
            lat: -22.8554046,
            lng: -43.3235243,
            nome: "Drogaria Destaque",
            morada1: "Av. Vicente de Carvalho, 246, Rio de Janeiro, Brazil",
            morada2: "511",
            population: 500 ,codPostal: "3391-200021"
        }, {
            lat: -22.9264759,
            lng: -43.3487007,
            nome: "Drogaria Destaque",
            morada1: "Estr. do Pau-Ferro, 255 - Loja D e E, Rio de Janeiro, Brazil",
            morada2: "512",
            population: 500 ,codPostal: "2424-135521"
        }, {
            lat: -22.8496038,
            lng: -43.3598798,
            nome: "Drogaria Destaque",
            morada1: "Rua Tacaratu, 417, Rio de Janeiro, Brazil",
            morada2: "513",
            population: 500 ,codPostal: "3830-463521"
        }, {
            lat: -22.8666046,
            lng: -43.3745751,
            nome: "Drogaria Destaque",
            morada1: "R. Gen. Savaget, 80 Loja a , Rio de Janeiro, Brazil",
            morada2: "514",
            population: 500 ,codPostal: "2450-500021"
        }, {
            lat: -22.8067566,
            lng: -43.4131259,
            nome: "Drogaria Destaque",
            morada1: "Estr. Gen. Mena Barreto, 205, Rio de Janeiro, Brazil",
            morada2: "515",
            population: 500 ,codPostal: "2691-132821"
        }, {
            lat: -22.881565,
            lng: -43.2474878,
            nome: "Drogaria Destaque",
            morada1: "Rua Jacare?, 12, Rio de Janeiro, Brazil",
            morada2: "516",
            population: 500 ,codPostal: "2599-045421"
        }, {
            lat: -22.7996306,
            lng: -43.359947,
            nome: "Drogaria Destaque",
            morada1: "Rua S?o Janu?rio, 168, Rio de Janeiro, Brazil",
            morada2: "517",
            population: 500 ,codPostal: "3860-500021"
        }, {
            lat: -22.7856942,
            lng: -43.2908002,
            nome: "Drogaria Destaque",
            morada1: "Rua Mal Bento Manuel, 562 , Rio de Janeiro, Brazil",
            morada2: "518",
            population: 500 ,codPostal: "3657-152021"
        }, {
            lat: -22.8860178,
            lng: -43.3018738,
            nome: "Dogarias Viva Bem",
            morada1: "Av. Dom H?lder C?mara, 7.707, Rio de Janeiro, Brazil",
            morada2: "519",
            population: 500 ,codPostal: "3822-2000"
        }, {
            lat: -22.9239674,
            lng: -43.2413244,
            nome: "Dogarias Viva Bem",
            morada1: "Rua Bar?o de Mesquita, 468, Rio de Janeiro, Brazil",
            morada2: "521",
            population: 500 ,codPostal: "2575-8000"
        }, {
            lat: -22.9265448,
            lng: -43.2533616,
            nome: "Dogarias Viva Bem",
            morada1: "Rua Paula Brito, 261, Rio de Janeiro, Brazil",
            morada2: "522",
            population: 500 ,codPostal: "2570-5000"
        }, {
            lat: -22.8703855,
            lng: -43.4798073,
            nome: "Dogarias Viva Bem",
            morada1: "Av. do Catequista , 39 ? Vila Alian?a, Rio de Janeiro, Brazil",
            morada2: "523",
            population: 500 ,codPostal: "3331-4585 / 3424-8972"
        }, {
            lat: -22.9723998,
            lng: -43.3976933,
            nome: "Dogarias Viva Bem",
            morada1: "Av. Emb. Abelardo Bueno, 3500 ? Lj 119, Rio de Janeiro, Brazil",
            morada2: "524",
            population: 500 ,codPostal: "3388-1000 / 2421-1441"
        }, {
            lat: -22.9718815,
            lng: -43.3853034,
            nome: "Dogarias Viva Bem",
            morada1: "R. Bruno Giorgi, 114 Ljs 120 e 121, Rio de Janeiro, Brazil",
            morada2: "525",
            population: 500 ,codPostal: "2421-5000"
        }, {
            lat: -22.8616865,
            lng: -43.3601503,
            nome: "Dogarias Viva Bem",
            morada1: "Rua Pacheco da Rocha, 278 A, Rio de Janeiro, Brazil",
            morada2: "526",
            population: 500 ,codPostal: "2450-3001"
        }, {
            lat: -22.835858,
            lng: -43.3006663,
            nome: "Dogarias Viva Bem",
            morada1: "Rua Camaqua, 173 ? D, Rio de Janeiro, Brazil",
            morada2: "527",
            population: 500 ,codPostal: "3137-3000"
        }, {
            lat: -22.9080514,
            lng: -43.5774227,
            nome: "Dogarias Viva Bem",
            morada1: "Av. Ces?rio de Melo, 4799, Lj A, Rio de Janeiro, Brazil",
            morada2: "528",
            population: 500 ,codPostal: "2415-2001"
        }, {
            lat: -22.8912841,
            lng: -43.5896693,
            nome: "Dogarias Viva Bem",
            morada1: "Estrada Carvalho Ramos, 01, Rio de Janeiro, Brazil",
            morada2: "529",
            population: 500 ,codPostal: "2394-6700 / 2411-2785"
        }, {
            lat: -22.8765452,
            lng: -43.5727364,
            nome: "Dogarias Viva Bem",
            morada1: "R. Severino E. de Castro, 72 ? Loja 02, Rio de Janeiro, Brazil",
            morada2: "530",
            population: 500 ,codPostal: "3394-2379"
        }, {
            lat: -22.8822577,
            lng: -43.3307982,
            nome: "Dogarias Viva Bem",
            morada1: "Av. Ernani Cardoso, n? 63, Rio de Janeiro, Brazil",
            morada2: "531",
            population: 500 ,codPostal: "2597-3082 / 3979-8779"
        }, {
            lat: -22.9147335,
            lng: -43.1883669,
            nome: "Dogarias Viva Bem",
            morada1: "R. Tadeu Kosciusco 91, Loja A, Rio de Janeiro, Brazil",
            morada2: "532",
            population: 500 ,codPostal: "2222-2635 / 2242-1652"
        }, {
            lat: -22.8985178,
            lng: -43.2986684,
            nome: "Dogarias Viva Bem",
            morada1: "Rua Doutor Leal, 306 ? Loja B, Rio de Janeiro, Brazil",
            morada2: "533",
            population: 500 ,codPostal: "2583-5633 / 2583-5232"
        }, {
            lat: -22.9123297,
            lng: -43.2702278,
            nome: "Dogarias Viva Bem",
            morada1: "Rua Gr?o Par?, 235 ? Lj. A e B, Rio de Janeiro, Brazil",
            morada2: "534",
            population: 500 ,codPostal: "2501-7469 / 2228-9000"
        }, {
            lat: -22.9386311,
            lng: -43.3351806,
            nome: "Dogarias Viva Bem",
            morada1: "Rua Geminiano Gois, 59 ? Loja B, Rio de Janeiro, Brazil",
            morada2: "535",
            population: 500 ,codPostal: "3116-5350"
        }, {
            lat: -22.9602305,
            lng: -43.3510951,
            nome: "Dogarias Viva Bem",
            morada1: "Rua Monodora, 227 ? Loja A, Rio de Janeiro, Brazil",
            morada2: "536",
            population: 500 ,codPostal: "3432-3046"
        }, {
            lat: -22.8557443,
            lng: -43.3599929,
            nome: "Dogarias Viva Bem",
            morada1: "Rua Mirinduba,?639, Rio de Janeiro, Brazil",
            morada2: "537",
            population: 500 ,codPostal: "2451-2037"
        }, {
            lat: -22.9572929,
            lng: -43.1991005,
            nome: "Dogarias Viva Bem",
            morada1: "Rua Humait?,?120, Rio de Janeiro, Brazil",
            morada2: "538",
            population: 500 ,codPostal: "2239-7900"
        }, {
            lat: -22.8623463,
            lng: -43.2655761,
            nome: "Dogarias Viva Bem",
            morada1: "Rua Camba?ba, 1.404 ? Loja B, Rio de Janeiro, Brazil",
            morada2: "539",
            population: 500 ,codPostal: "2466-2828"
        }, {
            lat: -22.8233393,
            lng: -43.3241468,
            nome: "Dogarias Viva Bem",
            morada1: "Rua Hannibal Porto, 650 ? A, Rio de Janeiro, Brazil",
            morada2: "540",
            population: 500 ,codPostal: "3373-6791"
        }, {
            lat: -22.8463042,
            lng: -43.324343,
            nome: "Dogarias Viva Bem",
            morada1: "Av. Monsenhor F?lix, 532-A, Rio de Janeiro, Brazil",
            morada2: "541",
            population: 500 ,codPostal: "3301-3000"
        }, {
            lat: -22.8093758,
            lng: -43.3211098,
            nome: "Dogarias Viva Bem",
            morada1: "Rua Franz Liszt, 583 ? Lojas B e C, Rio de Janeiro, Brazil",
            morada2: "542",
            population: 500 ,codPostal: "3346-6000"
        }, {
            lat: -22.8820781,
            lng: -43.410063,
            nome: "Dogarias Viva Bem",
            morada1: "Av. Marechal Fontenelle, 4190 LJ A, Rio de Janeiro, Brazil",
            morada2: "543",
            population: 500 ,codPostal: "3555-9887"
        }, {
            lat: -22.8828624,
            lng: -43.4089865,
            nome: "Dogarias Viva Bem",
            morada1: "Av. Marechal Fontenelle, 4050 ? Loja A, Rio de Janeiro, Brazil",
            morada2: "544",
            population: 500 ,codPostal: "2301-0121"
        }, {
            lat: -22.8623573,
            lng: -43.3721417,
            nome: "Dogarias Viva Bem",
            morada1: "Av. Gen. Osvaldo C. de Faria, 84 ? Lj C, Rio de Janeiro, Brazil",
            morada2: "545",
            population: 500 ,codPostal: "3018-3000"
        }, {
            lat: -22.90424,
            lng: -43.239651,
            nome: "Dogarias Viva Bem",
            morada1: "Trav. Sai?o Lobato, 11 Lj2, Rio de Janeiro, Brazil",
            morada2: "547",
            population: 500 ,codPostal: "2234-4786"
        }, {
            lat: -22.8772835,
            lng: -43.448489,
            nome: "Dogarias Viva Bem",
            morada1: "Rua Estancia, 36 ? Loja C, Rio de Janeiro, Brazil",
            morada2: "548",
            population: 500 ,codPostal: "2401-5324 / 2401-2155"
        }, {
            lat: -22.8132403,
            lng: -43.3742331,
            nome: "Dogarias Viva Bem",
            morada1: "Av. Prof. Bernardino da Rocha, 113-Lj B, Rio de Janeiro, Brazil",
            morada2: "549",
            population: 500 ,codPostal: "2455-7105"
        }, {
            lat: -22.8157734,
            lng: -43.3599176,
            nome: "Dogarias Viva Bem",
            morada1: "Rua Afonso terra, 1034, Rio de Janeiro, Brazil",
            morada2: "550",
            population: 500 ,codPostal: "2407-1000"
        }, {
            lat: -22.836108,
            lng: -43.278798,
            nome: "Dogarias Viva Bem",
            morada1: "Rua Conde de Agrolongo, 1034 ? Loja A, Rio de Janeiro, Brazil",
            morada2: "551",
            population: 500 ,codPostal: "2560-1000 / 3888-5117"
        }, {
            lat: -22.900982,
            lng: -43.354778,
            nome: "Dogarias Viva Bem",
            morada1: "Rua C?ndido Ben?cio, 2316, Rio de Janeiro, Brazil",
            morada2: "552",
            population: 500 ,codPostal: "2435-5000"
        }, {
            lat: -22.9938907,
            lng: -43.6101107,
            nome: "Dogarias Viva Bem",
            morada1: "Estr. da Matriz, 2506 ? loja I , Rio de Janeiro, Brazil",
            morada2: "553",
            population: 500 ,codPostal: "3317-9000"
        }, {
            lat: -22.9751943,
            lng: -43.6478005,
            nome: "Dogarias Viva Bem",
            morada1: "Estrada da Pedra, 4.541, Rio de Janeiro, Brazil",
            morada2: "554",
            population: 500 ,codPostal: "2417-3588 / 3401-4994"
        }, {
            lat: -22.8837437,
            lng: -43.3191101,
            nome: "Dogarias Viva Bem",
            morada1: "Av. Dom Helder C?mara, n? 9540 ? Lj A, Rio de Janeiro, Brazil",
            morada2: "555",
            population: 500 ,codPostal: "2595-3432"
        }, {
            lat: -22.8436473,
            lng: -43.2515233,
            nome: "Dogarias Viva Bem",
            morada1: "Rua Gerson Ferreira, 33, Rio de Janeiro, Brazil",
            morada2: "556",
            population: 500 ,codPostal: "3105-9142 / 3105-9678"
        }, {
            lat: -22.8741413,
            lng: -43.4299468,
            nome: "Dogarias Viva Bem",
            morada1: "R. Mal. Joaquim In?cio, 284, Rio de Janeiro, Brazil",
            morada2: "557",
            population: 500 ,codPostal: "3466-6526"
        }, {
            lat: -22.9021747,
            lng: -43.2560944,
            nome: "Dogarias Viva Bem",
            morada1: "Rua Vinte e Quatro de Maio, 442, Rio de Janeiro, Brazil",
            morada2: "559",
            population: 500 ,codPostal: "2201-0452 / 2261-0050"
        }, {
            lat: -22.9024246,
            lng: -43.2569014,
            nome: "Dogarias Viva Bem",
            morada1: "Rua Vinte e Quatro de Maio, 475 ? Lj. C, Rio de Janeiro, Brazil",
            morada2: "560",
            population: 500 ,codPostal: "2581-5898 / 2581-1795"
        }, {
            lat: -22.8523574,
            lng: -43.3495799,
            nome: "Dogarias Viva Bem",
            morada1: "Av. dos Italianos, 468 ? Loja C, Rio de Janeiro, Brazil",
            morada2: "561",
            population: 500 ,codPostal: "3372-6000 / 2471-3637"
        }, {
            lat: -22.8456065,
            lng: -43.3382404,
            nome: "Dogarias Viva Bem",
            morada1: "Est. do Barro Vermelho, 1470 ? Loja A, Rio de Janeiro, Brazil",
            morada2: "562",
            population: 500 ,codPostal: "2471-1585 / 3371-6390"
        }, {
            lat: -22.9194643,
            lng: -43.3732238,
            nome: "Dogarias Viva Bem",
            morada1: "Rua Atituba, 5 ? Loja A, Rio de Janeiro, Brazil",
            morada2: "563",
            population: 500 ,codPostal: "2424-4000"
        }, {
            lat: -22.9350364,
            lng: -43.2437249,
            nome: "Dogarias Viva Bem",
            morada1: "Rua Cde de Bonfim, 810 ? Loja B, Rio de Janeiro, Brazil",
            morada2: "564",
            population: 500 ,codPostal: "2575-5000"
        }, {
            lat: -22.8445784,
            lng: -43.3010602,
            nome: "Dogarias Viva Bem",
            morada1: "Av. Vicente de Carvalho, 1326 Lj A, Rio de Janeiro, Brazil",
            morada2: "565",
            population: 500 ,codPostal: "2219-6285"
        }, {
            lat: -22.8500172,
            lng: -43.3086951,
            nome: "Dogarias Viva Bem",
            morada1: "Av. Vicente de Carvalho, 995 ? Loja L, Rio de Janeiro, Brazil",
            morada2: "566",
            population: 500 ,codPostal: "3391-7394 / 3013-7769"
        }, {
            lat: -22.878804,
            lng: -43.3641189,
            nome: "Dogarias Viva Bem",
            morada1: "Rua das Verbenas, 05 ? Loja G, Rio de Janeiro, Brazil",
            morada2: "567",
            population: 500 ,codPostal: "3390-3949"
        }, {
            lat: -22.8428375,
            lng: -43.2680738,
            nome: "Dogarias Viva Bem",
            morada1: "Av. Dep. S? Rego, n? 666, Rio de Janeiro, Brazil",
            morada2: "568",
            population: 500 ,codPostal: "2671-4581"
        }, {
            lat: -22.8106504,
            lng: -43.4166896,
            nome: "Dogarias Viva Bem",
            morada1: "Rua Alberto Teixeira da Cunha, 230 , Rio de Janeiro, Brazil",
            morada2: "569",
            population: 500 ,codPostal: "2692-8000"
        }, {
            lat: -22.7987256,
            lng: -43.4114393,
            nome: "Dogarias Viva Bem",
            morada1: "Est. Antonio Jos? Bittencourt, 1075 , Rio de Janeiro, Brazil",
            morada2: "570",
            population: 500 ,codPostal: "2693-5983 / 2692-6437"
        }, {
            lat: -22.7591224,
            lng: -43.4802667,
            nome: "Dogarias Viva Bem",
            morada1: "R. Min. Lafaiete de Andrade, 1690, Rio de Janeiro, Brazil",
            morada2: "572",
            population: 500 ,codPostal: "2667-6019/2667-6142"
        }, {
            lat: -22.853283,
            lng: -43.2610331,
            nome: "Dogarias Viva Bem",
            morada1: "Av. N? S? das Gra?as, 7, Rio de Janeiro, Brazil",
            morada2: "574",
            population: 500 ,codPostal: "2799-1623 / 2686-4244"
        }, {
            lat: -22.7931688,
            lng: -43.3337715,
            nome: "Dogarias Viva Bem",
            morada1: "Av. Etevaldo Ara?jo, 25 ? Loja A, Rio de Janeiro, Brazil",
            morada2: "575",
            population: 500 ,codPostal: "2671-3103"
        }, {
            lat: -22.7868586,
            lng: -43.3801111,
            nome: "Dogarias Viva Bem",
            morada1: "Rua Dr. Agostinho Porto, 189 ? Loja A, Rio de Janeiro, Brazil",
            morada2: "576",
            population: 500 ,codPostal: "2437-9091"
        }, {
            lat: -23.0188054,
            lng: -43.4708345,
            nome: "Dogarias Viva Bem",
            morada1: "Av. Ven?ncio Veloso, 46, Rio de Janeiro, Brazil",
            morada2: "577",
            population: 500 ,codPostal: "2566-279622"
        }, {
            lat: -22.8819197,
            lng: -42.8963271,
            nome: "Dogarias Viva Bem",
            morada1: "Est. de Cassorotiba, Lt 03 Q28 S/N, Rio de Janeiro, Brazil",
            morada2: "578",
            population: 500 ,codPostal: "2636-7000"
        }, {
            lat: -22.9800397,
            lng: -43.2237883,
            nome: "Dogarias Viva Bem",
            morada1: "Av. S?rgio Vieira de Mello, S/n, Rio de Janeiro, Brazil",
            morada2: "579",
            population: 500 ,codPostal: "2771-858722"
        }, {
            lat: -22.4064658,
            lng: -41.8063424,
            nome: "Dogarias Viva Bem",
            morada1: "Rua Passargadas, n? 45 ? Loja A, Rio de Janeiro, Brazil",
            morada2: "582",
            population: 500 ,codPostal: "2773-509822"
        }, {
            lat: -22.8477572,
            lng: -43.4607022,
            nome: "Dogarias Viva Bem",
            morada1: "Rua Joaquim T?vora, 188, Rio de Janeiro, Brazil",
            morada2: "583",
            population: 500 ,codPostal: "2711-1202"
        }, {
            lat: -22.2292818,
            lng: -42.5219555,
            nome: "Dogarias Viva Bem",
            morada1: "Rua Jo?o Alberto Knust, 47 Loja , Rio de Janeiro, Brazil",
            morada2: "584",
            population: 500 ,codPostal: "2527-236122"
        }, {
            lat: -22.7259428,
            lng: -44.1390482,
            nome: "Dogarias Viva Bem",
            morada1: "Rua Presidente Vargas, 108, Rio de Janeiro, Brazil",
            morada2: "585",
            population: 500 ,codPostal: "2522-788222"
        }, {
            lat: -22.9186921,
            lng: -43.1839019,
            nome: "Dogarias Viva Bem",
            morada1: "Estr. Teres?polis ? , s/n?- Km 28, Rio de Janeiro, Brazil",
            morada2: "586",
            population: 500 ,codPostal: "2641-2286"
        }, {
            lat: -22.7976212,
            lng: -42.9569051,
            nome: "Dogarias Viva Bem",
            morada1: "Av. Pres. Roosevelt, 1340 Lj1, Rio de Janeiro, Brazil",
            morada2: "587",
            population: 500 ,codPostal: "2741-1453"
        }, {
            lat: -22.8847609,
            lng: -43.1215172,
            nome: "Dogarias Viva Bem",
            morada1: "Av. Feliciano Sodr?, 420 Lj 02, Rio de Janeiro, Brazil",
            morada2: "588",
            population: 500 ,codPostal: "2742-8023"
        }, {
            lat: -22.9021447,
            lng: -43.256212,
            nome: "Drogarias Mep",
            morada1: "R. Vinte e Quatro de Maio, 448, Rio de Janeiro, Brazil",
            morada2: "589",
            population: 500 ,codPostal: "2261-4000"
        }, {
            lat: -22.9687544,
            lng: -43.4137026,
            nome: "Drogarias Mep",
            morada1: "Estr. dos Bandeirantes, 7926, Rio de Janeiro, Brazil",
            morada2: "590",
            population: 500 ,codPostal: "2441-3360"
        }, {
            lat: -22.9157799,
            lng: -43.3770867,
            nome: "Drogarias Mep",
            morada1: "R. Ariperana, 356, Rio de Janeiro, Brazil",
            morada2: "591",
            population: 500 ,codPostal: "2440-7777"
        }, {
            lat: -22.8055284,
            lng: -43.3662862,
            nome: "Drogarias Mep",
            morada1: "Av. Srg. de Mil?cias, 14212, Rio de Janeiro, Brazil",
            morada2: "592",
            population: 500 ,codPostal: "3847-2229"
        }, {
            lat: -22.8182628,
            lng: -43.4099949,
            nome: "Drogarias Mep",
            morada1: "Av. Get?lio de Moura, 249, Rio de Janeiro, Brazil",
            morada2: "593",
            population: 500 ,codPostal: "2691-3836"
        }, {
            lat: -22.750597,
            lng: -43.4084695,
            nome: "Drogarias Mep",
            morada1: "Estr. Mineira, 64/68, Rio de Janeiro, Brazil",
            morada2: "594",
            population: 500 ,codPostal: "2762-5000"
        }, {
            lat: -22.7714419,
            lng: -43.4319844,
            nome: "Drogarias Mep",
            morada1: "R. Gerv?sio, 279, Rio de Janeiro, Brazil",
            morada2: "595",
            population: 500 ,codPostal: "2796-3811"
        }, {
            lat: -22.7902211,
            lng: -43.3064893,
            nome: "Drogarias Mep",
            morada1: "Av. Brg. Lima e Silva, 2036, Rio de Janeiro, Brazil",
            morada2: "596",
            population: 500 ,codPostal: "3659-9030"
        }, {
            lat: -22.7379547,
            lng: -43.456594,
            nome: "Drogarias Mep",
            morada1: "R. Jo?o Ven?ncio de Figueiredo, 1, Rio de Janeiro, Brazil",
            morada2: "597",
            population: 500 ,codPostal: "3102-1425"
        }, {
            lat: -22.8241995,
            lng: -43.4040723,
            nome: "Drogarias Mep",
            morada1: "Estr. Mal. Alencastro, 4277, Rio de Janeiro, Brazil",
            morada2: "598",
            population: 500 ,codPostal: "3339-2257"
        }, {
            lat: -22.7874251,
            lng: -43.3247906,
            nome: "Drogarias Mep",
            morada1: "Av. Nilo Pe?anha, 1613, Rio de Janeiro, Brazil",
            morada2: "599",
            population: 500 ,codPostal: "2667-4494"
        }, {
            lat: -22.785511,
            lng: -43.542995,
            nome: "Drogarias Mep",
            morada1: "Av. Ab?lio Augusto T?vora, 7091, Rio de Janeiro, Brazil",
            morada2: "600",
            population: 500 ,codPostal: "3764-4233"
        }, {
            lat: -22.7378769,
            lng: -43.4566839,
            nome: "Drogarias Mep",
            morada1: "R. Jo?o Ven?ncio de Figueiredo, 01, Rio de Janeiro, Brazil",
            morada2: "601",
            population: 500 ,codPostal: "3102-1462"
        }, {
            lat: -22.7456342,
            lng: -43.4887256,
            nome: "Drogarias Mep",
            morada1: "R. Leonel Gouv?ia, 75, Rio de Janeiro, Brazil",
            morada2: "602",
            population: 500 ,codPostal: "2695-2832"
        }, {
            lat: -22.7927893,
            lng: -43.3975274,
            nome: "Drogarias Mep",
            morada1: "R. Domingos Alves de Oliveira, 238, Rio de Janeiro, Brazil",
            morada2: "603",
            population: 500 ,codPostal: "2756-8206"
        }, {
            lat: -22.9677366,
            lng: -43.1884745,
            nome: "Drogaria Peixoto",
            morada1: "R. Figueiredo de Magalh?es, 581, loja C, Rio de Janeiro, Brazil",
            morada2: "605",
            population: 500 ,codPostal: "2256-2662"
        }, {
            lat: -22.9666242,
            lng: -43.1892167,
            nome: "Drogaria Peixoto",
            morada1: "R. Figueiredo de Magalh?es, 741, loja H, Rio de Janeiro, Brazil",
            morada2: "606",
            population: 500 ,codPostal: "2255-0000 / 2236-4048 / 3208-6006"
        }, {
            lat: -22.8462591,
            lng: -43.3116719,
            nome: "Droga Life",
            morada1: " Av. Meriti, 1408 / Loja Ce D, Rio de Janeiro, Brazil",
            morada2: "607",
            population: 500 ,codPostal: "3352-7166"
        }, {
            lat: -22.9461318,
            lng: -43.1857549,
            nome: "Droga Life",
            morada1: "R. Bambina, 86, Rio de Janeiro, Brazil",
            morada2: "609",
            population: 500 ,codPostal: "2286-0000"
        }, {
            lat: -22.9668049,
            lng: -43.1824527,
            nome: "Droga Life",
            morada1: "R. Rep?blica do Peru, 250, Rio de Janeiro, Brazil",
            morada2: "610",
            population: 500 ,codPostal: "2549-2000"
        }, {
            lat: -22.8944443,
            lng: -43.2199918,
            nome: "Droga Life",
            morada1: "Avenida Presidente Jo?o Goulart 23 - lj D, Rio de Janeiro, Brazil",
            morada2: "611",
            population: 500 ,codPostal: "3874-5043"
        }, {
            lat: -22.9140892,
            lng: -43.1819623,
            nome: "Droga Life",
            morada1: "R. Riachuelo, 69, Rio de Janeiro, Brazil",
            morada2: "612",
            population: 500 ,codPostal: "2232-6464"
        }, {
            lat: -22.8999287,
            lng: -43.2232905,
            nome: "Droga Rica",
            morada1: "R. S?o Luiz Gonzaga, 52, Rio de Janeiro, Brazil",
            morada2: "614",
            population: 500 ,codPostal: "2589-8000"
        }, {
            lat: -22.9271757,
            lng: -43.2360914,
            nome: "DrogaBel",
            morada1: "R conde de bonfim 490 lj a, Rio de Janeiro, Brazil",
            morada2: "615",
            population: 500 ,codPostal: "3796-3052"
        }, {
            lat: -22.9221592,
            lng: -43.2097475,
            nome: "DrogaBel",
            morada1: "Av. Paulo de Frontin, 38, Rio de Janeiro, Brazil",
            morada2: "616",
            population: 500 ,codPostal: "3337-8853"
        }, {
            lat: -23.0075301,
            lng: -43.3109104,
            nome: "DrogaBel",
            morada1: "Av. Fernando Mattos, 300, Rio de Janeiro, Brazil",
            morada2: "617",
            population: 500 ,codPostal: "3178-2606"
        }, {
            lat: -22.8723456,
            lng: -43.4514807,
            nome: "Drogarias Andorra",
            morada1: "R. Andorra, 58, Rio de Janeiro, Brazil",
            morada2: "618",
            population: 500 ,codPostal: "3159-0411"
        }, {
            lat: -22.8716967,
            lng: -43.4544345,
            nome: "Drogarias Andorra",
            morada1: "R. Manuel Resende, 309, Rio de Janeiro, Brazil",
            morada2: "619",
            population: 500 ,codPostal: "3463-9000"
        }, {
            lat: -22.9071383,
            lng: -43.289184,
            nome: "Drogaria Atra??o",
            morada1: "R Pedro de Carvalho, 228, Rio de Janeiro, Brazil",
            morada2: "620",
            population: 500 ,codPostal: "2594-0812"
        }, {
            lat: -22.9045486,
            lng: -43.1097806,
            nome: "Drogarias Cristal",
            morada1: "Rua Gavi?o Peixoto, 182, Rio de Janeiro, Brazil",
            morada2: "621",
            population: 500 ,codPostal: "2710-7000"
        }, {
            lat: -23.0011633,
            lng: -43.3879015,
            nome: "Drogarias Cristal",
            morada1: "Av das Am?ricas, 6700, Loja 114, BL 01( Info Barra?), Rio de Janeiro, Brazil",
            morada2: "622",
            population: 500 ,codPostal: "3325-1002"
        }, {
            lat: -22.9490202,
            lng: -43.1829954,
            nome: "Drogarias Cristal",
            morada1: "R. S?o Clemente, 12, Rio de Janeiro, Brazil",
            morada2: "623",
            population: 500 ,codPostal: "2286-5000"
        }, {
            lat: -22.956077,
            lng: -43.1976155,
            nome: "Drogarias Cristal",
            morada1: "R. Volunt?rios da P?tria, 466, Rio de Janeiro, Brazil",
            morada2: "624",
            population: 500 ,codPostal: "2537-5888"
        }, {
            lat: -22.9600672,
            lng: -43.2019923,
            nome: "Drogarias Cristal",
            morada1: "Rua Humait?, 261 - Loja A, Rio de Janeiro, Brazil",
            morada2: "625",
            population: 500 ,codPostal: "2286-3000"
        }, {
            lat: -22.9538773,
            lng: -43.181305,
            nome: "Drogarias Cristal",
            morada1: "Rua da Passagem, 119, Rio de Janeiro, Brazil",
            morada2: "626",
            population: 500 ,codPostal: "2542-9000"
        }, {
            lat: -22.9466781,
            lng: -43.1836242,
            nome: "Drogarias Cristal",
            morada1: "Praia de Botafogo, 360 - Loja A/B, Rio de Janeiro, Brazil",
            morada2: "627",
            population: 500 ,codPostal: "2551-4000"
        }, {
            lat: -22.9495957,
            lng: -43.1868056,
            nome: "Drogarias Cristal",
            morada1: "R. S?o Clemente, 145, Rio de Janeiro, Brazil",
            morada2: "628",
            population: 500 ,codPostal: "2266-4000"
        }, {
            lat: -22.9427724,
            lng: -43.3672122,
            nome: "Drogaria Center Farma",
            morada1: "Est Marechal Miguel Salazar Mendes de Moraes, 906 , Rio de Janeiro, Brazil",
            morada2: "629",
            population: 500 ,codPostal: "3412-6000"
        }, {
            lat: -22.920101,
            lng: -43.6822643,
            nome: "Drogaria Exata",
            morada1: "Rua Princesa Isabel,49, Rio de Janeiro, Brazil",
            morada2: "630",
            population: 500 ,codPostal: "2666-441922"
        }, {
            lat: -22.8928988,
            lng: -42.0425866,
            nome: "Drogaria Exata",
            morada1: "Av. Lecy Gomes da Costa, 304, Rio de Janeiro, Brazil",
            morada2: "631",
            population: 500 ,codPostal: "2645-207322"
        }, {
            lat: -22.9607617,
            lng: -43.2238174,
            nome: "Drogaria Exata",
            morada1: "Rua Visconde de Itauna, 227, Rio de Janeiro, Brazil",
            morada2: "632",
            population: 500 ,codPostal: "2678-6986"
        }, {
            lat: -22.9723307,
            lng: -43.1912089,
            nome: "Drogaria Exata",
            morada1: "Rua Cinco de Julho, 367, Rio de Janeiro, Brazil",
            morada2: "633",
            population: 500 ,codPostal: "2610-6000"
        }, {
            lat: -22.9181934,
            lng: -43.2230338,
            nome: "Drogaria Exata",
            morada1: "Rua Mariz e Barros 335 Loja E Loja A, Rio de Janeiro, Brazil",
            morada2: "634",
            population: 500 ,codPostal: "3602-5000"
        }, {
            lat: -22.9131811,
            lng: -43.1881662,
            nome: "Drogaria Exata",
            morada1: "Rua Carlos Sampaio, 319 - Loja A, Rio de Janeiro, Brazil",
            morada2: "635",
            population: 500 ,codPostal: "2509-0301"
        }, {
            lat: -22.9192471,
            lng: -43.6398859,
            nome: "Drogaria Exata",
            morada1: "Rua Romeu Cocco, 17, Rio de Janeiro, Brazil",
            morada2: "636",
            population: 500 ,codPostal: "3394- 7568 / 7896-7121"
        }, {
            lat: -22.8446616,
            lng: -43.3747874,
            nome: "Drogaria Exata",
            morada1: "Rua Luiz Coutinho Cavalcanti n? 76 Lj A, Rio de Janeiro, Brazil",
            morada2: "637",
            population: 500 ,codPostal: "2450-2590/3015-5222"
        }, {
            lat: -22.8235058,
            lng: -43.3257759,
            nome: "Drogaria Exata",
            morada1: "Rua Andre Filho 13 Loja A, Rio de Janeiro, Brazil",
            morada2: "638",
            population: 500 ,codPostal: "2475-5000"
        }, {
            lat: -22.9101598,
            lng: -43.283027,
            nome: "Drogaria Exata",
            morada1: "Rua Lins de Vasconcelos , 523 Loja, Rio de Janeiro, Brazil",
            morada2: "639",
            population: 500 ,codPostal: "3979-3188"
        }, {
            lat: -22.8934659,
            lng: -43.2788327,
            nome: "Drogaria Exata",
            morada1: "Rua Salvador Pires, 240 - Loja C, Rio de Janeiro, Brazil",
            morada2: "640",
            population: 500 ,codPostal: "2289-0919 / 2597-0220"
        }, {
            lat: -22.8934261,
            lng: -43.3218643,
            nome: "Drogaria Exata",
            morada1: "Rua Clarimundo de Melo 796, Rio de Janeiro, Brazil",
            morada2: "641",
            population: 500 ,codPostal: "3274-8302 / 3274-8416"
        }, {
            lat: -22.8795686,
            lng: -43.3139408,
            nome: "Drogaria Exata",
            morada1: "Rua Padre Manuel de Nobrega, 730 Loja C, Rio de Janeiro, Brazil",
            morada2: "642",
            population: 500 ,codPostal: "3272-7006"
        }, {
            lat: -22.8393747,
            lng: -43.3990953,
            nome: "Drogaria Exata",
            morada1: "Pra?a Claudio de Souza 82 loja A, Rio de Janeiro, Brazil",
            morada2: "644",
            population: 500 ,codPostal: "3019-0947-2455-1018/2455-3226/3358-7000"
        }, {
            lat: -22.9405795,
            lng: -43.25365,
            nome: "Drogaria Exata",
            morada1: "Rua S?o Miguel, 467 - Loja B, Rio de Janeiro, Brazil",
            morada2: "645",
            population: 500 ,codPostal: "2501-6001"
        }, {
            lat: -22.8344328,
            lng: -43.3654158,
            nome: "Drogaria Exata",
            morada1: "Estrada Jo?o Paulo 1845, Rio de Janeiro, Brazil",
            morada2: "646",
            population: 500 ,codPostal: "3837-9000"
        }, {
            lat: -22.8358028,
            lng: -43.3637878,
            nome: "Drogaria Exata",
            morada1: "Estrada Jo?o Paulo 1602 , Rio de Janeiro, Brazil",
            morada2: "647",
            population: 500 ,codPostal: "3451-6000"
        }, {
            lat: -22.9117859,
            lng: -43.1887236,
            nome: "Drogaria Exata",
            morada1: "Pra?a da Cruz Vermelha, n? 28, Rio de Janeiro, Brazil",
            morada2: "648",
            population: 500 ,codPostal: "2232-0848"
        }, {
            lat: -22.8880857,
            lng: -43.2596422,
            nome: "Drogaria Exata",
            morada1: "Rua Darcy Vargas, 73, Rio de Janeiro, Brazil",
            morada2: "649",
            population: 500 ,codPostal: "2581-8428"
        }, {
            lat: -22.8867984,
            lng: -43.2541689,
            nome: "Drogaria Exata",
            morada1: "Av. Guanabara, 181, Rio de Janeiro, Brazil",
            morada2: "650",
            population: 500 ,codPostal: "2201-7000"
        }, {
            lat: -22.8855271,
            lng: -43.2639508,
            nome: "Drogaria Exata",
            morada1: "Rua Feliciano de Aguiar, 501, Rio de Janeiro, Brazil",
            morada2: "651",
            population: 500 ,codPostal: "2581-8428 / 2228-8360 / 2201-1971"
        }, {
            lat: -22.8765175,
            lng: -43.3513967,
            nome: "Drogaria Exata",
            morada1: "Rua Henrique Braga, 498, Rio de Janeiro, Brazil",
            morada2: "652",
            population: 500 ,codPostal: "2583-4263"
        }, {
            lat: -22.8775854,
            lng: -43.3092008,
            nome: "Drogaria Exata",
            morada1: "Rua Cardoso Quint?o, 46 Loja B, Rio de Janeiro, Brazil",
            morada2: "653",
            population: 500 ,codPostal: "2289-0946"
        }, {
            lat: -22.8924566,
            lng: -43.4184893,
            nome: "Drogaria Exata",
            morada1: "Av Pirpirituba 44 lj C, Rio de Janeiro, Brazil",
            morada2: "654",
            population: 500 ,codPostal: "3555-9000 / 3555-5455"
        }, {
            lat: -22.890908,
            lng: -43.4387916,
            nome: "Drogaria Exata",
            morada1: "Rua do Governo 485 e 485 A, Rio de Janeiro, Brazil",
            morada2: "655",
            population: 500 ,codPostal: "2401-3000 / 3422-2988"
        }, {
            lat: -22.8855632,
            lng: -43.4171125,
            nome: "Drogaria Exata",
            morada1: "Rua Piraquara, 983 Loja A, Rio de Janeiro, Brazil",
            morada2: "656",
            population: 500 ,codPostal: "3555-4000"
        }, {
            lat: -22.9179794,
            lng: -43.224612,
            nome: "Drogaria Exata",
            morada1: "Rua S?o Francisco Xavier 146 lj A, Rio de Janeiro, Brazil",
            morada2: "657",
            population: 500 ,codPostal: "3872-1818/3234-4442"
        }, {
            lat: -22.9556121,
            lng: -43.1836545,
            nome: "Drogaria Exata",
            morada1: "Rua Arnaldo Santos 75 A, Rio de Janeiro, Brazil",
            morada2: "658",
            population: 500 ,codPostal: "2627-597422"
        }, {
            lat: -22.6516579,
            lng: -42.3908597,
            nome: "Drogaria Exata",
            morada1: "Av. 8 de Maio, 533 - Loja 02, Rio de Janeiro, Brazil",
            morada2: "659",
            population: 500 ,codPostal: "2668-196122"
        }, {
            lat: -22.7081514,
            lng: -43.3329067,
            nome: "Drogaria Exata",
            morada1: "ALM Argentina, s/n - LT 05 - QD 01 - LJ C, Rio de Janeiro, Brazil",
            morada2: "660",
            population: 500 ,codPostal: "3134-1641 / 3735-3220"
        }, {
            lat: -22.9725556,
            lng: -43.3680419,
            nome: "Drogaria Exata",
            morada1: "Av Boulevard, 245 Loja, Rio de Janeiro, Brazil",
            morada2: "661",
            population: 500 ,codPostal: "3662-2040"
        }, {
            lat: -22.9117859,
            lng: -43.1887236,
            nome: "Drogaria Exata",
            morada1: "Pra?a da Cruz Vermelha, n? 28, Rio de Janeiro, Brazil",
            morada2: "662",
            population: 500 ,codPostal: "2232-0848"
        }, {
            lat: -22.8539625,
            lng: -42.583315,
            nome: "Drogaria Exata",
            morada1: "Rod. Amaral Peixoto, 153 Km, Rio de Janeiro, Brazil",
            morada2: "663",
            population: 500 ,codPostal: "2764-182922"
        }, {
            lat: -22.7456448,
            lng: -43.7016808,
            nome: "Drogaria Exata",
            morada1: "Av. Ministro Fernando Costa, 775 - Loja, Rio de Janeiro, Brazil",
            morada2: "664",
            population: 500 ,codPostal: "3787-1009 / 2682-1010"
        }, {
            lat: -22.9248681,
            lng: -43.2110232,
            nome: "Drogaria Du Point",
            morada1: "Rua Do Bispo,?50, Rio Comprido, Rio de Janeiro, Brazil",
            morada2: "665",
            population: 500 ,codPostal: "2293-3748"
        }, {
            lat: -22.8810796,
            lng: -43.4806713,
            nome: "Drogaria Expressa",
            morada1: "Avenida De Santa Cruz, 67, Rio de Janeiro, Brazil",
            morada2: "666",
            population: 500 ,codPostal: "2467-4935"
        }, {
            lat: -22.8077658,
            lng: -43.1833225,
            nome: "Drogaria Expressa",
            morada1: "Rua Ten Cleto Campelo,?427, Rio de Janeiro, Brazil",
            morada2: "667",
            population: 500 ,codPostal: "2124-532000"
        }, {
            lat: -22.8827888,
            lng: -43.3683992,
            nome: "Drogaria Expressa",
            morada1: "Pra?a Saiqui,?66, Rio de Janeiro, Brazil",
            morada2: "668",
            population: 500 ,codPostal: "3324-5575"
        }, {
            lat: -22.9043262,
            lng: -43.2867054,
            nome: "Drogaria Extra",
            morada1: "R. Dias da Cruz, 371, Rio de Janeiro, Brazil",
            morada2: "669",
            population: 500 ,codPostal: "2593-8646"
        }, {
            lat: -22.9001103,
            lng: -43.223912,
            nome: "Drogaria Extra",
            morada1: "R. S?o Luiz Gonzaga, 122, Rio de Janeiro, Brazil",
            morada2: "670",
            population: 500 ,codPostal: "3890-3674"
        }, {
            lat: -22.8427987,
            lng: -43.2680703,
            nome: "Drogaria Extra",
            morada1: "Rua Leopoldina R?go, 666, Rio de Janeiro, Brazil",
            morada2: "671",
            population: 500 ,codPostal: "4003-3383"
        }, {
            lat: -22.8496051,
            lng: -43.3113842,
            nome: "Drogaria Extra",
            morada1: "Av. Vicente de Carvalho, 909, Rio de Janeiro, Brazil",
            morada2: "672",
            population: 500 ,codPostal: "3906-9626"
        }, {
            lat: -22.804756,
            lng: -43.2081446,
            nome: "Drogaria Extra",
            morada1: "Estrada do Gale?o, 2700, Rio de Janeiro, Brazil",
            morada2: "673",
            population: 500 ,codPostal: "2462-1385"
        }, {
            lat: -22.7978586,
            lng: -43.3512836,
            nome: "Drogaria Extra",
            morada1: "R. Maria Soares Sendas, 111 (Grande Rio Shop), Rio de Janeiro, Brazil",
            morada2: "674",
            population: 500 ,codPostal: "3752-3551"
        }, {
            lat: -22.790978,
            lng: -43.309542,
            nome: "Drogaria Extra",
            morada1: "Av. Governador Leonel de Moura Brizola, 1190, Rio de Janeiro, Brazil",
            morada2: "675",
            population: 500 ,codPostal: "2671-1969"
        }, {
            lat: -22.8785862,
            lng: -43.4652446,
            nome: "Drogaria Extra",
            morada1: "R. Francisco Real, 2050, Rio de Janeiro, Brazil",
            morada2: "676",
            population: 500 ,codPostal: "2401-9472"
        }, {
            lat: -22.8829846,
            lng: -43.2903171,
            nome: "Drogaria Extra",
            morada1: "Av. Dom H?lder C?mara, 6350, Rio de Janeiro, Brazil",
            morada2: "677",
            population: 500 ,codPostal: "3906-3606"
        }, {
            lat: -22.9293821,
            lng: -43.2420027,
            nome: "Drogaria Extra",
            morada1: "Rua Uruguai, 329, Rio de Janeiro, Brazil",
            morada2: "678",
            population: 500 ,codPostal: "2572-9075"
        }, {
            lat: -22.9057784,
            lng: -43.1763414,
            nome: "Drogaria Pacheco",
            morada1: "Rua S?o Jos?, 78, Rio de Janeiro, Brazil",
            morada2: "679",
            population: 500 ,codPostal: "2220-2974 / 2220-2976"
        }, {
            lat: -22.9045781,
            lng: -43.1767933,
            nome: "Drogaria Pacheco",
            morada1: "Rua Sete De Setembro, 61, Rio de Janeiro, Brazil",
            morada2: "680",
            population: 500 ,codPostal: "3970-1801 / 3970-1054"
        }, {
            lat: -22.9070298,
            lng: -43.1774865,
            nome: "Drogaria Pacheco",
            morada1: "Avenida Rio Branco, 156 , Rio de Janeiro, Brazil",
            morada2: "681",
            population: 500 ,codPostal: "2262-3213 /2262-3852"
        }, {
            lat: -22.9046685,
            lng: -43.1769218,
            nome: "Drogaria Pacheco",
            morada1: "Rua Sete De Setembro, 67 I, Rio de Janeiro, Brazil",
            morada2: "682",
            population: 500 ,codPostal: "2531-9262 /2531-8197"
        }, {
            lat: -22.9088405,
            lng: -43.1780734,
            nome: "Drogaria Pacheco",
            morada1: "Rua Senador Dantas, 118, Rio de Janeiro, Brazil",
            morada2: "683",
            population: 500 ,codPostal: "2220-2300 / 2220-2027"
        }, {
            lat: -22.9041636,
            lng: -43.1767371,
            nome: "Drogaria Pacheco",
            morada1: "Rua Sete De Setembro, 126 , Rio de Janeiro, Brazil",
            morada2: "684",
            population: 500 ,codPostal: "2224-4644 / 3970-1352"
        }, {
            lat: -22.9053669,
            lng: -43.1794914,
            nome: "Drogaria Pacheco",
            morada1: "Rua Uruguaiana, 27, Rio de Janeiro, Brazil",
            morada2: "685",
            population: 500 ,codPostal: "2224-9514 / 2224-9018"
        }, {
            lat: -22.9120622,
            lng: -43.1766346,
            nome: "Drogaria Pacheco",
            morada1: "Rua Senador Dantas, 5, Rio de Janeiro, Brazil",
            morada2: "686",
            population: 500 ,codPostal: "2240-8751 /2220-5680"
        }, {
            lat: -22.9120732,
            lng: -43.1763728,
            nome: "Drogaria Pacheco",
            morada1: "Rua Senador Dantas, 3, Rio de Janeiro, Brazil",
            morada2: "687",
            population: 500 ,codPostal: "2262-3210 / 2292-4512"
        }, {
            lat: -22.9035044,
            lng: -43.1817147,
            nome: "Drogaria Pacheco",
            morada1: "Rua Dos Andradas, 43, Rio de Janeiro, Brazil",
            morada2: "688",
            population: 500 ,codPostal: "2224-3781 /2292-5719"
        }, {
            lat: -22.8990791,
            lng: -43.1816119,
            nome: "Drogaria Pacheco",
            morada1: "Rua Do Acre, 68, Rio de Janeiro, Brazil",
            morada2: "689",
            population: 500 ,codPostal: "2253-2500 /2253-1950"
        }, {
            lat: -22.9202322,
            lng: -43.1770243,
            nome: "Drogaria Pacheco",
            morada1: "Rua Da Gl?ria,374, Rio de Janeiro, Brazil",
            morada2: "690",
            population: 500 ,codPostal: "3970-1031 /3970-1817"
        }, {
            lat: -22.9150968,
            lng: -43.1877467,
            nome: "Drogaria Pacheco",
            morada1: "Rua Do Riachuelo, 217, Rio de Janeiro, Brazil",
            morada2: "691",
            population: 500 ,codPostal: "2509-1571 /3970-1593"
        }, {
            lat: -22.9293366,
            lng: -43.1776516,
            nome: "Drogaria Pacheco",
            morada1: "Rua Do Catete, 250, Rio de Janeiro, Brazil",
            morada2: "692",
            population: 500 ,codPostal: "2265-8270 /2205-1341"
        }, {
            lat: -22.9303969,
            lng: -43.1776524,
            nome: "Drogaria Pacheco",
            morada1: "Largo Do Machado, 29, Rio de Janeiro, Brazil",
            morada2: "693",
            population: 500 ,codPostal: "2205-8572 /2265-6245"
        }, {
            lat: -22.9311582,
            lng: -43.1780662,
            nome: "Drogaria Pacheco",
            morada1: "Rua Do Catete, 300, Rio de Janeiro, Brazil",
            morada2: "694",
            population: 500 ,codPostal: "2245- 8899 /2265-6197"
        }, {
            lat: -22.9380253,
            lng: -43.1779813,
            nome: "Drogaria Pacheco",
            morada1: "Rua Marqu?s De Abrantes, 144, Rio de Janeiro, Brazil",
            morada2: "695",
            population: 500 ,codPostal: "3237-1044 /2285-7587"
        }, {
            lat: -22.9241013,
            lng: -43.209086,
            nome: "Drogaria Pacheco",
            morada1: "Rua Aristides Lobo, 244, Rio de Janeiro, Brazil",
            morada2: "696",
            population: 500 ,codPostal: "2293-8387 /2213-4073"
        }, {
            lat: -22.9146259,
            lng: -43.2129628,
            nome: "Drogaria Pacheco",
            morada1: "Rua Do Matoso, 100 , Rio de Janeiro, Brazil",
            morada2: "697",
            population: 500 ,codPostal: "2213-1824 /2273-8484"
        }, {
            lat: -22.9449171,
            lng: -43.1825521,
            nome: "Drogaria Pacheco",
            morada1: "Rua Praia De Botafogo, 316 B, Rio de Janeiro, Brazil",
            morada2: "698",
            population: 500 ,codPostal: "2237-7896 /2551-8585"
        }, {
            lat: -22.9478929,
            lng: -43.1828999,
            nome: "Drogaria Pacheco",
            morada1: "Praia De Botafogo, 406, Rio de Janeiro, Brazil",
            morada2: "699",
            population: 500 ,codPostal: "3237-1112 /3237-1115"
        }, {
            lat: -22.9494962,
            lng: -43.1850107,
            nome: "Drogaria Pacheco",
            morada1: "Rua S?o Clemente, 91 , Rio de Janeiro, Brazil",
            morada2: "700",
            population: 500 ,codPostal: "2226-6320 /2286-3566"
        }, {
            lat: -22.9221094,
            lng: -43.2210271,
            nome: "Drogaria Pacheco",
            morada1: "Rua Haddock Lobo, 463 , Rio de Janeiro, Brazil",
            morada2: "701",
            population: 500 ,codPostal: "3872-4434 /2254-6769"
        }, {
            lat: -22.9034055,
            lng: -43.1220477,
            nome: "Drogaria Pacheco",
            morada1: "Rua Dr. Paulo Alves, 72, Rio de Janeiro, Brazil",
            morada2: "702",
            population: 500 ,codPostal: "2717-9561 /2717-9852"
        }, {
            lat: -22.9520338,
            lng: -43.1869594,
            nome: "Drogaria Pacheco",
            morada1: "Rua Volunt?rios Da Patria,150, Rio de Janeiro, Brazil",
            morada2: "703",
            population: 500 ,codPostal: "2286-4142 /2286-4128"
        }, {
            lat: -22.9041106,
            lng: -43.2868608,
            nome: "Drogaria Pacheco",
            morada1: "R. Dias Da Cruz, 380, Rio de Janeiro, Brazil",
            morada2: "704",
            population: 500 ,codPostal: "3822-5319"
        }, {
            lat: -22.9040079,
            lng: -43.2847914,
            nome: "Drogaria Pacheco",
            morada1: "R. Dias da Cruz, 303, Rio de Janeiro, Brazil",
            morada2: "705",
            population: 500 ,codPostal: "2592-7461"
        }, {
            lat: -22.9024204,
            lng: -43.2817922,
            nome: "Drogaria Pacheco",
            morada1: "R. Dias da Cruz, 154, Rio de Janeiro, Brazil",
            morada2: "706",
            population: 500 ,codPostal: "2289-9117"
        }, {
            lat: -22.9014832,
            lng: -43.2793651,
            nome: "Drogaria Pacheco",
            morada1: "R. Silva Rab?lo, 14, Rio de Janeiro, Brazil",
            morada2: "707",
            population: 500 ,codPostal: "4020-4404"
        }, {
            lat: -22.9022291,
            lng: -43.2763525,
            nome: "Drogaria Pacheco",
            morada1: "R. Carolina M?ier, 12, Rio de Janeiro, Brazil",
            morada2: "708",
            population: 500 ,codPostal: "3278-3540"
        }, {
            lat: -22.8873005,
            lng: -43.2856322,
            nome: "Drogaria Pacheco",
            morada1: "Av. Dom H?lder C?mara, 5614, Rio de Janeiro, Brazil",
            morada2: "709",
            population: 500 ,codPostal: "3822-9513"
        }, {
            lat: -22.8852767,
            lng: -43.2986056,
            nome: "Drogaria Pacheco",
            morada1: "Av. Dom H?lder C?mara, 7351, Rio de Janeiro, Brazil",
            morada2: "710",
            population: 500 ,codPostal: "2595-3393"
        }, {
            lat: -22.8810932,
            lng: -43.2938719,
            nome: "Drogaria Pacheco",
            morada1: "Av. Jo?o Ribeiro, 1, Rio de Janeiro, Brazil",
            morada2: "711",
            population: 500 ,codPostal: "3272-8640"
        }, {
            lat: -22.8819222,
            lng: -43.32569,
            nome: "Drogaria Pacheco",
            morada1: "Av. Dom H?lder C?mara, 10244, Rio de Janeiro, Brazil",
            morada2: "712",
            population: 500 ,codPostal: "2591-7712"
        }, {
            lat: -22.9279547,
            lng: -43.3399141,
            nome: "Drogaria Pacheco",
            morada1: "Estr. do Pau-Ferro, 668, Rio de Janeiro, Brazil",
            morada2: "713",
            population: 500 ,codPostal: "2425-4031"
        }, {
            lat: -22.941388,
            lng: -43.3419227,
            nome: "Drogaria Pacheco",
            morada1: "Estr. de Jacarepagu?, 7741, Rio de Janeiro, Brazil",
            morada2: "714",
            population: 500 ,codPostal: "3327-6640"
        }, {
            lat: -22.9399054,
            lng: -43.3429064,
            nome: "Drogaria Pacheco",
            morada1: "Estr. dos Tr?s Rios, 74, Rio de Janeiro, Brazil",
            morada2: "715",
            population: 500 ,codPostal: "2456-1850"
        }, {
            lat: -22.8683387,
            lng: -43.3345014,
            nome: "Drogaria Pacheco",
            morada1: "Avenida Ministro Edgar Rom?rio, 203, Rio de Janeiro, Brazil",
            morada2: "716",
            population: 500 ,codPostal: "2458-8661"
        }, {
            lat: -22.8731549,
            lng: -43.3390985,
            nome: "Drogaria Pacheco",
            morada1: "Estr. do Portela, 99, Rio de Janeiro, Brazil",
            morada2: "717",
            population: 500 ,codPostal: "2489-5169"
        }, {
            lat: -22.9248592,
            lng: -43.2340538,
            nome: "Drogaria Pacheco",
            morada1: "Rua Conde de Bonfim, 396, Rio de Janeiro, Brazil",
            morada2: "718",
            population: 500 ,codPostal: "2254-5778"
        }, {
            lat: -22.8640357,
            lng: -43.2543765,
            nome: "Drogaria Pacheco",
            morada1: "Rua Cardoso de Morais, 101, Rio de Janeiro, Brazil",
            morada2: "719",
            population: 500 ,codPostal: "3976-6955"
        }, {
            lat: -22.923673,
            lng: -43.2322868,
            nome: "Drogaria Pacheco",
            morada1: "Rua Conde de Bonfim, 334, Rio de Janeiro, Brazil",
            morada2: "720",
            population: 500 ,codPostal: "2569-4538"
        }, {
            lat: -22.8548491,
            lng: -43.260736,
            nome: "Drogaria Pacheco",
            morada1: "R. Uranos, 1029, Rio de Janeiro, Brazil",
            morada2: "721",
            population: 500 ,codPostal: "2260-8697"
        }, {
            lat: -22.9000083,
            lng: -43.2239981,
            nome: "Drogaria Pacheco",
            morada1: "R. S?o Luiz Gonzaga, 126, Rio de Janeiro, Brazil",
            morada2: "722",
            population: 500 ,codPostal: "2589-2253"
        }, {
            lat: -22.9220528,
            lng: -43.3722575,
            nome: "Drogaria Pacheco",
            morada1: "Av. Nelson Cardoso, 1228, Rio de Janeiro, Brazil",
            morada2: "723",
            population: 500 ,codPostal: "2435-2948"
        }, {
            lat: -22.8747823,
            lng: -43.3364449,
            nome: "Drogaria Pacheco",
            morada1: "R. Carvalho de Souza, 306, Rio de Janeiro, Brazil",
            morada2: "724",
            population: 500 ,codPostal: "2489-5728"
        }, {
            lat: -22.8966146,
            lng: -43.3519058,
            nome: "Drogaria Pacheco",
            morada1: "R. C?ndido Ben?cio, 1748, Rio de Janeiro, Brazil",
            morada2: "725",
            population: 500 ,codPostal: "3355-4250"
        }, {
            lat: -22.924977,
            lng: -43.3702422,
            nome: "Drogaria Pacheco",
            morada1: "Estr. do Tindiba, Rio de Janeiro, Brazil",
            morada2: "726",
            population: 500 ,codPostal: "2423-2706"
        }, {
            lat: -22.9236188,
            lng: -43.3735685,
            nome: "Drogaria Pacheco",
            morada1: "Estr. dos Bandeirantes, 27 / Loja A - , Rio de Janeiro, Brazil",
            morada2: "727",
            population: 500 ,codPostal: "2427-3143"
        }, {
            lat: -22.9227609,
            lng: -43.372688,
            nome: "Drogaria Pacheco",
            morada1: "Av. Nelson Cardoso, 29, Rio de Janeiro, Brazil",
            morada2: "728",
            population: 500 ,codPostal: "4020-4404"
        }, {
            lat: -22.8754998,
            lng: -43.3384529,
            nome: "Drogaria Pacheco",
            morada1: "Avenida Ministro Edgar Rom?rio, 9, Rio de Janeiro, Brazil",
            morada2: "729",
            population: 500 ,codPostal: "2489-7441"
        }, {
            lat: -22.838578,
            lng: -43.3115832,
            nome: "Drogaria Pacheco",
            morada1: "Av. Meriti, 2445 / Loja C , Rio de Janeiro, Brazil",
            morada2: "730",
            population: 500 ,codPostal: "3391-5569"
        }, {
            lat: -22.8467552,
            lng: -43.3245424,
            nome: "Drogaria Pacheco",
            morada1: "Av. Monsenhor F?lix, 506, Rio de Janeiro, Brazil",
            morada2: "731",
            population: 500 ,codPostal: "2253-2031"
        }, {
            lat: -22.9155363,
            lng: -43.246011,
            nome: "Drogaria Pacheco",
            morada1: "Boulevard 28 de Setembro, 296, Rio de Janeiro, Brazil",
            morada2: "732",
            population: 500 ,codPostal: "3879-4555"
        }, {
            lat: -22.8405377,
            lng: -43.2803689,
            nome: "Drogaria Pacheco",
            morada1: "Av. Br?s de Pina, 150, Rio de Janeiro, Brazil",
            morada2: "733",
            population: 500 ,codPostal: "3104-4030"
        }, {
            lat: -22.8397666,
            lng: -43.3039214,
            nome: "Drogaria Pacheco",
            morada1: "R. Tom?s Lopes, 881 / Loja G -, Rio de Janeiro, Brazil",
            morada2: "734",
            population: 500 ,codPostal: "3352-2136"
        }, {
            lat: -22.9235833,
            lng: -43.2566902,
            nome: "Drogaria Pacheco",
            morada1: "R. Bar?o de Mesquita, 1063, Rio de Janeiro, Brazil",
            morada2: "735",
            population: 500 ,codPostal: "3238-4601"
        }, {
            lat: -22.8277847,
            lng: -43.3193732,
            nome: "Drogaria Pacheco",
            morada1: "Estr. da ?gua Grande, 781, Rio de Janeiro, Brazil",
            morada2: "736",
            population: 500 ,codPostal: "2407-1425"
        }, {
            lat: -22.9146259,
            lng: -43.2129628,
            nome: "Drogaria Pacheco",
            morada1: "R. do Matoso, 100, Rio de Janeiro, Brazil",
            morada2: "737",
            population: 500 ,codPostal: "2213-1824"
        }, {
            lat: -22.9167728,
            lng: -43.249763,
            nome: "Drogaria Pacheco",
            morada1: "Boulevard 28 de Setembro, 435, Rio de Janeiro, Brazil",
            morada2: "738",
            population: 500 ,codPostal: "3879-4681"
        }, {
            lat: -22.9290527,
            lng: -43.3540077,
            nome: "Drogaria Pacheco",
            morada1: "Av. Gerem?rio Dantas, Rio de Janeiro, Brazil",
            morada2: "739",
            population: 500 ,codPostal: "4020-4404"
        }, {
            lat: -22.932238,
            lng: -43.2400413,
            nome: "Drogaria Pacheco",
            morada1: "Rua Conde de Bonfim, 681, Rio de Janeiro, Brazil",
            morada2: "740",
            population: 500 ,codPostal: "3238-1440"
        }, {
            lat: -22.9057784,
            lng: -43.1763414,
            nome: "Drogaria Pacheco",
            morada1: "R. S?o Jos?, 78, Rio de Janeiro, Brazil",
            morada2: "741",
            population: 500 ,codPostal: "2220-2976"
        }, {
            lat: -22.976304,
            lng: -43.2285941,
            nome: "Drogaria Pacheco",
            morada1: "R. Marqu?s de S?o Vicente, 67, Rio de Janeiro, Brazil",
            morada2: "743",
            population: 500 ,codPostal: "3112-9999"
        }, {
            lat: -22.80921,
            lng: -43.3216568,
            nome: "Drogaria Pacheco",
            morada1: "R. Franz Liszt, 546 Loja A, Rio de Janeiro, Brazil",
            morada2: "746",
            population: 500 ,codPostal: "3346-5547"
        }, {
            lat: -22.9449171,
            lng: -43.1825521,
            nome: "Drogaria Pacheco",
            morada1: "Praia de Botafogo, 316, Rio de Janeiro, Brazil",
            morada2: "747",
            population: 500 ,codPostal: "2251-8585"
        }, {
            lat: -22.9240933,
            lng: -43.2405265,
            nome: "Drogaria Pacheco",
            morada1: "R. Bar?o de Mesquita, 8, Rio de Janeiro, Brazil",
            morada2: "748",
            population: 500 ,codPostal: "3238-4601"
        }, {
            lat: -22.8400103,
            lng: -43.2780214,
            nome: "Drogaria Pacheco",
            morada1: "R. Nicar?gua, 250, Rio de Janeiro, Brazil",
            morada2: "749",
            population: 500 ,codPostal: "2561-9105"
        }, {
            lat: -22.8052356,
            lng: -43.3665358,
            nome: "Drogaria Pacheco",
            morada1: "R. Srg. Fernandes Fontes, 30, Rio de Janeiro, Brazil",
            morada2: "750",
            population: 500 ,codPostal: "3847-3201"
        }, {
            lat: -22.9527945,
            lng: -43.1919034,
            nome: "Drogaria Pacheco",
            morada1: "R. da Matriz, 75, Rio de Janeiro, Brazil",
            morada2: "751",
            population: 500 ,codPostal: "2696-5857"
        }, {
            lat: -22.8849313,
            lng: -43.3661376,
            nome: "Drogaria Pacheco",
            morada1: "Rua Luiz Beltr?o, 117, Rio de Janeiro, Brazil",
            morada2: "752",
            population: 500 ,codPostal: "4020-4404"
        }, {
            lat: -22.9027092,
            lng: -43.1148951,
            nome: "Drogaria Pacheco",
            morada1: "R. Miguel de Frias, 78, Rio de Janeiro, Brazil",
            morada2: "753",
            population: 500 ,codPostal: "2618-3332"
        }, {
            lat: -23.0033644,
            lng: -43.3262618,
            nome: "Drogaria Pacheco",
            morada1: "R. Gild?sio Amado, 55, Rio de Janeiro, Brazil",
            morada2: "754",
            population: 500 ,codPostal: "2484-5323"
        }, {
            lat: -22.8269779,
            lng: -43.316622,
            nome: "Drogaria Pacheco",
            morada1: "Estr. da ?gua Grande, 1158, Rio de Janeiro, Brazil",
            morada2: "755",
            population: 500 ,codPostal: "2475-5047"
        }, {
            lat: -22.9150968,
            lng: -43.1877467,
            nome: "Drogaria Pacheco",
            morada1: "Riachuelo, 217 / Loja A - R., Rio de Janeiro, Brazil",
            morada2: "756",
            population: 500 ,codPostal: "3112-9999"
        }, {
            lat: -22.8990791,
            lng: -43.1816119,
            nome: "Drogaria Pacheco",
            morada1: "R. Acre, 68, Rio de Janeiro, Brazil",
            morada2: "757",
            population: 500 ,codPostal: "2253-1950"
        }, {
            lat: -23.0102819,
            lng: -43.3044632,
            nome: "Drogaria Pacheco",
            morada1: "Av. Oleg?rio Maciel, 520, Rio de Janeiro, Brazil",
            morada2: "758",
            population: 500 ,codPostal: "3153-8863"
        }, {
            lat: -22.976304,
            lng: -43.2285941,
            nome: "Drogaria Pacheco",
            morada1: "R. Marqu?s de S?o Vicente, 67, Rio de Janeiro, Brazil",
            morada2: "760",
            population: 500 ,codPostal: "3112-9999"
        }, {
            lat: -22.9399428,
            lng: -43.343895,
            nome: "Drogaria Futura",
            morada1: "Avenida Gerem?rio Dantas,?1458, LJ B, Rio de Janeiro, Brazil",
            morada2: "763",
            population: 500 ,codPostal: "2425-0090"
        }, {
            lat: -22.8309655,
            lng: -43.3190315,
            nome: "Drogaria Futura",
            morada1: "Avenida Br?s de Pina,?2133, Rio de Janeiro, Brazil",
            morada2: "764",
            population: 500 ,codPostal: "3391-7990"
        }, {
            lat: -22.8991088,
            lng: -43.2366764,
            nome: "Drogaria Futura",
            morada1: "Rua Ana Neri,?49, LJ A, Rio de Janeiro, Brazil",
            morada2: "765",
            population: 500 ,codPostal: "2580-0105"
        }, {
            lat: -22.974578,
            lng: -43.1881388,
            nome: "Drogaria Futura",
            morada1: "Rua Constante Ramos,?23, LJ A, Rio de Janeiro, Brazil",
            morada2: "766",
            population: 500 ,codPostal: "2549-6000"
        }, {
            lat: -22.8859582,
            lng: -43.4755581,
            nome: "Drogaria Futura",
            morada1: "Rua Rio Da Prata, 1342, Rio de Janeiro, Brazil",
            morada2: "767",
            population: 500 ,codPostal: "2581-8969"
        }, {
            lat: -22.9000721,
            lng: -43.202212,
            nome: "Drogaria Futura",
            morada1: "Rua Santo Cristo,?217, Rio de Janeiro, Brazil",
            morada2: "768",
            population: 500 ,codPostal: "2233-0906"
        }, {
            lat: -22.9342759,
            lng: -43.180789,
            nome: "Drogaria Futura",
            morada1: "Pra?a S?o Salvador,?1, Rio de Janeiro, Brazil",
            morada2: "769",
            population: 500 ,codPostal: "2498-2000"
        }, {
            lat: -22.9572929,
            lng: -43.1991005,
            nome: "Drogaria Futura",
            morada1: "Rua Humait?,?120, Rio de Janeiro, Brazil",
            morada2: "770",
            population: 500 ,codPostal: "2539-7900"
        }, {
            lat: -22.9631355,
            lng: -43.1754968,
            nome: "Drogaria Leme",
            morada1: "Av. Prado J?nior 237, Rio de Janeiro, Brazil",
            morada2: "771",
            population: 500 ,codPostal: "3223-9000"
        }, {
            lat: -23.00862,
            lng: -43.365349,
            nome: "Drogaria Leme",
            morada1: "Av. Ayrton Senna 250, Rio de Janeiro, Brazil",
            morada2: "772",
            population: 500 ,codPostal: "2433-2313"
        }, {
            lat: -22.9632252,
            lng: -43.1715381,
            nome: "Drogaria Leme",
            morada1: "R. Gustavo Sampaio, 650, Rio de Janeiro, Brazil",
            morada2: "773",
            population: 500 ,codPostal: "2275-2965"
        }, {
            lat: -22.9638169,
            lng: -43.1766426,
            nome: "Drogaria Leme",
            morada1: "R. Min. Viveiros de Castro, 62, Rio de Janeiro, Brazil",
            morada2: "774",
            population: 500 ,codPostal: "3223-9000"
        }, {
            lat: -23.00133,
            lng: -43.3497673,
            nome: "Drogaria Leme",
            morada1: "Av. das Am?ricas, 3555?? Barra Square Shopping Centre, Rio de Janeiro, Brazil",
            morada2: "775",
            population: 500 ,codPostal: "2430-7016"
        }, {
            lat: -22.8404605,
            lng: -43.296996,
            nome: "Drogaria Itaoc?ra",
            morada1: "Av. Vicente de Carvalho, 1582 - , Rio de Janeiro - RJ, , Rio de Janeiro, Brazil",
            morada2: "776",
            population: 500 ,codPostal: "2485-3311"
        }, {
            lat: -22.8461523,
            lng: -43.2832573,
            nome: "Drogaria Lider",
            morada1: "Av. Nossa Sra. da Penha, 528, Rio de Janeiro, Brazil",
            morada2: "777",
            population: 500 ,codPostal: "3887-4602"
        }, {
            lat: -22.9042592,
            lng: -43.2698957,
            nome: "Drogaria Nina",
            morada1: "R. Bar?o do Bom Retiro, 96 , Rio de Janeiro, Brazil",
            morada2: "778",
            population: 500 ,codPostal: "2501-2894"
        }, {
            lat: -22.8919728,
            lng: -43.2783381,
            nome: "Drogaria Max",
            morada1: "R. Cirne Maia, 42 - loja A, Rio de Janeiro, Brazil",
            morada2: "779",
            population: 500 ,codPostal: "2597-4046"
        }, {
            lat: -22.8852541,
            lng: -43.2983164,
            nome: "Drogaria Max",
            morada1: "Av. Dom H?lder C?mara, 7331, Rio de Janeiro, Brazil",
            morada2: "780",
            population: 500 ,codPostal: "3271-4848"
        }, {
            lat: -22.931131,
            lng: -43.238851,
            nome: "Drogaria Max",
            morada1: "Rua Conde de Bonfim, 624 A, Rio de Janeiro, Brazil",
            morada2: "781",
            population: 500 ,codPostal: "2575-6000"
        }, {
            lat: -22.9125906,
            lng: -43.216492,
            nome: "Drogaria Max",
            morada1: "R. Mariz e Barros, 166 - Loja A, Rio de Janeiro, Brazil",
            morada2: "782",
            population: 500 ,codPostal: "2273-3294"
        }, {
            lat: -22.938369,
            lng: -43.3479619,
            nome: "Drogaria Max",
            morada1: "Av. Gerem?rio Dantas, 1241, Rio de Janeiro, Brazil",
            morada2: "783",
            population: 500 ,codPostal: "2424-5252"
        }, {
            lat: -22.9989034,
            lng: -43.3608404,
            nome: "Drogaria Max",
            morada1: "Av. das Am?ricas, 8445(New York City Center), Rio de Janeiro, Brazil",
            morada2: "784",
            population: 500 ,codPostal: "3325-3060"
        }, {
            lat: -22.8863862,
            lng: -43.3026559,
            nome: "Drogaria Max",
            morada1: "Av. Dom H?lder C?mara, 7797, Rio de Janeiro, Brazil",
            morada2: "785",
            population: 500 ,codPostal: "2289-5973"
        }, {
            lat: -22.9655735,
            lng: -43.2188912,
            nome: "Drogaria Max",
            morada1: "R. Jardim Bot?nico, 720, Rio de Janeiro, Brazil",
            morada2: "786",
            population: 500 ,codPostal: "2294-3000"
        }, {
            lat: -22.6777426,
            lng: -43.2387182,
            nome: "Drogaria Max",
            morada1: "Pra?a Jo?o Pessoa,1, Rio de Janeiro, Brazil",
            morada2: "787",
            population: 500 ,codPostal: "2232-2000"
        }, {
            lat: -22.980405,
            lng: -43.1913458,
            nome: "Drogaria Max",
            morada1: "Av. Nossa Sra. de Copacabana, 1150/ A, Rio de Janeiro, Brazil",
            morada2: "788",
            population: 500 ,codPostal: "2247-9000"
        }, {
            lat: -22.9121404,
            lng: -43.1861612,
            nome: "Drogaria Max",
            morada1: "Av. Mem de S?, 178, Rio de Janeiro, Brazil",
            morada2: "789",
            population: 500 ,codPostal: "2224-5703"
        }, {
            lat: -22.8550754,
            lng: -43.2593889,
            nome: "Drogaria Max",
            morada1: "Rua Cardoso de Morais, 566, Rio de Janeiro, Brazil",
            morada2: "790",
            population: 500 ,codPostal: "2270-6569"
        }, {
            lat: -22.9159233,
            lng: -43.3780035,
            nome: "Drogaria Max",
            morada1: "Pra?a Jauru, 32/ C e D, Rio de Janeiro, Brazil",
            morada2: "791",
            population: 500 ,codPostal: "2436-5000"
        }, {
            lat: -22.8108143,
            lng: -43.1887602,
            nome: "Drogaria Max",
            morada1: "Estr. da Cacuia, 347/A, Rio de Janeiro, Brazil",
            morada2: "792",
            population: 500 ,codPostal: "2467-3000"
        }, {
            lat: -22.9167405,
            lng: -43.3617554,
            nome: "Drogaria Max",
            morada1: "Av. Gerem?rio Dantas, 12, Rio de Janeiro, Brazil",
            morada2: "793",
            population: 500 ,codPostal: "3392-4242"
        }, {
            lat: -22.8744626,
            lng: -43.3578144,
            nome: "Drogaria Max",
            morada1: "Estr. da Fontinha, 41, Rio de Janeiro, Brazil",
            morada2: "794",
            population: 500 ,codPostal: "3350-6629"
        }, {
            lat: -22.9570271,
            lng: -43.1984356,
            nome: "Drogaria Max",
            morada1: "Rua Humait?, 129 - Loja C, Rio de Janeiro, Brazil",
            morada2: "795",
            population: 500 ,codPostal: "2579-3000"
        }, {
            lat: -22.9685004,
            lng: -43.1888371,
            nome: "Drogaria Max",
            morada1: "Rua Tonelero, 326, Rio de Janeiro, Brazil",
            morada2: "796",
            population: 500 ,codPostal: "2255-2653"
        }, {
            lat: -22.9116405,
            lng: -43.1743773,
            nome: "Drogaria Max",
            morada1: "R. M?xico, 21 - Sala 201/202/302, Rio de Janeiro, Brazil",
            morada2: "797",
            population: 500 ,codPostal: "2220-0785"
        }, {
            lat: -22.9604858,
            lng: -43.2081358,
            nome: "Drogaria Max",
            morada1: "R. Maria Ang?lica, 301, Rio de Janeiro, Brazil",
            morada2: "798",
            population: 500 ,codPostal: "2266-6609"
        }, {
            lat: -22.9926447,
            lng: -43.2540827,
            nome: "Drogaria Max",
            morada1: "Estr. da G?vea, 817, Rio de Janeiro, Brazil",
            morada2: "799",
            population: 500 ,codPostal: "2422-8080"
        }, {
            lat: -22.889575,
            lng: -43.2634179,
            nome: "Drogaria Max",
            morada1: "R. da Gl?ria, 268 / Loja A, Rio de Janeiro, Brazil",
            morada2: "800",
            population: 500 ,codPostal: "2224-7094"
        }, {
            lat: -22.7914476,
            lng: -43.3121064,
            nome: "Drogaria Max",
            morada1: "R. Jos? de Alvarenga, 95, Rio de Janeiro, Brazil",
            morada2: "801",
            population: 500 ,codPostal: "2671-0779"
        }, {
            lat: -22.8246637,
            lng: -43.1695509,
            nome: "Drogaria Max",
            morada1: "R. Maldonado, 293 / C, Rio de Janeiro, Brazil",
            morada2: "802",
            population: 500 ,codPostal: "3396-0055"
        }, {
            lat: -22.8113722,
            lng: -43.1947552,
            nome: "Drogaria Max",
            morada1: "Estrada do Gale?o, 961, Rio de Janeiro, Brazil",
            morada2: "803",
            population: 500 ,codPostal: "2462-2000"
        }, {
            lat: -22.884704,
            lng: -43.3670657,
            nome: "Drogaria Max",
            morada1: "Rua Luiz Beltr?o, 46, Rio de Janeiro, Brazil",
            morada2: "804",
            population: 500 ,codPostal: "2451-6000"
        }, {
            lat: -22.9493643,
            lng: -43.1888092,
            nome: "Drogaria Max",
            morada1: "R. S?o Clemente, 188, Rio de Janeiro, Brazil",
            morada2: "806",
            population: 500 ,codPostal: "2538-1079"
        }, {
            lat: -22.933488,
            lng: -43.242165,
            nome: "Drogaria Max",
            morada1: "Rua Conde de Bonfim, 740, Rio de Janeiro, Brazil",
            morada2: "807",
            population: 500 ,codPostal: "2238-5611"
        }, {
            lat: -22.9043195,
            lng: -43.2855092,
            nome: "Drogaria Max",
            morada1: "R. Dias da Cruz, 335, Rio de Janeiro, Brazil",
            morada2: "808",
            population: 500 ,codPostal: "3979-2000"
        }, {
            lat: -22.8395129,
            lng: -43.2972897,
            nome: "Drogaria Max",
            morada1: "Av. Br?s de Pina, 950, Rio de Janeiro, Brazil",
            morada2: "809",
            population: 500 ,codPostal: "3341-3241"
        }, {
            lat: -22.8454813,
            lng: -43.3241108,
            nome: "Drogaria Max",
            morada1: "Av. Monsenhor F?lix, 555, Rio de Janeiro, Brazil",
            morada2: "810",
            population: 500 ,codPostal: "3301-7171"
        }, {
            lat: -22.9427724,
            lng: -43.3672122,
            nome: "Drogaria Max",
            morada1: "Estrada Marechal Miguel Salazar Mendes de Moraes, 906, Rio de Janeiro, Brazil",
            morada2: "811",
            population: 500 ,codPostal: "2426-1616"
        }, {
            lat: -22.9824365,
            lng: -43.2256034,
            nome: "Drogaria Max",
            morada1: "Rua Dias Ferreira, 521, Rio de Janeiro, Brazil",
            morada2: "812",
            population: 500 ,codPostal: "2294-1398"
        }, {
            lat: -22.832191,
            lng: -43.2795508,
            nome: "Drogaria Max",
            morada1: "Av. Lobo J?nior, 1354, Rio de Janeiro, Brazil",
            morada2: "813",
            population: 500 ,codPostal: "2564-2500"
        }, {
            lat: -22.7942074,
            lng: -43.1726161,
            nome: "Drogaria Max",
            morada1: "Av. Paranapu?, 162, Rio de Janeiro, Brazil",
            morada2: "814",
            population: 500 ,codPostal: "2466-9912"
        }, {
            lat: -22.8461831,
            lng: -43.3116201,
            nome: "Drogaria Max",
            morada1: "Av. Meriti, 1408, Rio de Janeiro, Brazil",
            morada2: "815",
            population: 500 ,codPostal: "2481-2214"
        }, {
            lat: -22.9410773,
            lng: -43.1721738,
            nome: "Drogaria Max",
            morada1: "Av. Rui Barbosa, 437, Rio de Janeiro, Brazil",
            morada2: "817",
            population: 500 ,codPostal: "2611-3538"
        }, {
            lat: -22.9315927,
            lng: -43.1799615,
            nome: "Drogaria Max",
            morada1: "R. das Laranjeiras, 15 - Loja B, Rio de Janeiro, Brazil",
            morada2: "818",
            population: 500 ,codPostal: "2556-0000"
        }, {
            lat: -22.8951545,
            lng: -43.2528497,
            nome: "Drogaria Max",
            morada1: "Rua Lino Teixeira, 38, Rio de Janeiro, Brazil",
            morada2: "819",
            population: 500 ,codPostal: "2582-9311"
        }, {
            lat: -22.962285,
            lng: -43.1682455,
            nome: "Drogaria Max",
            morada1: "R. Aurelino Leal, 52 - Loja B, Rio de Janeiro, Brazil",
            morada2: "820",
            population: 500 ,codPostal: "2629-7146"
        }, {
            lat: -22.9117859,
            lng: -43.1887236,
            nome: "Drogaria Max",
            morada1: "Pra?a Cruz Vermelha, 28, Rio de Janeiro, Brazil",
            morada2: "821",
            population: 500 ,codPostal: "2232-0848"
        }, {
            lat: -22.8971271,
            lng: -43.1817399,
            nome: "Drogaria Max",
            morada1: "R. Sacadura Cabral, 17, Rio de Janeiro, Brazil",
            morada2: "822",
            population: 500 ,codPostal: "2576-8846"
        }, {
            lat: -22.9358615,
            lng: -43.1752205,
            nome: "Drogaria Max",
            morada1: "Rua Senador Vergueiro, 93 A/B, Rio de Janeiro, Brazil",
            morada2: "823",
            population: 500 ,codPostal: "2558-6000"
        }, {
            lat: -22.9663007,
            lng: -43.1810538,
            nome: "Drogaria Max",
            morada1: "Rua Inhang?, 15, Rio de Janeiro, Brazil",
            morada2: "824",
            population: 500 ,codPostal: "2255-3372"
        }, {
            lat: -22.8839724,
            lng: -43.3171667,
            nome: "Drogaria Max",
            morada1: "Av. Dom H?lder C?mara, 9324, Rio de Janeiro, Brazil",
            morada2: "825",
            population: 500 ,codPostal: "3271-6000"
        }, {
            lat: -22.9679012,
            lng: -43.3919438,
            nome: "Drogaria Max",
            morada1: "Av. Jaime Poggi, 80 - Loja F, Rio de Janeiro, Brazil",
            morada2: "826",
            population: 500 ,codPostal: "3585-7021"
        }, {
            lat: -23.0025639,
            lng: -43.3502703,
            nome: "Drogaria Max",
            morada1: "Av. Marechal Henrique Lott, 120 / Loja 114, Rio de Janeiro, Brazil",
            morada2: "827",
            population: 500 ,codPostal: "3325-4000"
        }, {
            lat: -22.7942074,
            lng: -43.1726161,
            nome: "Drogaria Max",
            morada1: "Av. Paranapu?, 162, Rio de Janeiro, Brazil",
            morada2: "828",
            population: 500 ,codPostal: "2466-9912"
        }, {
            lat: -22.7732994,
            lng: -42.9194115,
            nome: "Drogaria Max",
            morada1: "R. Francisco Rafael de Barros, Loja 01, Rio de Janeiro, Brazil",
            morada2: "829",
            population: 500 ,codPostal: "2635-0001"
        }, {
            lat: -22.8815332,
            lng: -43.0857352,
            nome: "Drogaria Max",
            morada1: "Alameda S?o Boaventura, 974, Rio de Janeiro, Brazil",
            morada2: "830",
            population: 500 ,codPostal: "2627-7000"
        }, {
            lat: -22.9074572,
            lng: -43.5619682,
            nome: "Drogaria Max",
            morada1: "Av. Ces?rio de Melo, 3137, Rio de Janeiro, Brazil",
            morada2: "831",
            population: 500 ,codPostal: "2416-4040"
        }, {
            lat: -22.9015669,
            lng: -43.1000508,
            nome: "Drogaria Max",
            morada1: "R. Santa Rosa, 184 C, Rio de Janeiro, Brazil",
            morada2: "832",
            population: 500 ,codPostal: "2711-3333"
        }, {
            lat: -22.497299,
            lng: -44.105202,
            nome: "Pague Menos",
            morada1: "AV. S?VIO COTA DE ALMEIDA GAMA, 1633, Rio de Janeiro, Brazil",
            morada2: "833",
            population: 500 ,codPostal: "33389079 I 910924"
        }, {
            lat: -22.9039517,
            lng: -43.2846715,
            nome: "Pague Menos",
            morada1: "R. Dias da Cruz, 297, Rio de Janeiro, Brazil",
            morada2: "834",
            population: 500 ,codPostal: "Aberto at? ?s 22:00"
        }, {
            lat: -22.9051195,
            lng: -43.2892776,
            nome: "Pague Menos",
            morada1: "R. Dias da Cruz, 491, Rio de Janeiro, Brazil",
            morada2: "835",
            population: 500 ,codPostal: "Aberto at? ?s 22:00"
        }, {
            lat: -22.8743156,
            lng: -43.3370509,
            nome: "Pague Menos",
            morada1: "Av. Min. Edgard Romero, 94, Rio de Janeiro, Brazil",
            morada2: "836",
            population: 500 ,codPostal: "Aberto at? ?s 21:00"
        }, {
            lat: -22.9021124,
            lng: -43.2790349,
            nome: "Pague Menos",
            morada1: "R. Dias da Cruz, 47, Rio de Janeiro, Brazil",
            morada2: "837",
            population: 500 ,codPostal: "Aberto at? ?s 21:00"
        }, {
            lat: -22.9314212,
            lng: -43.239417,
            nome: "Pague Menos",
            morada1: "Rua Conde de Bonfim, 648, Rio de Janeiro, Brazil",
            morada2: "838",
            population: 500 ,codPostal: "Aberto at? ?s 20:00"
        }, {
            lat: -22.9225362,
            lng: -43.3733557,
            nome: "Drogaria Pegale",
            morada1: "Estr. do Tindiba, 2263, Rio de Janeiro, Brazil",
            morada2: "839",
            population: 500 ,codPostal: "2435-0000"
        }, {
            lat: -22.9128373,
            lng: -42.8212267,
            nome: "Drogaria Pegale",
            morada1: "Rua Abreu Sodr?, 32, Rio de Janeiro, Brazil",
            morada2: "840",
            population: 500 ,codPostal: "3731-2000"
        }, {
            lat: -22.9630893,
            lng: -43.170343,
            nome: "Drogaria Pegale",
            morada1: "R. Gustavo Sampaio, 535, Rio de Janeiro, Brazil",
            morada2: "841",
            population: 500 ,codPostal: "2541-8000"
        }, {
            lat: -22.8741266,
            lng: -43.2659303,
            nome: "Drogaria Real de Higianopolis",
            morada1: "R Darke de Matos, 285 - lj-A CEP:?21051-470, Rio de Janeiro, Brazil",
            morada2: "842",
            population: 500 ,codPostal: "2501-0468 / 2573-2688 / 2573-1045"
        }, {
            lat: -22.9064819,
            lng: -43.1745363,
            nome: "Drogaria Ric",
            morada1: "R. Debret, 7 A, Rio de Janeiro, Brazil",
            morada2: "843",
            population: 500 ,codPostal: "2215-2228"
        }, {
            lat: -22.9411299,
            lng: -43.1818673,
            nome: "Drogaria Ric",
            morada1: "Rua Farani, 42, Rio de Janeiro, Brazil",
            morada2: "844",
            population: 500 ,codPostal: "2553-0945"
        }, {
            lat: -22.9651327,
            lng: -43.2225884,
            nome: "Drogaria Ric",
            morada1: "R. Von Martius, 320 / Loja G, Rio de Janeiro, Brazil",
            morada2: "845",
            population: 500 ,codPostal: "2540-0140"
        }, {
            lat: -22.9067328,
            lng: -43.1739378,
            nome: "Drogaria Rio Farma",
            morada1: "Av.nilo Pe?anha, n?11 loja C, Rio de Janeiro, Brazil",
            morada2: "846",
            population: 500 ,codPostal: "2532-7000 / 2262-9595"
        }, {
            lat: -22.9123756,
            lng: -43.1746733,
            nome: "Drogaria Rio Farma",
            morada1: "Av.Rio Branco, n? 156 loja 17, Rio de Janeiro, Brazil",
            morada2: "847",
            population: 500 ,codPostal: "2544-3000 / 2524-6000"
        }, {
            lat: -22.9089897,
            lng: -43.1775607,
            nome: "Drogaria Rio Farma",
            morada1: "Av.Treze de Maio, n? 33 lj B, Rio de Janeiro, Brazil",
            morada2: "848",
            population: 500 ,codPostal: "2544-0000 / 2544-9999"
        }, {
            lat: -22.9257192,
            lng: -43.176766,
            nome: "Drogaria Rio Farma",
            morada1: "Rua do Catete, n? 144, Rio de Janeiro, Brazil",
            morada2: "849",
            population: 500 ,codPostal: "2245-3000 / 2245-7000"
        }, {
            lat: -22.928632,
            lng: -43.1773285,
            nome: "Drogaria Rio Farma",
            morada1: "Rua do catete, n? 234 loja B, Rio de Janeiro, Brazil",
            morada2: "850",
            population: 500 ,codPostal: "2245-6000 / 2556-8055"
        }, {
            lat: -22.9292722,
            lng: -43.1772016,
            nome: "Drogaria Rio Farma",
            morada1: "Rua do Catete, 257 Loja C, Rio de Janeiro, Brazil",
            morada2: "851",
            population: 500 ,codPostal: "2286-9000 / 2285-9000"
        }, {
            lat: -22.9307793,
            lng: -43.1771724,
            nome: "Drogaria Rio Farma",
            morada1: "Rua do Catete, n? 311 Loja A e B, Rio de Janeiro, Brazil",
            morada2: "852",
            population: 500 ,codPostal: "2245-8000 / 2245-9000"
        }, {
            lat: -22.9333986,
            lng: -43.177021,
            nome: "Drogaria Rio Farma",
            morada1: "Rua Senador Vergueiro, n? 23 lj D, Rio de Janeiro, Brazil",
            morada2: "853",
            population: 500 ,codPostal: "2265-4444 / 3235-4000"
        }, {
            lat: -22.9369602,
            lng: -43.1775996,
            nome: "Drogaria Rio Farma",
            morada1: "Rua Marques de Abrantes, n? 110 loja C, Rio de Janeiro, Brazil",
            morada2: "854",
            population: 500 ,codPostal: "2553-7000"
        }, {
            lat: -22.9404096,
            lng: -43.1787459,
            nome: "Drogaria Rio Farma",
            morada1: "Rua Marques de Abrantes, n?214, Rio de Janeiro, Brazil",
            morada2: "855",
            population: 500 ,codPostal: "2552-2000 / 2551-3000"
        }, {
            lat: -22.9382099,
            lng: -43.1921948,
            nome: "Drogaria Rio Farma",
            morada1: "Rua das Laranjeiras, 462 Lj E, Rio de Janeiro, Brazil",
            morada2: "856",
            population: 500 ,codPostal: "2246-1000 / 2285-1111"
        }, {
            lat: -22.9539237,
            lng: -43.1916182,
            nome: "Drogaria Rio Farma",
            morada1: "Rua Volunt?rios da P?tria, 278 Loja B, Rio de Janeiro, Brazil",
            morada2: "857",
            population: 500 ,codPostal: "2450 - 1000 / 2450-1060"
        }, {
            lat: -22.9680997,
            lng: -43.1833452,
            nome: "Drogaria Rio Farma",
            morada1: "Av.N.Sr? de Copacabana, 500, Rio de Janeiro, Brazil",
            morada2: "858",
            population: 500 ,codPostal: "2545-7353 / 3208-8000"
        }, {
            lat: -22.9684312,
            lng: -43.1848844,
            nome: "Drogaria Rio Farma",
            morada1: "Rua Siqueira Campos, n?36, Rio de Janeiro, Brazil",
            morada2: "859",
            population: 500 ,codPostal: "2256-5000 / 2548-6665"
        }, {
            lat: -22.9210503,
            lng: -43.2186725,
            nome: "Droga Raia",
            morada1: "RUA HADDOCK LOBO, 382 - A, Rio de Janeiro, Brazil",
            morada2: "860",
            population: 500 ,codPostal: "2204-0590"
        }, {
            lat: -22.9840147,
            lng: -43.2191942,
            nome: "Droga Raia",
            morada1: "AVENIDA ATAULFO DE PAIVA, 341 - LOJA B, Rio de Janeiro, Brazil",
            morada2: "861",
            population: 500 ,codPostal: "2511-6807"
        }, {
            lat: -22.9757113,
            lng: -43.1897342,
            nome: "Droga Raia",
            morada1: "AV.NOSSA SENHORA COPACABANA, 919 - LOJA A, Rio de Janeiro, Brazil",
            morada2: "862",
            population: 500 ,codPostal: "2548-0524"
        }, {
            lat: -22.9674643,
            lng: -43.1824619,
            nome: "Droga Raia",
            morada1: "AV. NOSSA SENHORA COPACABANA, 442 , Rio de Janeiro, Brazil",
            morada2: "863",
            population: 500 ,codPostal: "2548-8262"
        }, {
            lat: -22.9049666,
            lng: -43.1764118,
            nome: "Droga Raia",
            morada1: "RUA DA ASSEMBLEIA, 56 - LOJA A, Rio de Janeiro, Brazil",
            morada2: "864",
            population: 500 ,codPostal: "2224 0860"
        }, {
            lat: -22.9633973,
            lng: -43.175946,
            nome: "Droga Raia",
            morada1: "RUA MINISTRO VIVEIRO DE CASTRO, 46, Rio de Janeiro, Brazil",
            morada2: "865",
            population: 500 ,codPostal: "2543-7209"
        }, {
            lat: -22.9731661,
            lng: -43.1880772,
            nome: "Droga Raia",
            morada1: "AV. NOSSA SENHORA DE COPACABANA, 787 - LOJA A, Rio de Janeiro, Brazil",
            morada2: "866",
            population: 500 ,codPostal: "2547 - 2532"
        }, {
            lat: -22.9859135,
            lng: -43.22795,
            nome: "Droga Raia",
            morada1: "AVENIDA ATAULFO DE PAIVA, 1282 - LOJA A, Rio de Janeiro, Brazil",
            morada2: "867",
            population: 500 ,codPostal: "2274-8023"
        }, {
            lat: -22.9751216,
            lng: -43.227546,
            nome: "Droga Raia",
            morada1: "AVENIDA MARQUES DE S?O VICENTE, 22, Rio de Janeiro, Brazil",
            morada2: "868",
            population: 500 ,codPostal: "2239-4913"
        }, {
            lat: -22.9647012,
            lng: -43.217769,
            nome: "Droga Raia",
            morada1: "RUA JARDIM BOT?NICO, 647 - LOJA A, Rio de Janeiro, Brazil",
            morada2: "869",
            population: 500 ,codPostal: "2512-6095"
        }, {
            lat: -22.9997718,
            lng: -43.3846284,
            nome: "Droga Raia",
            morada1: "AVENIDA DAS AM?RICAS, 7000, Rio de Janeiro, Brazil",
            morada2: "870",
            population: 500 ,codPostal: " 2431-1081"
        }, {
            lat: -22.9045622,
            lng: -43.2886607,
            nome: "Droga Raia",
            morada1: "RUA DIAS DA CRUZ, 454, Rio de Janeiro, Brazil",
            morada2: "871",
            population: 500 ,codPostal: "2596-9887"
        }, {
            lat: -23.0123005,
            lng: -43.456601,
            nome: "Droga Raia",
            morada1: "AVENIDA DAS AM?RICAS, 14737 - LOJA A, Rio de Janeiro, Brazil",
            morada2: "872",
            population: 500 ,codPostal: "3150-9285"
        }, {
            lat: -22.9146665,
            lng: -43.2408278,
            nome: "Droga Raia",
            morada1: "BOULEVARD VINTE E OITO DE SETEMBRO, 150 - 150 A, Rio de Janeiro, Brazil",
            morada2: "873",
            population: 500 ,codPostal: "2569-2792"
        }, {
            lat: -22.8038578,
            lng: -43.2041752,
            nome: "Droga Raia",
            morada1: "RUA REPUBLICA ?RABE DA S?RIA, 383, Rio de Janeiro, Brazil",
            morada2: "874",
            population: 500 ,codPostal: "2462-2486"
        }, {
            lat: -22.9352325,
            lng: -43.1748623,
            nome: "Droga Raia",
            morada1: "RUA SENADOR VERGUEIRO, 73 - LOJA A, Rio de Janeiro, Brazil",
            morada2: "875",
            population: 500 ,codPostal: "2205-1583"
        }, {
            lat: -23.0059792,
            lng: -43.3174066,
            nome: "Droga Raia",
            morada1: "AVENIDA DAS AM?RICAS, 4666 - LOJA 211/A2, Rio de Janeiro, Brazil",
            morada2: "876",
            population: 500 ,codPostal: " 2431-9589"
        }, {
            lat: -22.9336971,
            lng: -43.1857232,
            nome: "Droga Raia",
            morada1: "RUA DAS LARANJEIRAS, 211 - LOJA A, Rio de Janeiro, Brazil",
            morada2: "877",
            population: 500 ,codPostal: "2552-1975"
        }, {
            lat: -23.0085843,
            lng: -43.4463688,
            nome: "Droga Raia",
            morada1: "AVENIDA DAS AM?RICAS, 13520 - LOJA B, Rio de Janeiro, Brazil",
            morada2: "878",
            population: 500 ,codPostal: "2437-0831"
        }, {
            lat: -22.9234993,
            lng: -43.2559134,
            nome: "Droga Raia",
            morada1: "RUA BAR?O DE MESQUITA, 944 - A SALA 201, Rio de Janeiro, Brazil",
            morada2: "879",
            population: 500 ,codPostal: "3238-4373"
        }, {
            lat: -22.9016216,
            lng: -43.2792465,
            nome: "Droga Raia",
            morada1: "RUA SILVA RABELO, 10 - LOJA B,C E SL, Rio de Janeiro, Brazil",
            morada2: "880",
            population: 500 ,codPostal: "3274-1819"
        }, {
            lat: -22.9237229,
            lng: -43.2342597,
            nome: "Droga Raia",
            morada1: "RUA SANTO AFONSO, 356 - LOJA A, Rio de Janeiro, Brazil",
            morada2: "881",
            population: 500 ,codPostal: "2567-5568"
        }, {
            lat: -22.9823016,
            lng: -43.2166874,
            nome: "Droga Raia",
            morada1: "AVENIDA AFRANIO DE MELO FRANCO, 290 - LOJA 103A/103B, Rio de Janeiro, Brazil",
            morada2: "882",
            population: 500 ,codPostal: "2512-8759"
        }, {
            lat: -23.0014139,
            lng: -43.3310503,
            nome: "Droga Raia",
            morada1: "AVENIDA DAS AM?RICAS, 1699 - LOJA B, Rio de Janeiro, Brazil",
            morada2: "883",
            population: 500 ,codPostal: "2499-8388"
        }, {
            lat: -22.9986147,
            lng: -43.3605018,
            nome: "Droga Raia",
            morada1: "AVENIDA DAS AM?RICAS, 5000 - LOJA 116, Rio de Janeiro, Brazil",
            morada2: "884",
            population: 500 ,codPostal: "2408-3363"
        }, {
            lat: -22.9532966,
            lng: -43.1904769,
            nome: "Droga Raia",
            morada1: "RUA VOLUNT?RIOS DA P?TRIA, 248 - LOJA ,AP 101 E 102, Rio de Janeiro, Brazil",
            morada2: "885",
            population: 500 ,codPostal: "2286-1510"
        }, {
            lat: -22.9217339,
            lng: -43.3705065,
            nome: "Droga Raia",
            morada1: "AVENIDA NELSON CARDOSO, 1081 - LOJA, Rio de Janeiro, Brazil",
            morada2: "886",
            population: 500 ,codPostal: "2435-2774"
        }, {
            lat: -22.8745044,
            lng: -43.3367399,
            nome: "Droga Raia",
            morada1: "RUA CARVALHO DE SOUZA, 326, Rio de Janeiro, Brazil",
            morada2: "887",
            population: 500 ,codPostal: "3019-5261"
        }, {
            lat: -22.9404725,
            lng: -43.3384615,
            nome: "Droga Raia",
            morada1: "RUA TIROL, 356, Rio de Janeiro, Brazil",
            morada2: "888",
            population: 500 ,codPostal: "2447-7320"
        }, {
            lat: -22.9994866,
            lng: -43.4149389,
            nome: "Droga Raia",
            morada1: "AVENIDA DAS AM?RICAS, 10300 - T?RREO LOJA D, Rio de Janeiro, Brazil",
            morada2: "889",
            population: 500 ,codPostal: "3328-4983"
        }, {
            lat: -23.0043316,
            lng: -43.3176488,
            nome: "Droga Raia",
            morada1: "AVENIDA DAS AM?RICAS, 500 - LOJA 121/122, Rio de Janeiro, Brazil",
            morada2: "890",
            population: 500 ,codPostal: "3153-7915"
        }, {
            lat: -22.9836027,
            lng: -43.2126318,
            nome: "Droga Raia",
            morada1: "RUA VISCONDE DE PIRAJ?, 592, Rio de Janeiro, Brazil",
            morada2: "892",
            population: 500 ,codPostal: "2294-2456"
        }, {
            lat: -22.9742288,
            lng: -43.4135279,
            nome: "Droga Raia",
            morada1: "ESTRADA DOS BANDEIRANTES, 8591 - LOJA 1001, Rio de Janeiro, Brazil",
            morada2: "893",
            population: 500 ,codPostal: "3328-0056"
        }, {
            lat: -23.0207387,
            lng: -43.4532387,
            nome: "Droga Raia",
            morada1: "AVENIDA ALFREDO BALTAZAR DA SILVEIRA, 96 - LOJA A E B, Rio de Janeiro, Brazil",
            morada2: "895",
            population: 500 ,codPostal: "3326-3546"
        }, {
            lat: -22.8878523,
            lng: -43.284539,
            nome: "Droga Raia",
            morada1: "RUA JOS? BONIF?CIO, 1085, Rio de Janeiro, Brazil",
            morada2: "896",
            population: 500 ,codPostal: "3173-7129"
        }, {
            lat: -22.9243638,
            lng: -43.2282406,
            nome: "Droga Raia",
            morada1: "RUA CONDE DE BONFIM, 211 - LOJA A, B, Rio de Janeiro, Brazil",
            morada2: "897",
            population: 500 ,codPostal: "2569-2596"
        }, {
            lat: -22.927999,
            lng: -43.3589425,
            nome: "Droga Raia",
            morada1: "ESTRADA DO TINDIBA, 467 - LT01,03 E 04, Rio de Janeiro, Brazil",
            morada2: "898",
            population: 500 ,codPostal: "3197-0162"
        }, {
            lat: -23.0144732,
            lng: -43.4682156,
            nome: "Droga Raia",
            morada1: "AVENIDA DAS AM?RICAS, 15800, Rio de Janeiro, Brazil",
            morada2: "899",
            population: 500 ,codPostal: " 2439-7564"
        }, {
            lat: -22.9709569,
            lng: -43.3725154,
            nome: "Droga Raia",
            morada1: "AVENIDA EMBAIXADOR ABELARDO BUENO, 1300, Rio de Janeiro, Brazil",
            morada2: "900",
            population: 500 ,codPostal: "3095-9166"
        }, {
            lat: -22.9166759,
            lng: -43.3771793,
            nome: "Droga Raia",
            morada1: "ESTRADA DO TINDIBA, 2938, Rio de Janeiro, Brazil",
            morada2: "901",
            population: 500 ,codPostal: "2440-0192"
        }, {
            lat: -22.9223831,
            lng: -43.2473778,
            nome: "Droga Raia",
            morada1: "RUA MAXWELL, 431, Rio de Janeiro, Brazil",
            morada2: "902",
            population: 500 ,codPostal: "3177-1783"
        }, {
            lat: -22.9261121,
            lng: -43.1768785,
            nome: "Droga Raia",
            morada1: "RUA DO CATETE, 156 - LOJA 02, Rio de Janeiro, Brazil",
            morada2: "903",
            population: 500 ,codPostal: "2285-5377"
        }, {
            lat: -22.9996019,
            lng: -43.396412,
            nome: "Droga Raia",
            morada1: "AVENIDA DAS AM?RICAS, 1880 - LOJA 01, Rio de Janeiro, Brazil",
            morada2: "904",
            population: 500 ,codPostal: "2433-0226"
        }, {
            lat: -22.9288806,
            lng: -43.2372251,
            nome: "Droga Raia",
            morada1: "RUA CONDE DE BONFIM, 536, Rio de Janeiro, Brazil",
            morada2: "905",
            population: 500 ,codPostal: "3178-1066"
        }, {
            lat: -22.9287978,
            lng: -43.3541666,
            nome: "Droga Raia",
            morada1: "AVENIDA GEREM?RIO DANTAS, 0 - 47668, Rio de Janeiro, Brazil",
            morada2: "906",
            population: 500 ,codPostal: "2426-7630"
        }, {
            lat: -22.8054033,
            lng: -43.201896,
            nome: "Droga Raia",
            morada1: "ESTRADA DO GALE?O, 2007, Rio de Janeiro, Brazil",
            morada2: "907",
            population: 500 ,codPostal: "2462-3156"
        }, {
            lat: -22.9254351,
            lng: -43.2329026,
            nome: "Droga Raia",
            morada1: "RUA GENERAL ROCA, 661, Rio de Janeiro, Brazil",
            morada2: "908",
            population: 500 ,codPostal: "2548-4797"
        }, {
            lat: -22.948892,
            lng: -43.1840004,
            nome: "Droga Raia",
            morada1: "RUA MUNIZ BARRETO, 805, Rio de Janeiro, Brazil",
            morada2: "909",
            population: 500 ,codPostal: "3178-3602"
        }, {
            lat: -22.9115358,
            lng: -43.1749278,
            nome: "Droga Raia",
            morada1: "AVENIDA RIO BRANCO, 257, Rio de Janeiro, Brazil",
            morada2: "910",
            population: 500 ,codPostal: "3174-3290"
        }, {
            lat: -23.0157172,
            lng: -43.4698872,
            nome: "Droga Raia",
            morada1: "AVENIDA DAS AM?RICAS, 16057, Rio de Janeiro, Brazil",
            morada2: "911",
            population: 500 ,codPostal: "2437-7439"
        }, {
            lat: -23.0182133,
            lng: -43.4794422,
            nome: "Droga Raia",
            morada1: "AVENIDA DAS AM?RICAS, 17145, Rio de Janeiro, Brazil",
            morada2: "912",
            population: 500 ,codPostal: "3328-7432"
        }, {
            lat: -23.0132838,
            lng: -43.3050309,
            nome: "Droga Raia",
            morada1: "AVENIDA OLEG?RIO MACIEL, 194, Rio de Janeiro, Brazil",
            morada2: "913",
            population: 500 ,codPostal: "3153-9329"
        }, {
            lat: -22.9032263,
            lng: -43.2833061,
            nome: "Droga Raia",
            morada1: "RUA DIAS DA CRUZ, 224, Rio de Janeiro, Brazil",
            morada2: "914",
            population: 500 ,codPostal: "2592-0608"
        }, {
            lat: -22.8765093,
            lng: -43.4649606,
            nome: "Droga Raia",
            morada1: "AVENIDA C?NEGO DE VASCONCELOS, 120, Rio de Janeiro, Brazil",
            morada2: "915",
            population: 500 ,codPostal: "3309-3467"
        }, {
            lat: -22.9080671,
            lng: -43.5647182,
            nome: "Droga Raia",
            morada1: "AVENIDA CES?RIO DE MELO, 3423, Rio de Janeiro, Brazil",
            morada2: "916",
            population: 500 ,codPostal: "2412-0107"
        }, {
            lat: -22.902737,
            lng: -43.5591893,
            nome: "Droga Raia",
            morada1: "RUA CORONEL AGOSTINHO, 12, Rio de Janeiro, Brazil",
            morada2: "917",
            population: 500 ,codPostal: "2415-5231"
        }, {
            lat: -22.9742734,
            lng: -43.1889398,
            nome: "Droga Raia",
            morada1: "AVENIDA NOSSA SENHORA DE COPACABANA, 845, Rio de Janeiro, Brazil",
            morada2: "918",
            population: 500 ,codPostal: "2236-5713"
        }, {
            lat: -22.9350256,
            lng: -43.3356977,
            nome: "Droga Raia",
            morada1: "RUA ARAGUAIA, 1200, Rio de Janeiro, Brazil",
            morada2: "919",
            population: 500 ,codPostal: "2425-8902"
        }, {
            lat: -22.9655281,
            lng: -43.180695,
            nome: "Droga Raia",
            morada1: "RUA BARATA RIBEIRO, 197, Rio de Janeiro, Brazil",
            morada2: "920",
            population: 500 ,codPostal: "2547-9512"
        }, {
            lat: -22.9427172,
            lng: -43.3406688,
            nome: "Droga Raia",
            morada1: "ESTRADA JACAREPAGU?, 7578, Rio de Janeiro, Brazil",
            morada2: "921",
            population: 500 ,codPostal: "3392-0695"
        }, {
            lat: -22.93807,
            lng: -43.1774973,
            nome: "Droga Raia",
            morada1: "RUA MARQU?S DE ABRANTES, 143, Rio de Janeiro, Brazil",
            morada2: "922",
            population: 500 ,codPostal: "2553-5315"
        }, {
            lat: -22.985829,
            lng: -43.2286064,
            nome: "Droga Raia",
            morada1: "RUA S? FERREIRA, 25, Rio de Janeiro, Brazil",
            morada2: "923",
            population: 500 ,codPostal: "2523-3520"
        }, {
            lat: -22.8863566,
            lng: -43.2827438,
            nome: "Droga Raia",
            morada1: "AVENIDA DOM HELDER C?MARA, 5474, Rio de Janeiro, Brazil",
            morada2: "924",
            population: 500 ,codPostal: "3979-8669"
        }, {
            lat: -22.9219385,
            lng: -43.2353733,
            nome: "Droga Raia",
            morada1: "AVENIDA MARACAN?, 987, Rio de Janeiro, Brazil",
            morada2: "925",
            population: 500 ,codPostal: "2567-1128"
        }, {
            lat: -23.0100519,
            lng: -43.3506712,
            nome: "Droga Raia",
            morada1: "AVENIDA L?CIO COSTA, 5170, Rio de Janeiro, Brazil",
            morada2: "926",
            population: 500 ,codPostal: "2497-5552"
        }, {
            lat: -22.9571438,
            lng: -43.1767233,
            nome: "Droga Raia",
            morada1: "AVENIDA LAURO SODR?, 445, Rio de Janeiro, Brazil",
            morada2: "927",
            population: 500 ,codPostal: "2542-5962"
        }, {
            lat: -22.9795897,
            lng: -43.2233138,
            nome: "Droga Raia",
            morada1: "RUA ADALBERTO FERREIRA, 18, Rio de Janeiro, Brazil",
            morada2: "928",
            population: 500 ,codPostal: "2249-9487"
        }, {
            lat: -22.7430701,
            lng: -43.437457,
            nome: "Drogaria Uni?o",
            morada1: "Estr. do Igua?u, 382, Rio de Janeiro, Brazil",
            morada2: "929",
            population: 500 ,codPostal: "2768-4773"
        }, {
            lat: -22.8539974,
            lng: -43.3130624,
            nome: "Drogaria Uni?o",
            morada1: "Av. Autom?vel Clube, 15, Rio de Janeiro, Brazil",
            morada2: "930",
            population: 500 ,codPostal: "2679-3000"
        }, {
            lat: -22.856529,
            lng: -43.4547286,
            nome: "Drogaria Mais Barato",
            morada1: "Rua?Vigilante Fortunato, 580, Rio de Janeiro, Brazil",
            morada2: "931",
            population: 500 ,codPostal: "3466_-6000"
        }, {
            lat: -22.8485646,
            lng: -43.4652846,
            nome: "Drogaria Mais Barato",
            morada1: "Rua Roque Barbosa, s/n, lote 11, quadra C, Rio de Janeiro, Brazil",
            morada2: "932",
            population: 500 ,codPostal: "3335-_2000"
        }, {
            lat: -22.8892316,
            lng: -43.2379595,
            nome: "Drogaria Mais Barato",
            morada1: "Rua Senador Domicio Barreto, 296, Rio de Janeiro, Brazil",
            morada2: "933",
            population: 500 ,codPostal: "3890-_5551"
        }, {
            lat: -22.870362,
            lng: -43.3670246,
            nome: "Drogaria Mais Barato",
            morada1: "Rua Divis?ria, 355, loja B, Rio de Janeiro, Brazil",
            morada2: "934",
            population: 500 ,codPostal: "2451-7055"
        }, {
            lat: -22.8600408,
            lng: -43.2539264,
            nome: "Drogaria Mais Barato",
            morada1: "Avenida Teixeira de Castro, 121, Rio de Janeiro, Brazil",
            morada2: "935",
            population: 500 ,codPostal: "2290-1000"
        }, {
            lat: -22.8228808,
            lng: -43.2869277,
            nome: "Drogaria Mais Barato",
            morada1: "Rua Taborari, 866, Rio de Janeiro, Brazil",
            morada2: "936",
            population: 500 ,codPostal: "2485-2000"
        }, {
            lat: -22.8872347,
            lng: -43.2805409,
            nome: "Drogaria Mais Barato",
            morada1: "Av. Dom Helder C?mara, 5099, loja B, Rio de Janeiro, Brazil",
            morada2: "937",
            population: 500 ,codPostal: "2595-5146"
        }, {
            lat: -22.8911041,
            lng: -43.2694434,
            nome: "Drogaria Mais Barato",
            morada1: "Rua Miguel Cervantes, 140, loja H, Rio de Janeiro, Brazil",
            morada2: "938",
            population: 500 ,codPostal: "3529-_2030"
        }, {
            lat: -22.8710337,
            lng: -43.3129638,
            nome: "Drogaria Mais Barato",
            morada1: "Rua Silva Vale, 177, Rio de Janeiro, Brazil",
            morada2: "939",
            population: 500 ,codPostal: "2595_-7000"
        }, {
            lat: -22.8246712,
            lng: -43.3114837,
            nome: "Drogaria Mais Barato",
            morada1: "Rua Cordovil, 1672, loja C, Rio de Janeiro, Brazil",
            morada2: "941",
            population: 500 ,codPostal: "2482-_7506"
        }, {
            lat: -22.8224347,
            lng: -43.29468,
            nome: "Drogaria Mais Barato",
            morada1: "Estrada Porto Velho, 167, Rio de Janeiro, Brazil",
            morada2: "942",
            population: 500 ,codPostal: "2485-3239"
        }, {
            lat: -22.8917686,
            lng: -43.2884311,
            nome: "Drogaria Mais Barato",
            morada1: "Rua Doutor Padilha, 485, loja B, Rio de Janeiro, Brazil",
            morada2: "943",
            population: 500 ,codPostal: "2596-4000"
        }, {
            lat: -22.970249,
            lng: -43.1839371,
            nome: "Drogaria Mais Barato",
            morada1: "Rua Loasa, 23 lojas C e D, Rio de Janeiro, Brazil",
            morada2: "944",
            population: 500 ,codPostal: "2458-_7000"
        }, {
            lat: -22.831369,
            lng: -43.3780477,
            nome: "Drogaria Mais Barato",
            morada1: "Estrada Camboat?, 3083, Rio de Janeiro, Brazil",
            morada2: "945",
            population: 500 ,codPostal: "3107-_1412"
        }, {
            lat: -22.7901989,
            lng: -43.1714998,
            nome: "Drogaria Mais Barato",
            morada1: "Rua Magno Martins, 162 loja E, Rio de Janeiro, Brazil",
            morada2: "946",
            population: 500 ,codPostal: " 3366-2000 / 96764-0102"
        }, {
            lat: -22.8039571,
            lng: -43.1932181,
            nome: "Drogaria Mais Barato",
            morada1: "Rua Berna, 55, Rio de Janeiro, Brazil",
            morada2: "947",
            population: 500 ,codPostal: "2465-2000"
        }, {
            lat: -22.7980911,
            lng: -43.1887336,
            nome: "Drogaria Mais Barato",
            morada1: "Estrada do Dend?, 494, loja A, Rio de Janeiro, Brazil",
            morada2: "948",
            population: 500 ,codPostal: "2465_-1000"
        }, {
            lat: -22.7971816,
            lng: -43.1936561,
            nome: "Drogaria Mais Barato",
            morada1: "Estrada do Dend?, 1151, loja A, Rio de Janeiro, Brazil",
            morada2: "949",
            population: 500 ,codPostal: "3367_-7000"
        }, {
            lat: -22.8960551,
            lng: -43.252113,
            nome: "Drogaria Mais Barato",
            morada1: "Rua Conselheiro Mayrink, 405, loja A, Rio de Janeiro, Brazil",
            morada2: "950",
            population: 500 ,codPostal: "2581-8000"
        }, {
            lat: -22.9167503,
            lng: -43.3621643,
            nome: "Drogaria Mais Barato",
            morada1: "Avenida Nelson Cardoso, 51, Rio de Janeiro, Brazil",
            morada2: "951",
            population: 500 ,codPostal: "3382-6000"
        }, {
            lat: -22.9408051,
            lng: -43.3672209,
            nome: "Drogaria Mais Barato",
            morada1: "Est. Mal Miguel Salazar Men de Moraes, 16, Rio de Janeiro, Brazil",
            morada2: "952",
            population: 500 ,codPostal: "2443-0291"
        }, {
            lat: -22.8848233,
            lng: -43.2567957,
            nome: "Drogaria Mais Barato",
            morada1: "Rua Amaro Rangel, 68, Rio de Janeiro, Brazil",
            morada2: "953",
            population: 500 ,codPostal: "2228-_8000"
        }, {
            lat: -22.810924,
            lng: -43.321085,
            nome: "Drogaria Mais Barato",
            morada1: "Rua Franz Liszt, 44o loja A e B, Rio de Janeiro, Brazil",
            morada2: "954",
            population: 500 ,codPostal: "3346-8000"
        }, {
            lat: -22.9070577,
            lng: -43.2378298,
            nome: "Drogaria Mais Barato",
            morada1: "Rua Visconde de Niteroi, 522, Rio de Janeiro, Brazil",
            morada2: "955",
            population: 500 ,codPostal: "2569-_3232"
        }, {
            lat: -22.8758281,
            lng: -43.2483156,
            nome: "Drogaria Mais Barato",
            morada1: "Estrada de Manguinhos, 173, Rio de Janeiro, Brazil",
            morada2: "956",
            population: 500 ,codPostal: "2241-_6000"
        }, {
            lat: -22.8599552,
            lng: -43.3695179,
            nome: "Drogaria Mais Barato",
            morada1: "Rua Surici, 62, loja B, Rio de Janeiro, Brazil",
            morada2: "957",
            population: 500 ,codPostal: "2450_-1500"
        }, {
            lat: -22.8558948,
            lng: -43.2445336,
            nome: "Drogaria Mais Barato",
            morada1: "Rua Teixeira Ribeiro, 656, loja A, Rio de Janeiro, Brazil",
            morada2: "958",
            population: 500 ,codPostal: "3105_-6805"
        }, {
            lat: -22.8362114,
            lng: -43.4114948,
            nome: "Drogaria Mais Barato",
            morada1: "Estrada do Engenho Novo, 1691, loja A, Rio de Janeiro, Brazil",
            morada2: "959",
            population: 500 ,codPostal: "3017-_7000"
        }, {
            lat: -22.8879075,
            lng: -43.2595725,
            nome: "Drogaria Mais Barato",
            morada1: "Rua Darci Vargas, 96, Rio de Janeiro, Brazil",
            morada2: "960",
            population: 500 ,codPostal: "3885-_8660"
        }, {
            lat: -22.8403905,
            lng: -43.3017725,
            nome: "Drogaria Mais Barato",
            morada1: "Av. Braz de Pina, 1130, loja C, Rio de Janeiro, Brazil",
            morada2: "961",
            population: 500 ,codPostal: "3352-6000"
        }, {
            lat: -22.8784846,
            lng: -43.2910484,
            nome: "Drogaria Mais Barato",
            morada1: "Rua ?lvaro de Miranda, 241, loja B, Rio de Janeiro, Brazil",
            morada2: "962",
            population: 500 ,codPostal: "2591-992"
        }, {
            lat: -22.8839724,
            lng: -43.3171667,
            nome: "Drogaria Mais Barato",
            morada1: "Av. Dom Helder Camara, 9324, Rio de Janeiro, Brazil",
            morada2: "963",
            population: 500 ,codPostal: "3271_-6000"
        }, {
            lat: -22.8462415,
            lng: -43.3392601,
            nome: "Drogaria Mais Barato",
            morada1: "Estrada do Barro Vermelho, 1391, Rio de Janeiro, Brazil",
            morada2: "964",
            population: 500 ,codPostal: "2471_-1000"
        }, {
            lat: -22.8827688,
            lng: -43.4916584,
            nome: "Drogaria Mais Barato",
            morada1: "Rua Silvio Fontes, 100 loja K, Rio de Janeiro, Brazil",
            morada2: "965",
            population: 500 ,codPostal: "3839-2000"
        }, {
            lat: -22.8617477,
            lng: -43.2421864,
            nome: "Drogaria Mais Barato",
            morada1: "Rua Nova Jerusal?m, 497, Rio de Janeiro, Brazil",
            morada2: "966",
            population: 500 ,codPostal: "3868-_1618"
        }, {
            lat: -22.8597883,
            lng: -43.3450981,
            nome: "Drogaria Mais Barato",
            morada1: "Estrada Otaviano, 256, loja D, Rio de Janeiro, Brazil",
            morada2: "967",
            population: 500 ,codPostal: "3830_-0505"
        }, {
            lat: -22.8893112,
            lng: -43.2285352,
            nome: "Drogaria Mais Barato",
            morada1: "Rua Ricardo Machado 478, Rio de Janeiro, Brazil",
            morada2: "969",
            population: 500 ,codPostal: "3890-5043"
        }, {
            lat: -22.8443654,
            lng: -43.2621632,
            nome: "Drogaria Mais Barato",
            morada1: "Rua Guara?nas, 3, loja E, Rio de Janeiro, Brazil",
            morada2: "970",
            population: 500 ,codPostal: "3351_-4949"
        }, {
            lat: -22.8078939,
            lng: -43.3075352,
            nome: "Drogaria Mais Barato",
            morada1: "Rua Valentim Magalh?es, 334 B, Rio de Janeiro, Brazil",
            morada2: "971",
            population: 500 ,codPostal: "3488_-1000"
        }, {
            lat: -22.883098,
            lng: -43.5747349,
            nome: "Drogaria Mais Barato",
            morada1: "Estrada de Vig?rio, 1034, loja C, Rio de Janeiro, Brazil",
            morada2: "972",
            population: 500 ,codPostal: "3452_-8000"
        }, {
            lat: -22.9080656,
            lng: -43.6066396,
            nome: "Drogaria Mais Barato",
            morada1: "Get?lio Vargas, 10, Loja A e B, Rio de Janeiro, Brazil",
            morada2: "973",
            population: 500 ,codPostal: "2230_-3000"
        }, {
            lat: -22.8349082,
            lng: -43.3029212,
            nome: "Drogaria Mais Barato",
            morada1: "Estrada do Quitungo, 1371, Rio de Janeiro, Brazil",
            morada2: "974",
            population: 500 ,codPostal: "3455_-4000"
        }, {
            lat: -22.8353154,
            lng: -43.3047517,
            nome: "Drogaria Mais Barato",
            morada1: "Estrada do Quitungo, 1554, loja D, Rio de Janeiro, Brazil",
            morada2: "975",
            population: 500 ,codPostal: "3391_-3000"
        }, {
            lat: -22.9143352,
            lng: -43.1947606,
            nome: "Drogaria Mais Barato",
            morada1: "Av. 31 de Mar?o, 140, Rio de Janeiro, Brazil",
            morada2: "977",
            population: 500 ,codPostal: "2575-_7000"
        }, {
            lat: -22.8762303,
            lng: -43.4636902,
            nome: "Drogaria Mais Barato",
            morada1: "Rua Silva Cardoso, 68, Rio de Janeiro, Brazil",
            morada2: "978",
            population: 500 ,codPostal: "2776-_3000"
        }, {
            lat: -22.8294293,
            lng: -43.4003761,
            nome: "Drogaria Mais Barato",
            morada1: "Rua Jo?o Moreira, s/n, lote 747, Rio de Janeiro, Brazil",
            morada2: "979",
            population: 500 ,codPostal: "2736_-4000"
        }, {
            lat: -22.8438089,
            lng: -43.3309505,
            nome: "Drogaria Mais Barato",
            morada1: "Av. Autom?vel Clube, 2679, loja 3, Rio de Janeiro, Brazil",
            morada2: "980",
            population: 500 ,codPostal: "2739-_2000"
        }, {
            lat: -22.8720379,
            lng: -43.1152473,
            nome: "Drogaria Mais Barato",
            morada1: "Rua M?rio Neves, 245, Rio de Janeiro, Brazil",
            morada2: "981",
            population: 500 ,codPostal: "2620-_1000"
        }, {
            lat: -22.7428129,
            lng: -43.485969,
            nome: "Drogaria Mais Barato",
            morada1: "Rua Tom?z Fonseca, 217, Rio de Janeiro, Brazil",
            morada2: "982",
            population: 500 ,codPostal: "2767-_6000"
        }, {
            lat: -22.7428739,
            lng: -43.4974095,
            nome: "Drogaria Mais Barato",
            morada1: "Rua Kilvio Santos, 1585, lote 90, Rio de Janeiro, Brazil",
            morada2: "983",
            population: 500 ,codPostal: "2767-_7000"
        }, {
            lat: -22.7714181,
            lng: -43.5223505,
            nome: "Drogaria Mais Barato",
            morada1: "Av. Ab?lio Augusto Tavora, 4210, loja 1A, Rio de Janeiro, Brazil",
            morada2: "984",
            population: 500 ,codPostal: "3778-_5000"
        }, {
            lat: -22.7089067,
            lng: -43.5547242,
            nome: "Drogaria Mais Barato",
            morada1: "Av. Doutor Pedro Jorge, 803, loja 2, Rio de Janeiro, Brazil",
            morada2: "985",
            population: 500 ,codPostal: "2655-6000"
        }, {
            lat: -22.9069337,
            lng: -43.2382105,
            nome: "Drogaria Mais Barato",
            morada1: "Rua Niter?i, 548 loja, Rio de Janeiro, Brazil",
            morada2: "986",
            population: 500 ,codPostal: "2771-9698 / 98404-3003"
        }, {
            lat: -22.9118051,
            lng: -43.2426331,
            nome: "Drogaria Mais Barato",
            morada1: "Rua?Duque de Caxias, 142, Rio de Janeiro, Brazil",
            morada2: "987",
            population: 500 ,codPostal: "2771-0263 / 99974-0742"
        }, {
            lat: -22.801363,
            lng: -43.0064891,
            nome: "Drogarias Mais Barato",
            morada1: "Rua Jo?o Floriano, 1420,, lt15, qd 101, Rio de Janeiro, Brazil",
            morada2: "988",
            population: 500 ,codPostal: "3710-3000"
        }, {
            lat: -22.8503513,
            lng: -43.3789501,
            nome: "Drogarias Mais Sa?de",
            morada1: "Rua Soldado Jos? Lopes Filho, 366, Rio de Janeiro, Brazil",
            morada2: "989",
            population: 500 ,codPostal: "3355-_2076"
        }, {
            lat: -22.884542,
            lng: -43.256367,
            nome: "Drogarias Mais Sa?de",
            morada1: "Rua Amaro Rangel, 18, Rio de Janeiro, Brazil",
            morada2: "990",
            population: 500 ,codPostal: "3688_-4318/ 2261-_5488 / 98051-6313"
        }, {
            lat: -22.7979281,
            lng: -43.190493,
            nome: "Drogarias Mais Sa?de",
            morada1: "Estrada do Dend?, 746, lojas E e B, Rio de Janeiro, Brazil",
            morada2: "991",
            population: 500 ,codPostal: "2465_-3000"
        }, {
            lat: -22.8437122,
            lng: -43.2833046,
            nome: "Drogarias Mais Sa?de",
            morada1: "Rua Engenheiro Francisco Passos, 357, Rio de Janeiro, Brazil",
            morada2: "992",
            population: 500 ,codPostal: "2590-2835"
        }, {
            lat: -22.8538879,
            lng: -43.3151995,
            nome: "Drogarias Mais Sa?de",
            morada1: "Rua Guaraunas, 36, loja A, Rio de Janeiro, Brazil",
            morada2: "993",
            population: 500 ,codPostal: "3459_-1000"
        }, {
            lat: -22.9006592,
            lng: -43.2772341,
            nome: "Drogarias Farta",
            morada1: "R. Santa F?, 5, Rio de Janeiro, Brazil",
            morada2: "994",
            population: 500 ,codPostal: "2241-0025"
        }, {
            lat: -22.9788376,
            lng: -43.1905071,
            nome: "Drogarias Farta",
            morada1: "Rua Djalma Ulrich, 110 loja D, Rio de Janeiro, Brazil",
            morada2: "995",
            population: 500 ,codPostal: "2521-0360"
        }, {
            lat: -22.8779097,
            lng: -43.3428745,
            nome: "Drogarias Farta",
            morada1: "Rua Cap Couto Menezes, 4, Rio de Janeiro, Brazil",
            morada2: "996",
            population: 500 ,codPostal: "33509504"
        }, {
            lat: -22.9142449,
            lng: -43.2046697,
            nome: "Drogarias Carioca",
            morada1: "AV. EST?CIO DE S?, 77, Rio de Janeiro, Brazil",
            morada2: "997",
            population: 500 ,codPostal: "2273-4000 / 2502-6778"
        }, {
            lat: -22.912853,
            lng: -43.2026525,
            nome: "Drogarias Carioca",
            morada1: "AV. SALVADOR DE S?, 218, Rio de Janeiro, Brazil",
            morada2: "998",
            population: 500 ,codPostal: "2273-9749 / 2502-6969"
        }, {
            lat: -22.8972367,
            lng: -43.1906912,
            nome: "Drogarias Carioca",
            morada1: "RUA SACADURA CABRAL, 265, Rio de Janeiro, Brazil",
            morada2: "999",
            population: 500 ,codPostal: "2516-1000"
        }, {
            lat: -22.9743897,
            lng: -43.2849521,
            nome: "Drogarias Carioca",
            morada1: "ESTR. ITAPICURU, 57 LJ B, Rio de Janeiro, Brazil",
            morada2: "1000",
            population: 500 ,codPostal: "3389-0139 / 3389-0141"
        }, {
            lat: -22.9005914,
            lng: -43.2679406,
            nome: "Drogarias Carioca",
            morada1: "RUA SOUSA BARROS, 450 - LJS E/F, Rio de Janeiro, Brazil",
            morada2: "1001",
            population: 500 ,codPostal: "2501-9000 / 2281-4654"
        }, {
            lat: -22.9006592,
            lng: -43.2772341,
            nome: "Drogarias Carioca",
            morada1: "RUA SANTA F?, 5 - LJ B E C, Rio de Janeiro, Brazil",
            morada2: "1002",
            population: 500 ,codPostal: "2241-0025 / 2501-4492"
        }, {
            lat: -22.8914637,
            lng: -43.2828038,
            nome: "Drogarias Carioca",
            morada1: "RUA JOSE BONIFACIO, 658, Rio de Janeiro, Brazil",
            morada2: "1003",
            population: 500 ,codPostal: "3899-6135 / 2599-1000"
        }, {
            lat: -22.8901415,
            lng: -43.2735962,
            nome: "Drogarias Carioca",
            morada1: "RUA CACHAMBI, 254, Rio de Janeiro, Brazil",
            morada2: "1004",
            population: 500 ,codPostal: "2501-8979 / 2281-7135 / 2261-000"
        }, {
            lat: -22.8913818,
            lng: -43.2686771,
            nome: "Drogarias Carioca",
            morada1: "RUA MIGUEL ?NGELO, N? 678, Rio de Janeiro, Brazil",
            morada2: "1005",
            population: 500 ,codPostal: "3297-1000"
        }, {
            lat: -22.885199,
            lng: -43.3111321,
            nome: "Drogarias Carioca",
            morada1: "AV. DOM HELDER CAMARA, 8701, Rio de Janeiro, Brazil",
            morada2: "1006",
            population: 500 ,codPostal: "3315-2000"
        }, {
            lat: -22.8547287,
            lng: -43.262405,
            nome: "Drogarias Carioca",
            morada1: "RUA EUCLIDES FARIA, 51 LJ A, Rio de Janeiro, Brazil",
            morada2: "1008",
            population: 500 ,codPostal: "2290-9000"
        }, {
            lat: -22.8427892,
            lng: -43.2615573,
            nome: "Drogarias Carioca",
            morada1: "RUA ANDRE AZEVEDO, 107 LJ.A, Rio de Janeiro, Brazil",
            morada2: "1009",
            population: 500 ,codPostal: "3867-6006"
        }, {
            lat: -22.846845,
            lng: -43.2654195,
            nome: "Drogarias Carioca",
            morada1: "RUA ANG?LICA MOTA, 490, Rio de Janeiro, Brazil",
            morada2: "1010",
            population: 500 ,codPostal: "3495-0200 / 3341-1873"
        }, {
            lat: -22.8403905,
            lng: -43.3017725,
            nome: "Drogarias Carioca",
            morada1: "AV. BR?S DE PINA, 1130 - LJ C , Rio de Janeiro, Brazil",
            morada2: "1011",
            population: 500 ,codPostal: "3352-6000"
        }, {
            lat: -22.8890159,
            lng: -43.3282756,
            nome: "Drogarias Carioca",
            morada1: "RUA CLARIMUNDO DE MELO, 1135 - LJ A, Rio de Janeiro, Brazil",
            morada2: "1012",
            population: 500 ,codPostal: "2596-8091"
        }, {
            lat: -22.8263866,
            lng: -43.3157421,
            nome: "Drogarias Carioca",
            morada1: "ESTR. DA ?GUA GRANDE, 1.168, Rio de Janeiro, Brazil",
            morada2: "1013",
            population: 500 ,codPostal: "3373-1551"
        }, {
            lat: -22.8496038,
            lng: -43.3598798,
            nome: "Drogarias Carioca",
            morada1: "RUA TACARATU, 417 - LJ A/B, Rio de Janeiro, Brazil",
            morada2: "1014",
            population: 500 ,codPostal: "3357-9000 / 2451-5607"
        }, {
            lat: -22.9270554,
            lng: -43.2513371,
            nome: "Drogarias Carioca",
            morada1: "RUA LEOPOLDO, 227 LOJA B, Rio de Janeiro, Brazil",
            morada2: "1015",
            population: 500 ,codPostal: "2238-5796"
        }, {
            lat: -23.0025639,
            lng: -43.3502703,
            nome: "Drogarias Carioca",
            morada1: "AV. MARECHAL HENRIQUE LOTT, 120 LJ 114, Rio de Janeiro, Brazil",
            morada2: "1016",
            population: 500 ,codPostal: "3325-4000"
        }, {
            lat: -22.9984286,
            lng: -43.3352334,
            nome: "Drogarias Carioca",
            morada1: "AV. DAS AM?RICAS, 2000 LOJAS 32 e 33, Rio de Janeiro, Brazil",
            morada2: "1018",
            population: 500 ,codPostal: "2439-1818 / 2439-1731"
        }, {
            lat: -22.9601437,
            lng: -43.3910484,
            nome: "Drogarias Carioca",
            morada1: "ESTR. DOS BANDEIRANTES, 5.258 - LJ A , Rio de Janeiro, Brazil",
            morada2: "1019",
            population: 500 ,codPostal: "3431-8000 / 2443-3001"
        }, {
            lat: -22.9571458,
            lng: -43.3316915,
            nome: "Drogarias Carioca",
            morada1: "RUA ARATICUM, 627 LJ A, Rio de Janeiro, Brazil",
            morada2: "1020",
            population: 500 ,codPostal: "3417-8000"
        }, {
            lat: -22.9167503,
            lng: -43.3621643,
            nome: "Drogarias Carioca",
            morada1: "AV. NELSON CARDOSO, 51, Rio de Janeiro, Brazil",
            morada2: "1021",
            population: 500 ,codPostal: "3413-9513 / 3382-6000"
        }, {
            lat: -22.9548506,
            lng: -43.3376864,
            nome: "Drogarias Carioca",
            morada1: "ESTR. DE JACAREPAGU?, 6.135 - LJ A, Rio de Janeiro, Brazil",
            morada2: "1022",
            population: 500 ,codPostal: "2436-0000"
        }, {
            lat: -22.9905562,
            lng: -43.3214637,
            nome: "Drogarias Carioca",
            morada1: "AV. ENGENHEIRO SOUZA FILHO, 42, Rio de Janeiro, Brazil",
            morada2: "1023",
            population: 500 ,codPostal: "2447-0808"
        }, {
            lat: -22.8173351,
            lng: -43.4103403,
            nome: "Drogarias Carioca",
            morada1: "AV. GETULIO DE MOURA, 345 LT 51 QD 1, Rio de Janeiro, Brazil",
            morada2: "1024",
            population: 500 ,codPostal: "3762-2000"
        }, {
            lat: -22.5058485,
            lng: -43.1721593,
            nome: "Drogarias Carioca",
            morada1: "AV. UM, 10 - BOX B, Rio de Janeiro, Brazil",
            morada2: "1025",
            population: 500 ,codPostal: "3888-5801 / 2573-6824"
        }, {
            lat: -22.6233497,
            lng: -43.2050286,
            nome: "Drogarias Carioca",
            morada1: "AV. CORONEL SISSON, 439, Rio de Janeiro, Brazil",
            morada2: "1026",
            population: 500 ,codPostal: "3661-4000"
        }, {
            lat: -22.8390527,
            lng: -43.3547525,
            nome: "Drogaria Rio Minas",
            morada1: "R Urura?, 460??Loja F, Rio de Janeiro, Brazil",
            morada2: "1027",
            population: 500 ,codPostal: "2471-7181"
        }, {
            lat: -22.8652215,
            lng: -43.273695,
            nome: "Rede Droga Minas",
            morada1: "Tv. Guadalajara, 173, Rio de Janeiro, Brazil",
            morada2: "1028",
            population: 500 ,codPostal: "3274-7000"
        }, {
            lat: -22.7942321,
            lng: -43.3372048,
            nome: "Rede Droga Minas",
            morada1: "R. Tubiacanga, 212, Rio de Janeiro, Brazil",
            morada2: "1029",
            population: 500 ,codPostal: "3105-5496"
        }, {
            lat: -22.7971084,
            lng: -43.5563601,
            nome: "Rede Droga Minas",
            morada1: "Av. Ab?lio Augusto T?vora, 9409, Rio de Janeiro, Brazil",
            morada2: "1030",
            population: 500 ,codPostal: "3776-1000"
        }, {
            lat: -22.7468629,
            lng: -43.4043179,
            nome: "RJ Drogarias",
            morada1: "Estrada Retiro da Imprensa, 1314, Rio de Janeiro, Brazil",
            morada2: "1031",
            population: 500 ,codPostal: "2761-2970"
        }, {
            lat: -22.7396802,
            lng: -43.4092574,
            nome: "RJ Drogarias",
            morada1: "Av. Heli?polis, 1152, Rio de Janeiro, Brazil",
            morada2: "1032",
            population: 500 ,codPostal: "2761-2408"
        }, {
            lat: -22.7835377,
            lng: -43.386089,
            nome: "RJ Drogarias",
            morada1: "Av. Dr. D?lio Guaran?, 62 -Lj A, Rio de Janeiro, Brazil",
            morada2: "1033",
            population: 500 ,codPostal: "2655-6191"
        }, {
            lat: -22.8964049,
            lng: -43.2893586,
            nome: "RJ Drogarias",
            morada1: "R. Curupaiti, 34, Rio de Janeiro, Brazil",
            morada2: "1034",
            population: 500 ,codPostal: "2667-2587"
        }, {
            lat: -22.83303,
            lng: -43.3289756,
            nome: "Drogarias Salom?o",
            morada1: "Endere?o:?R. Jos? Borges, 27, Rio de Janeiro, Brazil",
            morada2: "1035",
            population: 500 ,codPostal: "2473-8000"
        }, {
            lat: -22.9012975,
            lng: -43.1806298,
            nome: "Drogaria S?o Paulo",
            morada1: "Av. Presidente Vargas, 502 A, Rio de Janeiro, Brazil",
            morada2: "1036",
            population: 500 ,codPostal: "2516-3993 / 2518-1515"
        }, {
            lat: -22.9020198,
            lng: -42.0409338,
            nome: "Drogaria S?o Paulo",
            morada1: "Rua S?o Jo?o, 47, Rio de Janeiro, Brazil",
            morada2: "1037",
            population: 500 ,codPostal: "2613-4142 / 2622-6462"
        }, {
            lat: -22.8201523,
            lng: -43.0036591,
            nome: "Drogarias Tamoio",
            morada1: "Rua Yolanda Saad Abuzaid, 31, Rio de Janeiro, Brazil",
            morada2: "1038",
            population: 500 ,codPostal: "3605-0000"
        }, {
            lat: -22.8192881,
            lng: -43.0055046,
            nome: "Drogarias Tamoio",
            morada1: "Rua Dr. ALfredo Backer, 579 Blc A7 Lj 4 A 8 Lj 1, Rio de Janeiro, Brazil",
            morada2: "1039",
            population: 500 ,codPostal: "3708-3000 / 3710-9000"
        }, {
            lat: -22.9062878,
            lng: -43.1738183,
            nome: "Drogarias Tamoio",
            morada1: "Avenida Nilo Pe?anha, 14, Rio de Janeiro, Brazil",
            morada2: "1040",
            population: 500 ,codPostal: "2724-4697 /2724-4700 "
        }, {
            lat: -22.8196163,
            lng: -43.3817429,
            nome: "Drogarias Tamoio",
            morada1: "Rua Coronel Moreira Cesar, 03 , Rio de Janeiro, Brazil",
            morada2: "1041",
            population: 500 ,codPostal: "3713-3000 /2712-5051"
        }, {
            lat: -22.8196163,
            lng: -43.3817429,
            nome: "Drogarias Tamoio",
            morada1: "Rua Coronel Moreira Cesar, 15, Rio de Janeiro, Brazil",
            morada2: "1042",
            population: 500 ,codPostal: "2606-0000 /2605-1556"
        }, {
            lat: -22.8196163,
            lng: -43.3817429,
            nome: "Drogarias Tamoio",
            morada1: "Rua Coronel Moreira Cesar, 91, Rio de Janeiro, Brazil",
            morada2: "1043",
            population: 500 ,codPostal: "2605-0000 /2712-2424"
        }, {
            lat: -22.83321,
            lng: -43.0418594,
            nome: "Drogarias Tamoio",
            morada1: "Av. Humberto Alencar Castelo branco, 3382, Rio de Janeiro, Brazil",
            morada2: "1044",
            population: 500 ,codPostal: "2607-8000 /2712-3733"
        }, {
            lat: -22.83272,
            lng: -43.041698,
            nome: "Drogarias Tamoio",
            morada1: "Rua Jos? Louren?o Azevedo, 19 Loja B, Rio de Janeiro, Brazil",
            morada2: "1045",
            population: 500 ,codPostal: "3713-4000"
        }, {
            lat: -22.0571844,
            lng: -42.5175779,
            nome: "Drogarias Tamoio",
            morada1: "Rua Dr. Feliciano Sodr?, 219 , Rio de Janeiro, Brazil",
            morada2: "1046",
            population: 500 ,codPostal: "2605-4000"
        }, {
            lat: -22.8238051,
            lng: -43.0425598,
            nome: "Drogarias Tamoio",
            morada1: "Av.Presidente Kennedy, 425, Rio de Janeiro, Brazil",
            morada2: "1047",
            population: 500 ,codPostal: "3858-3100"
        }, {
            lat: -23.008392,
            lng: -44.3181479,
            nome: "Drogarias Tamoio",
            morada1: "Rua do Com?rcio, 236, Rio de Janeiro, Brazil",
            morada2: "1048",
            population: 500 ,codPostal: "3365-900024"
        }, {
            lat: -22.8730842,
            lng: -42.3371195,
            nome: "Drogarias Tamoio",
            morada1: "Rua Conselheiro Macedo soares,363, Rio de Janeiro, Brazil",
            morada2: "1049",
            population: 500 ,codPostal: "2664-400022"
        }, {
            lat: -22.892462,
            lng: -42.468179,
            nome: "Drogarias Tamoio",
            morada1: "Rua Prof. Francisco Fonseca, 135, Rio de Janeiro, Brazil",
            morada2: "1050",
            population: 500 ,codPostal: "2653-8080 22"
        }, {
            lat: -22.470319,
            lng: -43.825079,
            nome: "Drogarias Tamoio",
            morada1: "Rua Governador Portela, 62, Rio de Janeiro, Brazil",
            morada2: "1051",
            population: 500 ,codPostal: "2443-800124"
        }, {
            lat: -22.8800008,
            lng: -42.0191821,
            nome: "Drogarias Tamoio",
            morada1: "Avenida Teixeira e Souza, 49, Rio de Janeiro, Brazil",
            morada2: "1052",
            population: 500 ,codPostal: "2648-800022"
        }, {
            lat: -22.8351568,
            lng: -42.0298443,
            nome: "Drogarias Tamoio",
            morada1: "Av. ?zio Cardoso Fonseca, 30, Rio de Janeiro, Brazil",
            morada2: "1053",
            population: 500 ,codPostal: "2629-500022"
        }, {
            lat: -22.8821108,
            lng: -42.0277391,
            nome: "Drogarias Tamoio",
            morada1: "Largo de Santo Ant?nio, 39 Loja 1, Rio de Janeiro, Brazil",
            morada2: "1054",
            population: 500 ,codPostal: "2644-300022"
        }, {
            lat: -22.559094,
            lng: -42.6891521,
            nome: "Drogarias Tamoio",
            morada1: "Rua Floriano Peixoto, 173, Rio de Janeiro, Brazil",
            morada2: "1056",
            population: 500 ,codPostal: "2649-2222"
        }, {
            lat: -22.8712835,
            lng: -43.6288374,
            nome: "Drogarias Tamoio",
            morada1: "Av. Rui Barbosa, 1061, Rio de Janeiro, Brazil",
            morada2: "1057",
            population: 500 ,codPostal: "2734-690022"
        }, {
            lat: -21.7575604,
            lng: -41.325132,
            nome: "Drogarias Tamoio",
            morada1: "Rua Bar?o do Amazonas, 117, Rio de Janeiro, Brazil",
            morada2: "1058",
            population: 500 ,codPostal: "2734-690022"
        }, {
            lat: -21.759293,
            lng: -41.3352196,
            nome: "Drogarias Tamoio",
            morada1: "Av. Pelinca, 269, Rio de Janeiro, Brazil",
            morada2: "1059",
            population: 500 ,codPostal: "2731989822"
        }, {
            lat: -22.74699,
            lng: -42.8572856,
            nome: "Drogarias Tamoio",
            morada1: "Avenida 22 de Maio, 5714, Rio de Janeiro, Brazil",
            morada2: "1060",
            population: 500 ,codPostal: "2645-7201 /2645-4245"
        }, {
            lat: -22.7466577,
            lng: -42.8561099,
            nome: "Drogarias Tamoio",
            morada1: "Avenida 22 de Maio, 5.840, Rio de Janeiro, Brazil",
            morada2: "1061",
            population: 500 ,codPostal: "2635-2265 / 2645-4042"
        }, {
            lat: -22.7477169,
            lng: -42.8602999,
            nome: "Drogarias Tamoio",
            morada1: "Avenida 22 de Maio, 5386 Lj A, Rio de Janeiro, Brazil",
            morada2: "1062",
            population: 500 ,codPostal: "3639-3000"
        }, {
            lat: -21.2068956,
            lng: -41.8883395,
            nome: "Drogarias Tamoio",
            morada1: "Av. Cardoso Moreira, 322, Rio de Janeiro, Brazil",
            morada2: "1063",
            population: 500 ,codPostal: "3822-2828"
        }, {
            lat: -21.2090105,
            lng: -41.8874074,
            nome: "Drogarias Tamoio",
            morada1: "Av. Cardoso Moreira, 77, Rio de Janeiro, Brazil",
            morada2: "1064",
            population: 500 ,codPostal: "3824-3030"
        }, {
            lat: -22.9431148,
            lng: -43.1747363,
            nome: "Drogarias Tamoio",
            morada1: "Avenida Rui Barbosa, 795, Rio de Janeiro, Brazil",
            morada2: "1065",
            population: 500 ,codPostal: "2762-200022"
        }, {
            lat: -22.9408547,
            lng: -43.1718798,
            nome: "Drogarias Tamoio",
            morada1: "Av. Rui Barbosa, 407, Rio de Janeiro, Brazil",
            morada2: "1066",
            population: 500 ,codPostal: ", Tel.: (22) 2791-600022"
        }, {
            lat: -22.8648887,
            lng: -43.3069865,
            nome: "Drogarias Tamoio",
            morada1: "Av. Autom?vel Club, 2960, Rio de Janeiro, Brazil",
            morada2: "1068",
            population: 500 ,codPostal: "3739-1212"
        }, {
            lat: -23.023966,
            lng: -43.4579134,
            nome: "Drogarias Tamoio",
            morada1: "P?a. Conselheiro Macedo Soares, 126, Rio de Janeiro, Brazil",
            morada2: "1069",
            population: 500 ,codPostal: "3731-1010"
        }, {
            lat: -22.8933074,
            lng: -43.1230422,
            nome: "Drogarias Tamoio",
            morada1: "Avenida Ernani do Amaral Peixoto,55, Rio de Janeiro, Brazil",
            morada2: "1070",
            population: 500 ,codPostal: "2621-3453"
        }, {
            lat: -22.8933128,
            lng: -43.1220319,
            nome: "Drogarias Tamoio",
            morada1: "Avenida Ernani do Amaral Peixoto, 171, Rio de Janeiro, Brazil",
            morada2: "1071",
            population: 500 ,codPostal: "2704-3000"
        }, {
            lat: -22.9035697,
            lng: -43.1104686,
            nome: "Drogarias Tamoio",
            morada1: "Rua Presidente Backer, 155 Lj. 4, Rio de Janeiro, Brazil",
            morada2: "1072",
            population: 500 ,codPostal: "3603-2000 /2610-5030"
        }, {
            lat: -22.8239269,
            lng: -43.3787256,
            nome: "Drogarias Tamoio",
            morada1: "Rua Coronel Moreira Cesar 101, Rio de Janeiro, Brazil",
            morada2: "1073",
            population: 500 ,codPostal: "2611-2222 /2704-1000"
        }, {
            lat: -22.8196163,
            lng: -43.3817429,
            nome: "Drogarias Tamoio",
            morada1: "Rua Coronel Moreira Cesar, 38 Lj 101, Rio de Janeiro, Brazil",
            morada2: "1074",
            population: 500 ,codPostal: "2722-3030 /2620-2160"
        }, {
            lat: -22.8981769,
            lng: -43.1031353,
            nome: "Drogarias Tamoio",
            morada1: "Rua Noronha Torrez?o, 10 Lj E e F, Rio de Janeiro, Brazil",
            morada2: "1075",
            population: 500 ,codPostal: "3602-6000 / 3602-7000"
        }, {
            lat: -22.9371472,
            lng: -43.0623877,
            nome: "Drogarias Tamoio",
            morada1: "Estrada Francisco da Cruz Nunes, 1604 Lj 106, Rio de Janeiro, Brazil",
            morada2: "1076",
            population: 500 ,codPostal: "2709-9000 / 2704-4000"
        }, {
            lat: -22.9406194,
            lng: -43.027994,
            nome: "Drogarias Tamoio",
            morada1: "Av Ewerton Xavier, 1186, Rio de Janeiro, Brazil",
            morada2: "1077",
            population: 500 ,codPostal: "2705-2020"
        }, {
            lat: -22.8814745,
            lng: -43.08626,
            nome: "Drogarias Tamoio",
            morada1: "Alameda S?o Boaventura, 950, Rio de Janeiro, Brazil",
            morada2: "1078",
            population: 500 ,codPostal: "2627-5000 / 2627-6000"
        }, {
            lat: -22.9209676,
            lng: -43.0934484,
            nome: "Drogarias Tamoio",
            morada1: "Avenida Quintino Bocaiuva, 343 Lj 2, Rio de Janeiro, Brazil",
            morada2: "1079",
            population: 500 ,codPostal: "3701-9000 / 3701-3000"
        }, {
            lat: -22.9386742,
            lng: -43.1710365,
            nome: "Drogarias Tamoio",
            morada1: "Av. Ruy Barbosa 112 Loja 2, Rio de Janeiro, Brazil",
            morada2: "1080",
            population: 500 ,codPostal: "3603-3333 / 2714-5564 "
        }, {
            lat: -22.823659,
            lng: -43.3789145,
            nome: "Drogarias Tamoio",
            morada1: "R. Cel Moreira C?sar, 126, Loja 102, Rio de Janeiro, Brazil",
            morada2: "1081",
            population: 500 ,codPostal: "3602-9000"
        }, {
            lat: -22.8221904,
            lng: -43.3796658,
            nome: "Drogarias Tamoio",
            morada1: "R. Cel Moreira C?sar, 300, Rio de Janeiro, Brazil",
            morada2: "1082",
            population: 500 ,codPostal: "2704-5000"
        }, {
            lat: -22.9385847,
            lng: -43.1730834,
            nome: "Drogarias Tamoio",
            morada1: "Av. Rui Barbosa, 83, Rio de Janeiro, Brazil",
            morada2: "1084",
            population: 500 ,codPostal: "3701-1000 / 3811-7768"
        }, {
            lat: -22.8954295,
            lng: -43.1240773,
            nome: "Drogarias Tamoio",
            morada1: "Av. Visconde do Rio Branco, 505, Rio de Janeiro, Brazil",
            morada2: "1085",
            population: 500 ,codPostal: "2622-3000"
        }, {
            lat: -22.9067705,
            lng: -43.0559377,
            nome: "Drogarias Tamoio",
            morada1: "Estrada Caetano Monteiro, 1650, Rio de Janeiro, Brazil",
            morada2: "1086",
            population: 500 ,codPostal: "2716-2700"
        }, {
            lat: -22.9043501,
            lng: -43.1151195,
            nome: "Drogarias Tamoio",
            morada1: "Pra?a Get?lio Vargas, 168, Rio de Janeiro, Brazil",
            morada2: "1087",
            population: 500 ,codPostal: "2526-100022"
        }, {
            lat: -22.5092635,
            lng: -43.1743021,
            nome: "Drogarias Tamoio",
            morada1: "Rua do Imperador, 466 e 470, Rio de Janeiro, Brazil",
            morada2: "1088",
            population: 500 ,codPostal: "2246-100024"
        }, {
            lat: -22.8713202,
            lng: -43.4955327,
            nome: "Drogarias Tamoio",
            morada1: "Rua Brasil, 13, Rio de Janeiro, Brazil",
            morada2: "1089",
            population: 500 ,codPostal: "2739-7000 "
        }, {
            lat: -22.4651223,
            lng: -44.4475109,
            nome: "Drogarias Tamoio",
            morada1: "Avenida Albino de Almeida, 90, Rio de Janeiro, Brazil",
            morada2: "1090",
            population: 500 ,codPostal: "3355-070724"
        }, {
            lat: -22.8730206,
            lng: -43.4909742,
            nome: "Drogarias Tamoio",
            morada1: "Rua XV de Novembro, 188, Rio de Janeiro, Brazil",
            morada2: "1091",
            population: 500 ,codPostal: "2734-379421"
        }, {
            lat: -22.5251245,
            lng: -41.9412018,
            nome: "Drogarias Tamoio",
            morada1: "Rodovia Amaral Peixoto, 5019, Rio de Janeiro, Brazil",
            morada2: "1092",
            population: 500 ,codPostal: "2760-9000 22"
        }, {
            lat: -22.5241846,
            lng: -41.9397175,
            nome: "Drogarias Tamoio",
            morada1: "Rodovia Amaral Peixoto, 5181, Rio de Janeiro, Brazil",
            morada2: "1093",
            population: 500 ,codPostal: "2764-8000 22"
        }, {
            lat: -22.8360566,
            lng: -42.1016578,
            nome: "Drogarias Tamoio",
            morada1: "Rua Dr. Ant?nio Alves, 169, Rio de Janeiro, Brazil",
            morada2: "1094",
            population: 500 ,codPostal: "2625-500022"
        }, {
            lat: -22.837506,
            lng: -42.103237,
            nome: "Drogarias Tamoio",
            morada1: "Av. S?o Pedro, 60, Rio de Janeiro, Brazil",
            morada2: "1095",
            population: 500 ,codPostal: "2625-2525 22"
        }, {
            lat: -22.4124132,
            lng: -42.9670275,
            nome: "Drogarias Tamoio",
            morada1: "Av. Delfim Moreira, 391, Rio de Janeiro, Brazil",
            morada2: "1096",
            population: 500 ,codPostal: "2643-600021"
        }, {
            lat: -22.9063555,
            lng: -43.1786482,
            nome: "Drogarias Tamoio",
            morada1: "Av. Nilo Pe?anha, 319, Rio de Janeiro, Brazil",
            morada2: "1097",
            population: 500 ,codPostal: "2453-663324"
        }, {
            lat: -22.9026038,
            lng: -43.1721442,
            nome: "Drogarias Tamoio",
            morada1: "Pra?a XV - Esta??o das Barcas S.A., Rio de Janeiro, Brazil",
            morada2: "1098",
            population: 500 ,codPostal: "2215-1000 / 21 2215-481521"
        }, {
            lat: -22.7299912,
            lng: -43.4242715,
            nome: "Drogarias Tamoio",
            morada1: "Rua da Sociedade, 146, Rio de Janeiro, Brazil",
            morada2: "1099",
            population: 500 ,codPostal: "2762-4946"
        }, {
            lat: -22.7219945,
            lng: -43.3988888,
            nome: "Drogarias Tamoio",
            morada1: "Av. Nova Aurora S/N - Lt 14 Qd 12, Rio de Janeiro, Brazil",
            morada2: "1100",
            population: 500 ,codPostal: "2762-3000"
        }, {
            lat: -22.7036446,
            lng: -43.2796402,
            nome: "Drogarias Tamoio",
            morada1: "Av. Actura, QD. 03 Lt. 09?/ Loja 548-558, Rio de Janeiro, Brazil",
            morada2: "1101",
            population: 500 ,codPostal: "2776-1395"
        }, {
            lat: -22.7197309,
            lng: -43.523253,
            nome: "Drogarias Tamoio",
            morada1: "Rua Cel. Monteiro de Barros, 102, Rio de Janeiro, Brazil",
            morada2: "1102",
            population: 500 ,codPostal: "2763-4340"
        }, {
            lat: -22.9063773,
            lng: -43.1761852,
            nome: "Drogarias Tamoio",
            morada1: "Av. Nilo Pe?anha, 1.074, Rio de Janeiro, Brazil",
            morada2: "1103",
            population: 500 ,codPostal: "2669-3360"
        }, {
            lat: -22.7631429,
            lng: -43.5210641,
            nome: "Drogarias Tamoio",
            morada1: "Est. da Palhada 1.031, Rio de Janeiro, Brazil",
            morada2: "1105",
            population: 500 ,codPostal: "2698-9000"
        }, {
            lat: -22.6961486,
            lng: -43.5571026,
            nome: "Drogarias Tamoio",
            morada1: "Rua Dr. Pedro Jorge, 782, Rio de Janeiro, Brazil",
            morada2: "1106",
            population: 500 ,codPostal: "3778-9000 / 3770-4342"
        }, {
            lat: -22.8282138,
            lng: -43.4004388,
            nome: "Drogarias Tamoio",
            morada1: "Est. Marechal Alencastro, 3.695, Rio de Janeiro, Brazil",
            morada2: "1107",
            population: 500 ,codPostal: "3258-5295"
        }, {
            lat: -22.8273679,
            lng: -43.4182756,
            nome: "Drogarias Tamoio",
            morada1: "Rua Lyses Melga?o, 1370 / loja A, Rio de Janeiro, Brazil",
            morada2: "1108",
            population: 500 ,codPostal: "3358-4753"
        }, {
            lat: -22.8859885,
            lng: -43.4719943,
            nome: "Drogarias Tamoio",
            morada1: "Rua Rio da Prata, 968, Rio de Janeiro, Brazil",
            morada2: "1109",
            population: 500 ,codPostal: "3337-0707"
        }, {
            lat: -22.8887464,
            lng: -43.2432172,
            nome: "Drogarias Tamoio",
            morada1: "Av. Carlos Mattoso Correa, 198, Rio de Janeiro, Brazil",
            morada2: "1110",
            population: 500 ,codPostal: "2585-0945"
        }, {
            lat: -22.9434641,
            lng: -43.3625839,
            nome: "Drogarias Tamoio",
            morada1: "Rua Pintor Leandro Joaquim, 335 / Lojas A e B, Rio de Janeiro, Brazil",
            morada2: "1115",
            population: 500 ,codPostal: "3342-6794"
        }, {
            lat: -22.8653305,
            lng: -43.2938379,
            nome: "Drogarias Tamoio",
            morada1: "Est. Adhemar Bebiano, 4.063 Bloco D -Loja C/D, Rio de Janeiro, Brazil",
            morada2: "1116",
            population: 500 ,codPostal: "2289-4246"
        }, {
            lat: -22.87272,
            lng: -43.2804378,
            nome: "Drogarias Tamoio",
            morada1: "Estr. Adhemar Bebiano, 2.226, Rio de Janeiro, Brazil",
            morada2: "1117",
            population: 500 ,codPostal: "2594-1594"
        }, {
            lat: -22.8672445,
            lng: -43.2721919,
            nome: "Drogarias Tamoio",
            morada1: "Av. Itaoca, 1.884 Loja A, Rio de Janeiro, Brazil",
            morada2: "1118",
            population: 500 ,codPostal: "3866-2702"
        }, {
            lat: -22.8662744,
            lng: -43.33434,
            nome: "Drogarias Tamoio",
            morada1: "Av. Min. Edgar Romero, 460, Rio de Janeiro, Brazil",
            morada2: "1119",
            population: 500 ,codPostal: "3013-0199"
        }, {
            lat: -22.8602173,
            lng: -43.3703215,
            nome: "Drogarias Tamoio",
            morada1: "Rua Latife Luvizaro, Loja 59, Rio de Janeiro, Brazil",
            morada2: "1120",
            population: 500 ,codPostal: "2450-0747"
        }, {
            lat: -22.8840605,
            lng: -43.2488281,
            nome: "Drogarias Tamoio",
            morada1: "Rua Leopoldo Bulh?es, 700 / QD 27 LOJA 1, Rio de Janeiro, Brazil",
            morada2: "1121",
            population: 500 ,codPostal: "3890-6740 / 2580-2815"
        }, {
            lat: -22.8626189,
            lng: -43.3720113,
            nome: "Drogarias Tamoio",
            morada1: "Av. Gal. Osvaldo Cordeiro de Farias, 133 -, Rio de Janeiro, Brazil",
            morada2: "1122",
            population: 500 ,codPostal: "3011-9191"
        }, {
            lat: -22.8453757,
            lng: -43.2641681,
            nome: "Drogarias Tamoio",
            morada1: "Rua Ang?lica Mota, 282, Rio de Janeiro, Brazil",
            morada2: "1123",
            population: 500 ,codPostal: "3976-2002"
        }, {
            lat: -22.8545284,
            lng: -43.2607095,
            nome: "Drogarias Mundial",
            morada1: "Rua Uranos, 1.037, Rio de Janeiro, Brazil",
            morada2: "1124",
            population: 500 ,codPostal: "2590-7676"
        }, {
            lat: -22.9012496,
            lng: -43.2559174,
            nome: "Drogarias Mundial",
            morada1: "Rua Ana Neri, 2.078, Rio de Janeiro, Brazil",
            morada2: "1125",
            population: 500 ,codPostal: "2261-1555 / 2501-6260"
        }, {
            lat: -22.8394907,
            lng: -43.4007493,
            nome: "Drogarias Mundial",
            morada1: "Rua Japora, 174 Loja A, Rio de Janeiro, Brazil",
            morada2: "1126",
            population: 500 ,codPostal: "3358-0440"
        }, {
            lat: -22.9000264,
            lng: -43.2241285,
            nome: "Drogarias Mundial",
            morada1: "Rua S?o Luiz Gonzaga, 142, Rio de Janeiro, Brazil",
            morada2: "1127",
            population: 500 ,codPostal: "3860-3000"
        }, {
            lat: -22.8796223,
            lng: -43.5037771,
            nome: "Drogarias Mundial",
            morada1: "Av. de santa Cruz, 8.841, Rio de Janeiro, Brazil",
            morada2: "1128",
            population: 500 ,codPostal: "3335-1000"
        }, {
            lat: -22.9232999,
            lng: -43.3615386,
            nome: "Drogarias Mundial",
            morada1: "Av. Geremario Dantas, 308, Rio de Janeiro, Brazil",
            morada2: "1129",
            population: 500 ,codPostal: "3392-2000"
        }, {
            lat: -22.9356391,
            lng: -43.244424,
            nome: "Drogarias Mundial",
            morada1: "Rua Conde de Bonfim, 830, Rio de Janeiro, Brazil",
            morada2: "1130",
            population: 500 ,codPostal: "2575-1000"
        }, {
            lat: -22.88784,
            lng: -43.2612203,
            nome: "Drogarias Mundial",
            morada1: "Av. Get?lio Vargas, Loja 2.134, Rio de Janeiro, Brazil",
            morada2: "1131",
            population: 500 ,codPostal: "2723-3000"
        }, {
            lat: -22.7994929,
            lng: -43.3797877,
            nome: "Drogarias Mundial",
            morada1: "Av. Get?lio de Moura, 958 Loja B, Rio de Janeiro, Brazil",
            morada2: "1132",
            population: 500 ,codPostal: "3756-9000"
        }, {
            lat: -22.7856647,
            lng: -43.3899854,
            nome: "Drogarias Mundial",
            morada1: "Av. Dr. D?lio Guaran?, 304, Rio de Janeiro, Brazil",
            morada2: "1133",
            population: 500 ,codPostal: "2755-0200"
        }, {
            lat: -22.8916619,
            lng: -43.240826,
            nome: "Drogarias Tamba?",
            morada1: "Av.: Dom H?lder C?mara, 105, Rio de Janeiro, Brazil",
            morada2: "1134",
            population: 500 ,codPostal: "3087-7692"
        }, {
            lat: -22.9864382,
            lng: -43.1923911,
            nome: "Drogarias Tamba?",
            morada1: "Rua Joaquim Nabuco, 127 - Lojas C, D, E, Rio de Janeiro, Brazil",
            morada2: "1135",
            population: 500 ,codPostal: "2522-5555 / 2523-6190 / 2521-1454"
        },{
            lat: -23.0174737,
            lng: -43.4592418,
            nome: "Drogarias Tamba?",
            morada1: "Rua Fernando Leite Mendes, 61, Rio de Janeiro, Brazil",
            morada2: "1137",
            population: 500 ,codPostal: "2498-2000"
        }, {
            lat: -22.923588,
            lng: -43.3828712,
            nome: "Ennes Farma",
            morada1: "Estr. Rodrigues Caldas, 915, Rio de Janeiro, Brazil",
            morada2: "1138",
            population: 500 ,codPostal: "2443-8427"
        }, {
            lat: -22.8845231,
            lng: -43.3673438,
            nome: "Drogaria Mega Farma",
            morada1: "Rua Luiz Beltr?o, 14, Rio de Janeiro, Brazil",
            morada2: "1139",
            population: 500 ,codPostal: "2453-8000"
        }, {
            lat: -22.8503244,
            lng: -43.3602586,
            nome: "Drogaria Mega Farma",
            morada1: " R. Am?rico Rocha, 1497 / Loja B -, Rio de Janeiro, Brazil",
            morada2: "1140",
            population: 500 ,codPostal: "3390-5329"
        }, {
            lat: -22.9286497,
            lng: -43.3924559,
            nome: "Drogaria Mega Farma",
            morada1: "Estr. Rodrigues Caldas, 2257, Rio de Janeiro, Brazil",
            morada2: "1141",
            population: 500 ,codPostal: "2446-8223"
        }, {
            lat: -22.9186602,
            lng: -43.4058303,
            nome: "Drogaria Mega Farma",
            morada1: "Estr. do Rio Grande, 4000, Rio de Janeiro, Brazil",
            morada2: "1142",
            population: 500 ,codPostal: "2446-5393"
        }, {
            lat: -22.9186856,
            lng: -43.4069254,
            nome: "Drogaria Mega Farma",
            morada1: "Estr. do Rio Grande, 4176, Rio de Janeiro, Brazil",
            morada2: "1143",
            population: 500 ,codPostal: "2446-8001"
        }, {
            lat: -22.8388388,
            lng: -43.3500215,
            nome: "Drogaria Mega Farma",
            morada1: "Av. dos Italianos, 1089, Rio de Janeiro, Brazil",
            morada2: "1144",
            population: 500 ,codPostal: "2471-2020"
        }, {
            lat: -22.9909005,
            lng: -43.2521755,
            nome: "Drogaria Mega Farma",
            morada1: "Via ?pia da Rocinha, 19, Rio de Janeiro, Brazil",
            morada2: "1145",
            population: 500 ,codPostal: "3324-4000"
        }, {
            lat: -22.7917485,
            lng: -43.39575,
            nome: "Drogaria Mega Farma",
            morada1: "Av. Dr. D?lio Guaran?, 144, Rio de Janeiro, Brazil",
            morada2: "1146",
            population: 500 ,codPostal: "3756-2255"
        }, {
            lat: -22.9914206,
            lng: -43.2518652,
            nome: "Drogaria Mega Farma",
            morada1: "Tv. Liberdade, 4, Rio de Janeiro, Brazil",
            morada2: "1148",
            population: 500 ,codPostal: "3324-4000"
        }, {
            lat: -22.9493643,
            lng: -43.1888092,
            nome: "Farmacia Ruy Barbosa",
            morada1: "R. S?o Clemente, 188 - Loja A, Rio de Janeiro, Brazil",
            morada2: "1149",
            population: 500 ,codPostal: "2246-5000"
        }, {
            lat: -22.8787942,
            lng: -43.36299,
            nome: "Mega Expressa Drogaria",
            morada1: "R. das Rosas, 37 , Rio de Janeiro, Brazil",
            morada2: "1150",
            population: 500 ,codPostal: "2453-2525"
        }, {
            lat: -22.9168231,
            lng: -43.3871291,
            nome: "Mega Expressa Drogaria",
            morada1: "Av. dos Mananciais, 916, Rio de Janeiro, Brazil",
            morada2: "1151",
            population: 500 ,codPostal: "2440-7625"
        }, {
            lat: -22.9279345,
            lng: -43.1759343,
            nome: "Note Sul Drogarias",
            morada1: "R. Corr?a Dutra, 59 - Loja C, Rio de Janeiro, Brazil",
            morada2: "1152",
            population: 500 ,codPostal: "2245-8300"
        }, {
            lat: -22.9382071,
            lng: -43.1924863,
            nome: "Note Sul Drogarias",
            morada1: "R. das Laranjeiras, 466, Rio de Janeiro, Brazil",
            morada2: "1153",
            population: 500 ,codPostal: "9958-4164"
        }, {
            lat: -22.9331962,
            lng: -43.1759649,
            nome: "Note Sul Drogarias",
            morada1: "R. Bar?o do Flamengo, 35, Rio de Janeiro, Brazil",
            morada2: "1154",
            population: 500 ,codPostal: "2285-5000"
        }, {
            lat: -22.9248585,
            lng: -43.2507217,
            nome: "Note Sul Drogarias",
            morada1: "R. Bar?o de Mesquita, 796, Rio de Janeiro, Brazil",
            morada2: "1155",
            population: 500 ,codPostal: "2238-4040"
        }, {
            lat: -22.8243856,
            lng: -43.4047597,
            nome: "Rede de Drogarias Leg?tima",
            morada1: "Estrada Engenho Novo, 82 - Loja C, Rio de Janeiro, Brazil",
            morada2: "1156",
            population: 500 ,codPostal: "2455-5813"
        }, {
            lat: -22.8816499,
            lng: -43.3265704,
            nome: "Rede de Drogarias Leg?tima",
            morada1: "RUA SIDONEO PAES, 13, Rio de Janeiro, Brazil",
            morada2: "1157",
            population: 500 ,codPostal: "2785-5964"
        }, {
            lat: -22.9559853,
            lng: -43.1854365,
            nome: "Rede de Drogarias Leg?tima",
            morada1: "Rua General Polidoro, 156, Rio de Janeiro, Brazil",
            morada2: "1158",
            population: 500 ,codPostal: "2541-2000"
        }, {
            lat: -22.883423,
            lng: -42.025433,
            nome: "Rede de Drogarias Leg?tima",
            morada1: "Avenida Teixeira e Souza, 798, Rio de Janeiro, Brazil",
            morada2: "1159",
            population: 500 ,codPostal: "2647-368922"
        }, {
            lat: -22.9123041,
            lng: -42.0598531,
            nome: "Rede de Drogarias Leg?tima",
            morada1: "Avenida Adolfo Beranguer J?niot, Rio de Janeiro, Brazil",
            morada2: "1160",
            population: 500 ,codPostal: "2644-234422"
        }, {
            lat: -22.8852053,
            lng: -42.0272664,
            nome: "Rede de Drogarias Leg?tima",
            morada1: "Avenida Teixeira e Souza, 1037, Rio de Janeiro, Brazil",
            morada2: "1161",
            population: 500 ,codPostal: "2647 -537422"
        }, {
            lat: -22.775722,
            lng: -43.53141,
            nome: "Rede de Drogarias Leg?tima",
            morada1: "AV ABILIO AUGUSTO TAVORA,5177 QD G LT 03 LOJA , Rio de Janeiro, Brazil",
            morada2: "1162",
            population: 500 ,codPostal: "2882-8337"
        }, {
            lat: -22.8965326,
            lng: -43.2726936,
            nome: "Rede de Drogarias Leg?tima",
            morada1: "Rua Capita? Rezende, 403, Rio de Janeiro, Brazil",
            morada2: "1163",
            population: 500 ,codPostal: "2281-6780"
        }, {
            lat: -22.776248,
            lng: -43.316277,
            nome: "Rede de Drogarias Leg?tima",
            morada1: "Rua Coronel Alberto de Melo, 274, Rio de Janeiro, Brazil",
            morada2: "1164",
            population: 500 ,codPostal: "2674-4000"
        }, {
            lat: -22.7800554,
            lng: -43.3208962,
            nome: "Rede de Drogarias Leg?tima",
            morada1: "RUA SALDANHA MARINHO, 711 LOJA E, Rio de Janeiro, Brazil",
            morada2: "1165",
            population: 500 ,codPostal: "2652-6971/ 3651-3791 / 9 8135-5660"
        }, {
            lat: -22.8753447,
            lng: -43.3091084,
            nome: "Rede de Drogarias Leg?tima",
            morada1: "RUA MANOEL CORREIA, 23, Rio de Janeiro, Brazil",
            morada2: "1166",
            population: 500 ,codPostal: "2772-1000"
        }, {
            lat: -22.9063743,
            lng: -43.178286,
            nome: "Rede de Drogarias Leg?tima",
            morada1: "Avenida Nilo Pe?anha 364, Loja,, Rio de Janeiro, Brazil",
            morada2: "1167",
            population: 500 ,codPostal: "2652-1812/ 2772-4302"
        }, {
            lat: -22.8301063,
            lng: -43.0020659,
            nome: "Rede de Drogarias Leg?tima",
            morada1: "RUA CAPITAL JUVENAL FIGUEIREDO, 55 LOJA 03, Rio de Janeiro, Brazil",
            morada2: "1168",
            population: 500 ,codPostal: "2488-4424/ 2488-3259"
        }, {
            lat: -22.8471829,
            lng: -43.3255266,
            nome: "Rede de Drogarias Leg?tima",
            morada1: "Avenida Autom?vel Clube, 3329 / Loja C;, Rio de Janeiro, Brazil",
            morada2: "1169",
            population: 500 ,codPostal: "26594006 / 26494368"
        }, {
            lat: -22.7526985,
            lng: -43.304846,
            nome: "Rede de Drogarias Leg?tima",
            morada1: "Avenida Darci Vargas, 910, Rio de Janeiro, Brazil",
            morada2: "1170",
            population: 500 ,codPostal: "2672-0000"
        }, {
            lat: -22.408128,
            lng: -43.14779,
            nome: "Rede de Drogarias Leg?tima",
            morada1: "Estrada Uni?o e Industria, 11833, Rio de Janeiro, Brazil",
            morada2: "1171",
            population: 500 ,codPostal: "2222-252222"
        }, {
            lat: -22.3961024,
            lng: -43.1333835,
            nome: "Rede de Drogarias Leg?tima",
            morada1: "EST. UNI?O E IND?STRIA, 11.000, Rio de Janeiro, Brazil",
            morada2: "1172",
            population: 500 ,codPostal: "2222-355822"
        }, {
            lat: -22.9451147,
            lng: -43.1866836,
            nome: "Rede de Drogarias Leg?tima",
            morada1: "RUA PREFEITO ASSUMP??O, 185, Rio de Janeiro, Brazil",
            morada2: "1173",
            population: 500 ,codPostal: "3352-1371 /??24 3352-201322"
        }, {
            lat: -22.8028233,
            lng: -43.3141312,
            nome: "Rede de Drogarias Leg?tima",
            morada1: "Rua Vicente Celestino, S/N Lt.1069, Rio de Janeiro, Brazil",
            morada2: "1174",
            population: 500 ,codPostal: "2678-5027"
        }, {
            lat: -22.6795232,
            lng: -43.2751047,
            nome: "Rede de Drogarias Leg?tima",
            morada1: "ESTRADA DO ROS?RIO, 1219 - LOJA D, Rio de Janeiro, Brazil",
            morada2: "1175",
            population: 500 ,codPostal: "3939-0059"
        }, {
            lat: -22.9443187,
            lng: -43.3924039,
            nome: "Rede de Drogarias Leg?tima",
            morada1: "Est. Manoel de S? 15 Lj. A, Rio de Janeiro, Brazil",
            morada2: "1176",
            population: 500 ,codPostal: "3134-2059/ 3135-7002"
        }, {
            lat: -22.3607709,
            lng: -41.772738,
            nome: "Rede de Drogarias Leg?tima",
            morada1: "AV. LUIZ LIRIO, 1027 LOJA 01 E 02, Rio de Janeiro, Brazil",
            morada2: "1177",
            population: 500 ,codPostal: "2762-8536"
        }, {
            lat: -22.8050676,
            lng: -43.4178795,
            nome: "Rede de Drogarias Leg?tima",
            morada1: "AV. GET?LIO DE MOURA, 1921, Rio de Janeiro, Brazil",
            morada2: "1179",
            population: 500 ,codPostal: "3763-9999"
        }, {
            lat: -22.847531,
            lng: -43.0921812,
            nome: "Rede de Drogarias Leg?tima",
            morada1: "RUA DR. ALBERTO TORRES, 899, Rio de Janeiro, Brazil",
            morada2: "1180",
            population: 500 ,codPostal: "2615-8000"
        }, {
            lat: -22.427387,
            lng: -43.135229,
            nome: "Rede de Drogarias Leg?tima",
            morada1: "AVENIDA LEOPOLDINA, 230, Rio de Janeiro, Brazil",
            morada2: "1181",
            population: 500 ,codPostal: "2221-454624"
        }, {
            lat: -21.1331145,
            lng: -41.6843039,
            nome: "Rede de Drogarias Leg?tima",
            morada1: "P? DOUTOR WALDIR DA MOTA, 10 LOJA, Rio de Janeiro, Brazil",
            morada2: "1182",
            population: 500 ,codPostal: "3867- 5700 / 5701/ 5702"
        }, {
            lat: -22.75711,
            lng: -43.311645,
            nome: "Rede de Drogarias Leg?tima",
            morada1: "Rua Pedro Lessa, S/N LT 4 QD 37, Rio de Janeiro, Brazil",
            morada2: "1183",
            population: 500 ,codPostal: "2784-6200"
        }, {
            lat: -22.9725454,
            lng: -43.18808,
            nome: "Drogaria Onofre",
            morada1: "Avenida Nossa Senhora de Copacabana, 774, Rio de Janeiro, Brazil",
            morada2: "1184",
            population: 500 ,codPostal: "2235256921"
        }, {
            lat: -22.985256,
            lng: -43.226864,
            nome: "Drogaria Onofre",
            morada1: "Avenida Ataulfo de Paiva, 1160, Rio de Janeiro, Brazil",
            morada2: "1185",
            population: 500 ,codPostal: "2540900921"
        }, {
            lat: -22.9253703,
            lng: -43.2351173,
            nome: "Drogaria Onofre",
            morada1: "Rua Pinto de Figueiredo, 32, Rio de Janeiro, Brazil",
            morada2: "1186",
            population: 500 ,codPostal: "3294948821"
        }, {
            lat: -23.0059792,
            lng: -43.3174066,
            nome: "Drogaria Onofre",
            morada1: "Avenida das Am?ricas, 4666, Rio de Janeiro, Brazil",
            morada2: "1187",
            population: 500 ,codPostal: "4007252821"
        }, 

           {
      lat: -23.014168,
      lng: -43.466448000000014,
      nome: "DUO Solutions 2",
      morada1:"Rua dos Balneários do Complexo Desportivo",
      morada2: "Gafanha da Nazaré",
      population: 500 ,codPostal: "3830-225 Gafanha da Nazaré" // não colocar virgula no último item de cada maracdor
   } // não colocar vírgula no último marcador
];