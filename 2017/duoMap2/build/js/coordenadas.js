var markersData = [
   {
      lat: -22.7196509,
      lng: -43.417626600000006,
      nome: "Casa do Duff",
      morada1:"Rua Diogo Cão, 125",
      morada2: "Praia da Barra",
      codPostal: "3830-772 Gafanha da Nazaré" // não colocar virgula no último item de cada maracdor
   },
   {
      lat: -22.9086444,
      lng: -43.285239899999965,
      nome: "Casa do João Farias",
      morada1:"Quarta dos Patos, n.º 2",
      morada2: "Praia da Costa Nova",
      codPostal: "3830-453 Gafanha da Encarnação" // não colocar virgula no último item de cada maracdor
   },
   {
      lat: -23.014168,
      lng: -43.466448000000014,
      nome: "DUO Solutions 2",
      morada1:"Rua dos Balneários do Complexo Desportivo",
      morada2: "Gafanha da Nazaré",
      codPostal: "3830-225 Gafanha da Nazaré" // não colocar virgula no último item de cada maracdor
   } // não colocar vírgula no último marcador
];