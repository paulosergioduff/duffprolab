<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Mapa de calor</title>
    <script type="text/javascript" src='js/<?php echo $_GET['templateJS']; ?>'></script>
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #floating-panel {
        position: absolute;
        top: 10px;
        left: 25%;
        z-index: 5;
        background-color: #fff;
        padding: 5px;
        border: 1px solid #999;
        text-align: center;
        font-family: 'Roboto','sans-serif';
        line-height: 30px;
        padding-left: 10px;
      }
      #floating-panel {
        background-color: #fff;
        border: 1px solid #999;
        left: 25%;
        padding: 5px;
        position: absolute;
        top: 10px;
        z-index: 5;
      }
    </style>
  </head>

  <body>
    <div id="floating-panel">
      <button onclick="toggleHeatmap()">Esconder camadas</button>
      <button onclick="changeGradient()">Otimizar gradiente</button>
      <button onclick="changeRadius()">Alterar raio</button>
      <button onclick="changeOpacity()">Inserir transparência</button>
    </div>
    <div id="map"></div>
    <script>

      // This example requires the Visualization library. Include the libraries=visualization
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=visualization">

      var map, heatmap;

      function initMap() {
        var latlng = new google.maps.LatLng(-23.014168, -43.466448000000014);
        map = new google.maps.Map(document.getElementById('map'), {
          zoom: 10,
          center: latlng,
          mapTypeId: 'satellite'
        });

        heatmap = new google.maps.visualization.HeatmapLayer({
          data: getPoints(),
          map: map
        });
      }

      function toggleHeatmap() {
        heatmap.setMap(heatmap.getMap() ? null : map);
      }

      function changeGradient() {
        var gradient = [
         'rgba(102,255,0,0)', 
         'rgba(147,255,0,1)', 
         'rgba(193,255,0,1)', 
         'rgba(238,255,0,1)', 
         'rgba(244,227,0,1)', 
         'rgba(244,227,0,1)', 
         'rgba(249,198,0,1)', 
         'rgba(255,170,0,1)', 
         'rgba(255,113,0,1)', 
         'rgba(255,57,0,1)', 
         'rgba(255,0,0,1)'
        ]
        heatmap.set('gradient', heatmap.get('gradient') ? null : gradient);
      }

      function changeRadius() {
        heatmap.set('radius', heatmap.get('radius') ? null : 80);
      }

      function changeOpacity() {
        heatmap.set('opacity', heatmap.get('opacity') ? null : 0.2);
      }

      
    </script>
    <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCC6dCItLsLNgMg8eCl_rTcREN6sDOGiEg&libraries=visualization&callback=initMap">
    </script>
  </body>
</html>