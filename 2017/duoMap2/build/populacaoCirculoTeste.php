<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Meu círculo</title>
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }

       #zoomCapturado{
        position: fixed;
        font-size: 36px;
        color: white;
        top: 0%;
        right: 0%;
        height: auto;
        z-index: 0;
      }
    </style>
    <script type="text/javascript" src='js/<?php echo $_GET['templateJS']; ?>'></script>
  </head>
  <body>
    <div id="map"></div>
    <script>
      // This example creates circles on the map, representing populations in North
      // America.

      // First, create an object containing LatLng and population for each city.
     

      function initMap() {
        // Create the map.
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 12,
          center: {lat: -22.9637811, lng: -43.34603370000002},
          mapTypeId: 'satellite'
        });

        
       

        // Construct the circle for each value in citymap.
        // Note: We scale the area of the circle based on the population.
        for (var city in citymap) {
          // Add the circle for this city to the map.
          
          
          var cityCircle = new google.maps.Circle({
            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#FF0000',
            fillOpacity: 0.10,
            map: map,
            center: citymap[city].center,
            radius: 1500 // Raio decidigo em metros
          });



          // Supostamente essa função captura o evento zoom da interação
   map.addListener('zoom_changed', function() {
          //alert(map.getZoom())
          var pegaZoom = map.getZoom();
          document.getElementById('zoomCapturado').innerHTML = '<a href="http://localhost/NovoMapa/multiplos_marcadores/pegaRaio.php?marcador='+pegaZoom+'">'+geoRadius+'</a>';
          //var destino = 'http://localhost/NovoMapa/multiplos_marcadores/mapaDinamico.php?marcador='+pegaZoom;

        });

        }
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCC6dCItLsLNgMg8eCl_rTcREN6sDOGiEg&callback=initMap">
    </script>
    </div>
    <div id="zoomCapturado">16</div>
  </body>
</html>