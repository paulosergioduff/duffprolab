<?php
// Fonte http://phpbrasil.com/artigo/-4-bAMnsogUW/entendendo-o-conceito-de-interfaces-no-php
class Pessoa
{
                private $tratamento;
                private $nomeProprio;
                private $sobreNome;
                private $sufixo;
                public function setTratamento($tratamento)
                {
                                $this->tratamento = $tratamento;
                }
                public function getTratamento()
                {
                                return $this->tratamento;
                }
                public function setNomeProprio($recebeNomeProprio)
                {
                                $this->nomeProprio = $recebeNomeProprio;
                }
                public function getNomeProprio()
                {
                                return $this->nomeProprio;
                }
                public function setSobreNome($recebeSobreNome)
                {
                                $this->sobreNome = $recebeSobreNome;
                }
                public function getSobreNome()
                {
                                return $this->sobreNome;
                }
                public function setSufixo($sufixo)
                {
                                $this->sufixo = $sufixo;
                }
                public function getSufixo()
                {
                                return $sufixo;
                }
}
$pessoa = new Pessoa();
$pessoa->setTratamento("Senhor ");
$pessoa->setNomeProprio("João");
echo $pessoa->getTratamento();
echo $pessoa->getNomeProprio();

function sintaxeEstranha()
	{	
		$string = "<br> Nova string";
		echo ($string);
	}

	sintaxeEstranha();
?>