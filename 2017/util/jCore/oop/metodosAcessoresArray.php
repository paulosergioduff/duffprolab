<?php
class Pessoa
{
    private $nomeDaPessoa = array();
    
    public function setTratamento($tratamento)
    {
        $this->nomeDaPessoa['tratamento'] = $tratamento;
    }
    
    public function getTratamento()
    {
        return $this->nomeDaPessoa['tratamento'];
    }
    
    public function setNomeProprio($recebeNomeProprio)
    {
        $this->nomeDaPessoa['nomeProprio'] = $recebeNomeProprio;
    }
    
    public function getNomeProprio()
    {
        return $this->nomeDaPessoa['nomeProprio'];
    }

    /* etc... */
}

/*
 * Even though the internal implementation changed, the code here stays exactly
 * the same. The change has been encapsulated only to the Pessoa class.
 */
$pessoa = new Pessoa();
$pessoa->setTratamento("Senhor ");
$pessoa->setNomeProprio("João");

echo($pessoa->getTratamento());
echo($pessoa->getNomeProprio());

?>