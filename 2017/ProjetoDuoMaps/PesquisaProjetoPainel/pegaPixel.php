<?php

$coordenadas = $_GET['coor'];
$calibragemX = $_GET['calibragemX'];
$calibragemY = $_GET['calibragemY'];

?>

<!DOCTYPE html>
<html>
  <head>
    <title>Showing pixel and tile coordinates</title>
    <meta name="viewport" content="initial-scale=1.0">
    <meta charset="utf-8">
    <style>

      #cadastraPixel
        {
          top: 200px;
          left: 0%;
          background: #F5F5F5;
          position: fixed;
        }

      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
    </style>
  </head>
  <body>
  
   <div id="map"></div>
    <script>
var calibragemX = <?php echo $calibragemX; ?>;//24600
var calibragemY = <?php echo $calibragemY; ?>;//36000
    
      function initMap() {
        var rioDeJaneiro = new google.maps.LatLng(<?php echo $coordenadas; ?>); //-22.2887261, -42.5340796

        var map = new google.maps.Map(document.getElementById('map'), {
          center: rioDeJaneiro,
          zoom: 8,
          mapTypeId: 'satellite'
        });

        var coordInfoWindow = new google.maps.InfoWindow();
        coordInfoWindow.setContent(createInfoWindowContent(rioDeJaneiro, map.getZoom()));
        coordInfoWindow.setPosition(rioDeJaneiro);
        coordInfoWindow.open(map);

        map.addListener('zoom_changed', function() {
          coordInfoWindow.setContent(createInfoWindowContent(rioDeJaneiro, map.getZoom()));
          coordInfoWindow.open(map);
        });
      }

      var TILE_SIZE = 256;

      function createInfoWindowContent(latLng, zoom) {
        var scale = 1 << zoom;

        var worldCoordinate = project(latLng);
        

       //################# PEGA PIXEL ###########################
       var alturaPixel = parseInt(worldCoordinate.x * scale - calibragemX);
       var larguraPixel = parseInt(worldCoordinate.y * scale - calibragemY);
       // var alturaPixel = "400";
        //var larguraPixel = "400";
        //alert(alturaPixel+":altura,"+larguraPixel+"Largura");
        document.getElementById('objetoJS').innerHTML = "<div style='position: fixed; top: "+alturaPixel+"px; left: "+larguraPixel+"px; width: 20px; height: 20px; border-radius: 20px; background: orange;' ></>";
        //################# PEGA PIXEL ###########################

        var pixelCoordinate = new google.maps.Point(
            Math.floor(worldCoordinate.x * scale),
            Math.floor(worldCoordinate.y * scale));

        var tileCoordinate = new google.maps.Point(
            Math.floor(worldCoordinate.x * scale / TILE_SIZE),
            Math.floor(worldCoordinate.y * scale / TILE_SIZE));

        return [
          'rioDeJaneiro, IL',
          'LatLng: ' + latLng,
          'Zoom level: ' + zoom,
          'World Coordinate: ' + worldCoordinate,
          'Pixel Coordinate: ' + pixelCoordinate,
          'Tile Coordinate: ' + tileCoordinate,
          'Cadastrar pixelCoordinate: <a href="cadastrapixel.php?altura='+alturaPixel+'&largura='+larguraPixel+'&coordenadas='+latLng+'">Clique aqui para cadastrar</a>',
          'Altura: '+alturaPixel,
          'Largura: '+larguraPixel
        ].join('<br>');
      }

      // The mapping between latitude, longitude and pixels is defined by the web
      // mercator projection.
      function project(latLng) {
      var siny = Math.sin(latLng.lat() * Math.PI / 180);

        // Truncating to 0.9999 effectively limits latitude to 89.189. This is
        // about a third of a tile past the edge of the world tile.
        siny = Math.min(Math.max(siny, -0.9999), 0.9999);

        return new google.maps.Point(
            TILE_SIZE * (0.5 + latLng.lng() / 360),
            TILE_SIZE * (0.5 - Math.log((1 + siny) / (1 - siny)) / (4 * Math.PI)));
        
      }
        
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCC6dCItLsLNgMg8eCl_rTcREN6sDOGiEg&callback=initMap">
    </script>

    <div id="objetoJS"></div>

    <form action="" id="cadastraPixel">
    Coordenadas<input type="text" name="coor" value="<?php echo $coordenadas ; ?>"><br>
    Altura<input type="text" name="calibragemX" value="<?php echo $calibragemX ; ?>"><br>
    Largura<input type="text" name="calibragemY" value="<?php echo $calibragemY ; ?>"><br>
      <input type="submit" name="" value="Calibrar">
    </form>
    
  </body>
</html>