# CRUD em PDO mutável para PHP

 Projeto criado atender as seguintes necessidades de aplicações em PHP para data de publicação deste documento:

  - Principais rotinas envolvendo banco de dados.
  - Atender exigências de compatibilidade com versões mais recentes do PHP (até a publicação deste documento, o PHP7).
  - Facilidade em menutenção de núcleo das aplicações.
  - Adaptação na maior variedade de projetos possíveis.
  - Facilidade em migração tanto de métodos quanto diferentes banco de dados.

# Vantagens:

  - Inspirado no padrão MVC e REST, facilitando para a comunidade a compreeção do código.
  - Sem excesso de implementação, o que facilita a customização. 
  - Uso de tabela genérica para adaptação de projetos e rápidos testes.

### Como utilizar:

```php
<?php 
require_once "control/config.php";   
require_once "control/validaEntradas.php";
require_once "model/crud.class.php"; 
include "view/viewsRetornos.php";

$crud = crud::getInstance(Conexao::getInstance());

$crud->suaFuncaoDaQuery($campo1, $campo2, $campo3);
// Seja feliz!
?>
```
### INSERT:
```PHP
$crud->insert($login, $email, $senha, $valor1, $valor2, $tabela, $dbTabela);
```

### UPDATE:
```PHP
$crud->update($login, $email, $senha, $valor1, $valor2, $id, $dbTabela);
```

### DELETE:
```PHP
$crud->delete($id, $dbTabela);
```
### SELECT TOTAL:
```PHP
$dados = $crud->getAlltabela($dbTabela); // para todos da tabela
  if ($controleSelectTotal = true) {
     if ($controleSelectTotal != false) {
    foreach ($dados as $reg): 
       $id = $reg->id ;
       $varchar1 = $reg->varchar1;
       $$varchar2 = $reg->varchar2;
      retornaSelectTotal($id, $varchar1, $$varchar2);
      endforeach; 
  } 
  }
```
### SELECT DE UM ID:
```PHP
$dados = $crud->selecionaId($id, $dbTabela); 
  if ($controleSelect != false) {
    foreach ($dados as $reg): 
       $id = $reg->id ;
       $varchar1 = $reg->varchar1;
       $$varchar2 = $reg->varchar2;
      retornaSelecaoPorId($id, $varchar1, $$varchar2);
      endforeach; 
  }
```
### TABELA A SER USADA:
```SQL
CREATE TABLE IF NOT EXISTS `tabela` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `varchar1` varchar(255) CHARACTER SET utf8 NOT NULL,
  `varchar2` varchar(255) CHARACTER SET utf8 NOT NULL,
  `varchar3` varchar(255) CHARACTER SET utf8 NOT NULL,
  `varchar4` varchar(255) CHARACTER SET utf8 NOT NULL,
  `varchar5` varchar(255) CHARACTER SET utf8 NOT NULL,
  `num1` int(11) NOT NULL,
  `num2` int(11) NOT NULL,
  `num3` int(11) NOT NULL,
  `num4` int(11) NOT NULL,
  `num5` int(11) NOT NULL,
  `data1` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `data2` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `data3` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `data4` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `data5` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `bit1` int(1) NOT NULL,
  `bit2` int(1) NOT NULL,
  `bit3` int(1) NOT NULL,
  `bit4` int(1) NOT NULL,
  `bit5` int(1) NOT NULL,
  `texto1` longtext CHARACTER SET utf8 NOT NULL,
  `texto2` longtext CHARACTER SET utf8 NOT NULL,
  `texto3` longtext CHARACTER SET utf8 NOT NULL,
  `texto4` longtext CHARACTER SET utf8 NOT NULL,
  `texto5` longtext CHARACTER SET utf8 NOT NULL,
  `alias` varchar(255) CHARACTER SET utf8 NOT NULL,
  `tabela` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;
```

# ATENÇÃO!!

A simplicidade desta aplicação não isenta o desenvolvedor de se preocupar com a segurança dos dados. Sempre filtre os dados de entrada. Trabalhe no arquivo control/validaEntradas.php
Tome cuidado também com as regras escolhidas no control.