<!DOCTYPE html>
 <html lang="pt,BR">
    <head>
        <meta charset="utf-8" />
        <title>Google Maps API v3: Criando um mapa personalizado</title>
        <link rel="stylesheet" type="text/css" href="css/estilo.css">
   <script language="JavaScript">
   //Guarda Resolucao em variavel
   var largura = screen.width;
   var autura = screen.height;

//alert(" Reso: "+screen.width+"X"+screen.height)
</script>

    </head>
 
    <body bgcolor="#191970">
    	<div id="mapa" style="height: 1080px; width: 100%">
        </div>
		
		<script src="js/jquery.min.js"></script>
 
        <!-- Maps API Javascript -->
        <!-- minha ID  AIzaSyCC6dCItLsLNgMg8eCl_rTcREN6sDOGiEg 
        <script src="//maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&key=AIzaSyCC6dCItLsLNgMg8eCl_rTcREN6sDOGiEg" async="" defer="defer" type="text/javascript"></script>
         -->
         <script src="//maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&key=AIzaSyCC6dCItLsLNgMg8eCl_rTcREN6sDOGiEg&language=pt&region=BR"">
             
             // Escolhe região: &language=pt&region=BR
         </script>
        
        <!-- Caixa de informação -->
        <script src="js/infobox.php"></script>
		
        <!-- Agrupamento dos marcadores -->
		<script src="js/markerclusterer.php"></script>
 
        <!-- Arquivo de inicialização do mapa -->
		<script src="js/mapa.php"></script>
    </body>
</html>