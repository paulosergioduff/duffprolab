var map;
var idInfoBoxAberto;
var infoBox = [];
var markers = [];

function initialize() {	

	var coordPrincipal = '-18.8800397, -47.05878999999999';
	var latlng = new google.maps.LatLng(coordPrincipal);

	
    var options = {
        zoom: 12,
		center: latlng,
        mapTypeId: 'satellite'
// Original : google.maps.MapTypeId.ROADMAP
// 'satellite' para Personalização absoluta
// Mais em https://developers.google.com/maps/documentation/javascript/maptypes?hl=pt-br
/*roadmap exibe o mapa de vias padrão. Esse é o tipo de mapa padrão.
satellite exibe imagens de satélite do Google Earth
hybrid exibe uma combinação de visualizações normais e de satélite
terrain exibe um mapa físico baseado em informações do terreno.

*/

// Convertendo cordenadas em pixel https://developers.google.com/maps/documentation/javascript/maptypes#PixelCoordinates
// Implementação do citado acima https://developers.google.com/maps/documentation/javascript/examples/map-coordinates



    };

    map = new google.maps.Map(document.getElementById("mapa"), options);
}

initialize();

function abrirInfoBox(id, marker) {
	if (typeof(idInfoBoxAberto) == 'number' && typeof(infoBox[idInfoBoxAberto]) == 'object') {
		infoBox[idInfoBoxAberto].close();

	}

	infoBox[id].open(map, marker);
	idInfoBoxAberto = id;


	// pegar coordenadas

	
}

function carregarPontos() {
	
	$.getJSON('js/pontos.php', function(pontos) {
		
		var latlngbounds = new google.maps.LatLngBounds();
		
		$.each(pontos, function(index, ponto) {
			
			var marker = new google.maps.Marker({
				position: new google.maps.LatLng(ponto.Latitude, ponto.Longitude),
				title: "Clique na região para informações específicas!",
				icon: 'img/RaioMarcador.png'
			});
			
			var myOptions = {
				content: "<p>" + ponto.Descricao + "</p>",
				pixelOffset: new google.maps.Size(-150, 0)
        	};

			infoBox[ponto.Id] = new InfoBox(myOptions);
			infoBox[ponto.Id].marker = marker;
			
			infoBox[ponto.Id].listener = google.maps.event.addListener(marker, 'click', function (e) {
				abrirInfoBox(ponto.Id, marker);
			});
			
			markers.push(marker);
			
			latlngbounds.extend(marker.position);


			
		});
		
		var markerCluster = new MarkerClusterer(map, markers);
		
		map.fitBounds(latlngbounds);
		
	});
	
}

carregarPontos();

  function createInfoWindowContent(latLng, zoom) {
        var scale = 1 << zoom;

        var worldCoordinate = project(latLng);
        

       //################# PEGA PIXEL ###########################
       var alturaPixel = parseInt(worldCoordinate.x * scale - calibragemX);
       var larguraPixel = parseInt(worldCoordinate.y * scale - calibragemY);
       // var alturaPixel = "400";
        //var larguraPixel = "400";
        //alert(alturaPixel+":altura,"+larguraPixel+"Largura");
        document.getElementById('objetoJS').innerHTML = "<div style='position: fixed; top: "+alturaPixel+"px; left: "+larguraPixel+"px; width: 20px; height: 20px; border-radius: 20px; background: orange;' ></>";
        //################# PEGA PIXEL ###########################

        var pixelCoordinate = new google.maps.Point(
            Math.floor(worldCoordinate.x * scale),
            Math.floor(worldCoordinate.y * scale));

        var tileCoordinate = new google.maps.Point(
            Math.floor(worldCoordinate.x * scale / TILE_SIZE),
            Math.floor(worldCoordinate.y * scale / TILE_SIZE));

        return [
          'rioDeJaneiro, IL',
          'LatLng: ' + latLng,
          'Zoom level: ' + zoom,
          'World Coordinate: ' + worldCoordinate,
          'Pixel Coordinate: ' + pixelCoordinate,
          'Tile Coordinate: ' + tileCoordinate,
          'Cadastrar pixelCoordinate: <a href="cadastrapixel.php?altura='+alturaPixel+'&largura='+larguraPixel+'&coordenadas='+latLng+'">Clique aqui para cadastrar</a>',
          'Altura: '+alturaPixel,
          'Largura: '+larguraPixel
        ].join('<br>');
      }

      // The mapping between latitude, longitude and pixels is defined by the web
      // mercator projection.
      function project(latLng) {
      var siny = Math.sin(latLng.lat() * Math.PI / 180);

        // Truncating to 0.9999 effectively limits latitude to 89.189. This is
        // about a third of a tile past the edge of the world tile.
        siny = Math.min(Math.max(siny, -0.9999), 0.9999);

        return new google.maps.Point(
            TILE_SIZE * (0.5 + latLng.lng() / 360),
            TILE_SIZE * (0.5 - Math.log((1 + siny) / (1 - siny)) / (4 * Math.PI)));
        
      }