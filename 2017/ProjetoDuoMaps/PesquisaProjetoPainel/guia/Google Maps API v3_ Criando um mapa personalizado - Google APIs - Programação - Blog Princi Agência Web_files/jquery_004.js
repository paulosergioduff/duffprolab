﻿/// <reference path="../../jquery.min.js" />

$(document).ready(function () {

    /* ====================
    Validação
    ==================== */

    $("#txtComentar").elastic();

    // Regras e mensagens
    $("#form-comentario").validate({
        rules: {
            txtComentar: {
                required: true
            }
        },
        messages: {
            txtComentar: {
                required: "Digite o seu comentário!"
            }
        },
        submitHandler: function (form) {
            $(":submit", form).prop("disabled", true);
            form.submit();
        },
        errorPlacement: function (error, element) {
            element.parents("fieldset > div").append(error);
        }
    });

    if ($('.logado-como').length == 0) {
        $('#txtEmailComentar').rules('add', {
            email: true,
            messages: {
                email: "Formato inválido de e-mail!"
            }
        });

        $('#txtNomeComentar').rules('add', {
            required: true,
            messages: {
                required: "Digite o seu nome!"
            }
        });

        function verificarNotificacao() {
            if ($('#txtNotificarComentario').is(':checked')) {
                $('#txtEmailComentar').rules('add', {
                    required: true,
                    messages: {
                        required: "Digite o seu e-mail!"
                    }
                });
            }
            else {
                $('#txtEmailComentar').rules("remove", "required");
            }
        }

        verificarNotificacao();

        $('#txtNotificarComentario').change(function () {
            verificarNotificacao();
        });
    }
});