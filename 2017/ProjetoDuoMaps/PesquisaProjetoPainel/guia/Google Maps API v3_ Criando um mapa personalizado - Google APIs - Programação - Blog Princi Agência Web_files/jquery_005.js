﻿/// <reference path="../../jquery.min.js" />

SyntaxHighlighter.all();

$(document).ready(function () {

    // Colorbox
    jQuery.extend(jQuery.colorbox.settings, {
        current: "",
        previous: "Anterior",
        next: "Próxima",
        close: "Fechar",
        slideshowStart: "iniciar apresentação de slides",
        slideshowStop: "parar apresentação de slides",
        xhrError: "Erro ao carregar o conteúdo.",
        imgError: "Erro ao carregar a imagem.",
        scalePhotos: true,
        maxWidth: "90%",
        maxHeight: "90%"
    });

    $('#conteudo-post a:has(img)[href*=".jpg"], #conteudo-post a:has(img)[href*=".gif"], #conteudo-post a:has(img)[href*=".png"], #conteudo-post a:has(img)[href*=".jpeg"], #conteudo-post a:has(img)[href*=".bmp"]').colorbox({ rel: 'colorbox' });

});