﻿/// <reference path="../../jquery.min.js" />

$(document).ready(function () {

    $('.link-responder:not(.clicado)').click(function (event) {
        event.preventDefault();

        var comentario = $(this).parents('.caixa-comentario');
        
        if (comentario.next().attr('class') != 'formulario-responder') {

            var clone = $('.formulario-responder:hidden').clone(true);

            if ((comentario.index('.caixa-comentario') + 1) == $('.caixa-comentario').length)
                $('.divisa', clone).remove();

            clone.removeClass('template');
            clone.insertAfter(comentario);
            clone.slideDown("normal", function () {
                $(':input:first', clone).focus();
            });

            var id = comentario.attr('id').replace('comentario-', '');

            $('form', clone).attr('id', 'form-responder-comentario-' + id);
            $('form', clone).attr('action', $('form', clone).attr('action') + id);

            $('label[for="txtNomeResposta"]', clone).attr('for', 'txtNomeResposta' + id);
            $('input[id="txtNomeResposta"]', clone).attr('id', 'txtNomeResposta' + id);
            $('label[for="txtEmailResposta"]', clone).attr('for', 'txtEmailResposta' + id);
            $('input[id="txtEmailResposta"]', clone).attr('id', 'txtEmailResposta' + id);
            $('label[for="txtMensagemResposta"]', clone).attr('for', 'txtMensagemResposta' + id);
            $('textarea[id="txtMensagemResposta"]', clone).attr('id', 'txtMensagemResposta' + id);
            $('label[for="txtNotificarResposta"]', clone).attr('for', 'txtNotificarResposta' + id);
            $('input[id="txtNotificarResposta"]', clone).attr('id', 'txtNotificarResposta' + id);

            /* ====================
            Validação
            ==================== */

            // Regras e mensagens
            $('#form-responder-comentario-' + id).validate({
                rules: {
                    txtMensagemResposta: {
                        required: true
                    }
                },
                messages: {
                    txtMensagemResposta: {
                        required: "Digite o seu comentário!"
                    }
                },
                submitHandler: function (form) {
                    $(":submit", form).prop("disabled", true);
                    form.submit();
                },
                errorPlacement: function (error, element) {
                    element.parents("fieldset > div").append(error);
                }
            });

            if ($('.logado-como').length == 0) {
                $('[name="txtEmailResposta"]', clone).rules('add', {
                    email: true,
                    messages: {
                        email: "Formato inválido de e-mail!"
                    }
                });

                $('[name="txtNomeResposta"]', clone).rules('add', {
                    required: true,
                    messages: {
                        required: "Digite o seu nome!"
                    }
                });

                function verificarNotificacao() {
                    if ($('[name="txtNotificarResposta"]', clone).is(':checked')) {
                        $('[name="txtEmailResposta"]', clone).rules('add', {
                            required: true,
                            messages: {
                                required: "Digite o seu e-mail!"
                            }
                        });
                    }
                    else {
                        $('[name="txtEmailResposta"]', clone).rules("remove", "required");
                    }
                }

                verificarNotificacao();

                $('[name="txtNotificarResposta"]', clone).change(function () {
                    verificarNotificacao();
                });
            }
        }
        else {
            comentario.next().find(':input:first').focus();
        }
    });
});
