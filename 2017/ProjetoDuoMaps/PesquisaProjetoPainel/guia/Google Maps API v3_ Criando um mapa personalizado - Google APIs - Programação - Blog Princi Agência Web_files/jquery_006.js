﻿/// <reference path="jquery.min.js" />

$(document).ready(function () {

    /* =============
    Checkbox
    ============= */

    $(':checkbox')
    .wrap(function () {
        return '<div class="checkbox' + ((this.checked) ? " checked" : "") + (($(this).is(":disabled")) ? " disabled" : "") + ' ' + $(this).attr("class") + '" />';
    }).focus(function () {
        $(this).parent().addClass('focus');
    }).blur(function () {
        $(this).parent().removeClass('focus');
    });

    $('.checkbox:has(:disabled)').addClass('disabled').unbind('click');

    //Ao clicar na imagem do checkbox
    $('.checkbox:not(.disabled)').click(function (event) {
        if (event.target.nodeName != 'INPUT') $(':checkbox', this).prop('checked', !$(':checkbox', this).prop('checked')).change();

        if ($(':checkbox', this).prop('checked')) $(':checkbox', this).parent().addClass('checked');
        else $(':checkbox', this).parent().removeClass('checked');
    });

    // Polyfill para SVG
    if (Modernizr.svg) {
        $('#logo').html('<a class="svg" href="http://www.princiweb.com.br/blog/">Agência Digital Princi Web - Blog</a>');

    } else {
        $('#logo').html('<a class="png" href="http://www.princiweb.com.br/blog/">Agência Digital Princi Web - Blog</a>');
    }

    // Clique na linha inteira dos Posts mais comentados
    $('.lista-posts li').click(function () {
        var href = $('a', this).attr('href');

        location = href;
    });

    // Categorias
    var categoriaURL = window.location.pathname.split('/')[2];

    $(".categorias .accordion").each(function () {

        var categoriaAtual = $('h4', this).attr('class')

        if (categoriaURL != "" && categoriaAtual == categoriaURL) {
            $("." + categoriaURL).parent("div").accordion({ collapsible: true, header: "h4", heightStyle: "content" });
        } else {
            $(this).accordion({ active: false, collapsible: true, header: "h4", heightStyle: "content" });
        }

    });
    
});