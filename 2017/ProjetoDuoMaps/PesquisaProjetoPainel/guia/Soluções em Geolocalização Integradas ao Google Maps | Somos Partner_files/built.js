function mascara(o, f) {
    v_obj = o
    v_fun = f
    setTimeout("execmascara()", 1)
}

function execmascara() {
    v_obj.value = v_fun(v_obj.value)
}

function mtel(v) {
    v = v.replace(/\D/g, "") //Remove tudo o que não é dígito
    v = v.replace(/^(\d\d)(\d)/g, "($1) $2") //Coloca parênteses em volta dos dois primeiros dígitos
    v = v.replace(/(\d{4})(\d)/, "$1-$2") //Coloca hífen entre o quarto e o quinto dígitos
    return v
}

function move(pos){
    $('html, body').animate({scrollTop: pos - 150}, 800);
}
$(function() {
    var navigatePage = $('.navigatePage');

    window.onload = function(){
        if(!!(location.hash && $(location.hash).length)){
            move($(location.hash).offset().top);
        }   
    }

    navigatePage.on("click", function(e){
        e.preventDefault();
        var element = $($(this).attr("href"));
        move(element.offset().top);
    });
    

    $(document).on('scroll', function(e){
        var header = $("#top");
        if(header.offset().top > 250)
            header.addClass("scroll-header");
        else
            header.removeClass("scroll-header");
    });

    $('#telefone').keypress(function() {
        $(this).attr('maxlength', 15);
        mascara(this, mtel);
    });
    $('#topo').topLink({
        min: 400,
        fadeSpeed: 100
    });
    //smoothscroll
    $('#topo').click(function(e) {
        e.preventDefault();
        $.scrollTo(0, 800);
    });

    $('form').on('submit', function(event) {
        var $form = $(event.target).closest('form');
        var inputs = $($form).find(':input').serializeArray();
        RdIntegration.post(inputs, function() {
            return false;
        });
    });

    $('#bannerflow').cycle({
        fx: 'scrollHorz',
        timeout: 5000,
        next: '#next-flow',
        prev: '#prev-flow'
    });

    $("#btnTeste").on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({ scrollTop: ($('#teste').offset().top - 100) }, 800)
    });

    $('.carrosel-container').jcarousel();

    $('.arrow#prev')
        .on('jcarouselcontrol:active', function() {
            $(this).removeClass('inactive');
        })
        .on('jcarouselcontrol:inactive', function() {
            $(this).addClass('inactive');
        })
        .jcarouselControl({
            target: '-=1'
        });

    $('.arrow#next')
        .on('jcarouselcontrol:active', function() {
            $(this).removeClass('inactive');
        })
        .on('jcarouselcontrol:inactive', function() {
            $(this).addClass('inactive');
        })
        .jcarouselControl({
            target: '+=1'
        });
});
jQuery.fn.topLink = function(settings) {
    settings = jQuery.extend({
        min: 1,
        fadeSpeed: 200,
        ieOffset: 50
    }, settings);
    return this.each(function() {
        //listen for scroll
        var el = $(this);
        el.css('display', 'none'); //in case the user forgot
        $(window).scroll(function() {
            if (!jQuery.support.hrefNormalized) {
                el.css({
                    'position': 'absolute',
                    'top': $(window).scrollTop() + $(window).height() - settings.ieOffset
                });
            }
            if ($(window).scrollTop() >= settings.min) {
                el.fadeIn(settings.fadeSpeed);
            } else {
                el.fadeOut(settings.fadeSpeed);
            }
        });
    });
};
$(function(a) {
    "use strict";

    function b(b) {
        var c = b.find(".link-menu"),
            d = document.location.href.split("/"),
            e = d[d.length - 1];
        c.each(function() {
            var b = a(this).attr("href");
            b === e ? a(this).addClass("ative") : "" === e && c.eq(0).addClass("ative")
        })
    }
    var c = a("#menu");
    b(c)
}), $(function(a) {
    "use strict";
$("#container-boxBanner").cycleOld({
        fx: "scrollHorz",
        timeout: 6000,
        pager: "#pager-boxBanner",
    });
    function b(b) {
        var c = 800,
            d = a(b),
            e = d.find(".tt-banner"),
            f = d.find(".img-banner"),
            g = d.find(".btn-banner");
        e.animate({
            marginTop: "0px"
        }, c), setTimeout(f.animate({
            marginRight: "0px"
        }, c), 2e3), setTimeout(g.animate({
            marginLeft: "0px"
        }, c), 3500)
    }
    var c = a("#banner"),
        d = c.find("li");
    a.each(d, function() {
        var b = a(this),
            c = b.attr("data-bg");
        b.css("backgroundImage", "url(" + c + ")")
    }), c.cycle({
        timeout: 7000,
        pager: "#pager",
        speed: "slow",
        prev: "#prevslide",
        next: "#nextslide",
        timeoutFn: '',
        end: "nowrap"
    }), a("#case").cycle({
        fx: "scrollHorz",
        speed: "slow",
        prev: "#prevcase",
        next: "#nextcase",
        timeout: 1e4
    }), a("#list-clientes").cycle({
        fx: "scrollHorz",
        speed: "slow",
        timeout: 6e3
    })
}), $(function(a) {
    "use strict";
    var b = null,
        c = a(".app-iten");
    c.mouseenter(function() {
        var c = a(this).find("img"),
            d = c.attr("src").split("/"),
            e = d[d.length - 1].split(".");
        return b = e[0], c.fadeIn("slow", function() {
            a(this).attr("src", "assets/img/" + b + "_hover.png")
        }), b
    }), c.mouseleave(function() {
        var c = a(this).find("img");
        c.fadeIn("slow", function() {
            a(this).attr("src", "assets/img/" + b + ".png")
        })
    })
}), $(function(a) {
    "use strict";
    a("#news").validationEngine()
}), $(function(a) {
    "use strict";
    a("#form-contato").validationEngine()
}), $(function(a) {
    "use strict";
    var b = a(window).width();
    if (900 > b) {
        var c = a("#top"),
            d = a("#menu"),
            e = c.find(".btn"),
            f = c.find(".cl-9");
        f.before('<div id="menu-mobile"/>'), a("#menu-mobile").on("click", function() {
            var a = !1;
            return d.is(":visible") ? (e.fadeOut(), d.slideUp("slow"), a = !1) : (d.slideDown("slow", function() {
                e.fadeIn()
            }), a = !0)
        })
    }
});