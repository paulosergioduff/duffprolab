$(function(){
  	$('#telefone').keypress(function() {
          $(this).val( mtel($(this).val() ));
  	});   
    $("#form-kissflow").submit(function(){
        var $nome     = $("#nome").val(),
            $email    = $("#email").val(),
            $empresa  = $("#empresa").val(),
            $telefone = $("#telefone").val();
        if(isNotEmpty($nome) && isNotEmpty($email) && isNotEmpty($empresa) && isNotEmpty($telefone)){
            var campos = $(this).serialize();
            var options = { fieldMapping: {
                    'email': $email,
                    'nome': $nome ,
                    'empresa': $empresa,
                    'telefone': $telefone
                }
            };
            RdIntegration.integrate('cde7ffd791bcaea558d37310c19df24c', '[KiSSFLOW] Formulário Landing Page - http://ipnetsolucoes.com.br/kissflow', options);  

            $.ajax({
				type: "POST",
				url: "https://ipnet-plune.appspot.com/contato",
				data: campos,
				success: function(data){
					location.href = 'https://kissflow.com/create-account/?partner=sfmvjiHgZ&email=' + $email;
				}
			});     
        }
        return false;
    }).validationEngine();
});
function isNotEmpty(val){
    if(!val || val === undefined || val === null || val === "")
        return false;
    return true;
}
function mtel(v){
    v = v.replace(/\D/g,"");
    v = v.replace(/^(\d\d)(\d)/g,"($1) $2");
    v = v.replace(/(\d{4})(\d)/,"$1-$2");
    return v;
}