var map;
var idInfoBoxAberto;
var infoBox = [];
var markers = [];

function initialize() {	
	var latlng = new google.maps.LatLng(-18.8800397, -47.05878999999999);

	
    var options = {
        zoom: 12,
		center: latlng,
        mapTypeId: 'satellite'
// Original : google.maps.MapTypeId.ROADMAP
// 'satellite' para Personalização absoluta
// Mais em https://developers.google.com/maps/documentation/javascript/maptypes?hl=pt-br
/*roadmap exibe o mapa de vias padrão. Esse é o tipo de mapa padrão.
satellite exibe imagens de satélite do Google Earth
hybrid exibe uma combinação de visualizações normais e de satélite
terrain exibe um mapa físico baseado em informações do terreno.

*/

// Convertendo cordenadas em pixel https://developers.google.com/maps/documentation/javascript/maptypes#PixelCoordinates
// Implementação do citado acima https://developers.google.com/maps/documentation/javascript/examples/map-coordinates

    };

    map = new google.maps.Map(document.getElementById("mapa"), options);
}

initialize();

function abrirInfoBox(id, marker) {
	if (typeof(idInfoBoxAberto) == 'number' && typeof(infoBox[idInfoBoxAberto]) == 'object') {
		infoBox[idInfoBoxAberto].close();
	}

	infoBox[id].open(map, marker);
	idInfoBoxAberto = id;
}

function carregarPontos() {
	
	$.getJSON('js/pontos.php', function(pontos) {
		
		var latlngbounds = new google.maps.LatLngBounds();
		
		$.each(pontos, function(index, ponto) {
			
			var marker = new google.maps.Marker({
				position: new google.maps.LatLng(ponto.Latitude, ponto.Longitude),
				title: "Clique na região para informações específicas!",
				icon: 'img/marcador.png'
			});
			
			var myOptions = {
				content: "<p>" + ponto.Descricao + "</p>",
				pixelOffset: new google.maps.Size(-150, 0)
        	};

			infoBox[ponto.Id] = new InfoBox(myOptions);
			infoBox[ponto.Id].marker = marker;
			
			infoBox[ponto.Id].listener = google.maps.event.addListener(marker, 'click', function (e) {
				abrirInfoBox(ponto.Id, marker);
			});
			
			markers.push(marker);
			
			latlngbounds.extend(marker.position);
			
		});
		
		var markerCluster = new MarkerClusterer(map, markers);
		
		map.fitBounds(latlngbounds);
		
	});
	
}

carregarPontos();