<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
	
	<link rel="stylesheet" type="text/css" href="view/novoMapa.css">
	<link rel="stylesheet" type="text/css" href="css/estilo.css">
	<title>Meu FrameWork Mobile dinâmico</title>
</head>
<body>
	<div id="pagina">
		<div id="mapa">
		
		</div>

		<script src="js/jquery.min.js"></script>
 
        <!-- Maps API Javascript -->
        <!-- minha ID  AIzaSyCC6dCItLsLNgMg8eCl_rTcREN6sDOGiEg 
        <script src="//maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&key=AIzaSyCC6dCItLsLNgMg8eCl_rTcREN6sDOGiEg" async="" defer="defer" type="text/javascript"></script>
         -->
         <script src="//maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&key=AIzaSyCC6dCItLsLNgMg8eCl_rTcREN6sDOGiEg&language=pt&region=BR"">
             
             // Escolhe região: &language=pt&region=BR
         </script>
        
        <!-- Caixa de informação -->
        <script src="js/infobox.php"></script>
		
        <!-- Agrupamento dos marcadores -->
		<script src="js/markerclusterer.php"></script>
		
 
        <!-- Arquivo de inicialização do mapa -->
		<script src="js/mapa.php"></script>
		<script src="js/extra.js"></script>
		
		</div>
	 </div>

	 <div id="objetoJS">
	 </div>

</script>
</body>
</html>