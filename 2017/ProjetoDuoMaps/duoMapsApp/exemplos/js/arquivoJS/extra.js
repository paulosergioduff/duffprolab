  
  var calibragemX = <?php echo $calibragemX; ?>;//24600
var calibragemY = <?php echo $calibragemY; ?>;//36000

  var TILE_SIZE = 256;

  function createInfoWindowContent(latLng, zoom) {
        var scale = 1 << zoom;

        var worldCoordinate = project(latLng);
        

       //################# PEGA PIXEL ###########################
       var alturaPixel = parseInt(worldCoordinate.x * scale - calibragemX);
       var larguraPixel = parseInt(worldCoordinate.y * scale - calibragemY);
       // var alturaPixel = "400";
        //var larguraPixel = "400";
        //alert(alturaPixel+":altura,"+larguraPixel+"Largura");
        document.getElementById('objetoJS').innerHTML = "<div style='position: fixed; top: "+alturaPixel+"px; left: "+larguraPixel+"px; width: 20px; height: 20px; border-radius: 20px; background: orange;' ></>";
        //################# PEGA PIXEL ###########################

        var pixelCoordinate = new google.maps.Point(
            Math.floor(worldCoordinate.x * scale),
            Math.floor(worldCoordinate.y * scale));

        var tileCoordinate = new google.maps.Point(
            Math.floor(worldCoordinate.x * scale / TILE_SIZE),
            Math.floor(worldCoordinate.y * scale / TILE_SIZE));

        return [
          'rioDeJaneiro, IL',
          'LatLng: ' + latLng,
          'Zoom level: ' + zoom,
          'World Coordinate: ' + worldCoordinate,
          'Pixel Coordinate: ' + pixelCoordinate,
          'Tile Coordinate: ' + tileCoordinate,
          'Cadastrar pixelCoordinate: <a href="cadastrapixel.php?altura='+alturaPixel+'&largura='+larguraPixel+'&coordenadas='+latLng+'">Clique aqui para cadastrar</a>',
          'Altura: '+alturaPixel,
          'Largura: '+larguraPixel
        ].join('<br>');
      }

      // The mapping between latitude, longitude and pixels is defined by the web
      // mercator projection.
      function project(latLng) {
      var siny = Math.sin(latLng.lat() * Math.PI / 180);

        // Truncating to 0.9999 effectively limits latitude to 89.189. This is
        // about a third of a tile past the edge of the world tile.
        siny = Math.min(Math.max(siny, -0.9999), 0.9999);

        return new google.maps.Point(
            TILE_SIZE * (0.5 + latLng.lng() / 360),
            TILE_SIZE * (0.5 - Math.log((1 + siny) / (1 - siny)) / (4 * Math.PI)));
        
      }